### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ 1027d6e0-7390-11ed-319f-1746555f5646
const input = read("04_input.txt", String)

# ╔═╡ 28277bc5-24bb-4420-b0ed-ce05103cbe69
const test_input = "\
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
"

# ╔═╡ 7d8b4090-7d89-4cda-8d43-328942e646fe
function parse_range(l)
	n = parse.(Int,split(l,"-"))
	n[1]:n[2]
end

# ╔═╡ f388e56c-d19f-42cc-96a9-7aaf5893a440
parse_pair(l) = split(l,",") .|> parse_range

# ╔═╡ 71d86f5b-5392-4603-a72a-a86e268cca07
const lines = split(input, "\n")[1:end-1]

# ╔═╡ b1e33b24-b209-4cae-9970-d3f8fc49b7c4
const pairs = parse_pair.(lines)

# ╔═╡ 98c6b728-9c7d-438b-b1f6-ec1937ad49c9
count_p(p) = issubset(p[1], p[2]) || issubset(p[2], p[1])

# ╔═╡ 496ed5bf-2006-462f-bd70-4e1d5ed93579
count(count_p, parse_pair.(lines))

# ╔═╡ e39a374a-54ec-4349-84ef-d7d144bc0738
count2(p) = !isempty(p[1] ∩ p[2])

# ╔═╡ b25b28ec-ea3d-4623-8d4b-015b1710f6c0
count(count2, pairs)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═1027d6e0-7390-11ed-319f-1746555f5646
# ╠═28277bc5-24bb-4420-b0ed-ce05103cbe69
# ╠═7d8b4090-7d89-4cda-8d43-328942e646fe
# ╠═f388e56c-d19f-42cc-96a9-7aaf5893a440
# ╠═71d86f5b-5392-4603-a72a-a86e268cca07
# ╠═b1e33b24-b209-4cae-9970-d3f8fc49b7c4
# ╠═98c6b728-9c7d-438b-b1f6-ec1937ad49c9
# ╠═496ed5bf-2006-462f-bd70-4e1d5ed93579
# ╠═e39a374a-54ec-4349-84ef-d7d144bc0738
# ╠═b25b28ec-ea3d-4623-8d4b-015b1710f6c0
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
