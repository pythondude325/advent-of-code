### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ f0f79a0e-7aa5-11ed-327e-bfdd9bdd45f6
const input = readlines("13_input.txt")

# ╔═╡ e52a1474-5bbc-4c0c-a8a0-ac5934d70863
const test_input = split("\
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]", "\n")

# ╔═╡ 3aed8086-90c9-42af-b840-212adbc712cc
parse_input(i) = tuple.(eval(i[1:3:end]), eval(i[2:3:end]))

# ╔═╡ 84bdbaac-b720-4b1e-b767-83b351f8e803
test_input |> parse_input

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═f0f79a0e-7aa5-11ed-327e-bfdd9bdd45f6
# ╠═e52a1474-5bbc-4c0c-a8a0-ac5934d70863
# ╠═3aed8086-90c9-42af-b840-212adbc712cc
# ╠═84bdbaac-b720-4b1e-b767-83b351f8e803
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
