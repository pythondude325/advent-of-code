### A Pluto.jl notebook ###
# v0.19.20

using Markdown
using InteractiveUtils

# ╔═╡ 2179e8e1-d607-4010-a757-4a87d211788c
using AStarSearch

# ╔═╡ 6ec62334-79d8-11ed-1bf9-1db7af0d5f05
# const input = read("12_input.txt", String)
const input = readlines("12_input.txt")

# ╔═╡ 23ee4944-c2be-4c16-bab7-0fc6c634feaf
const test_input = split("\
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi", "\n")

# ╔═╡ 519e6698-90dd-4fa1-b2b3-15324a686d57
parse_height(c)::Int =
    if c == 'S'
        1
    elseif c == 'E'
        26
    else
        c - 'a' + 1
    end

# ╔═╡ ae903a50-6220-4f8a-bfa0-7d19b7b6bdf1
function parse_grid(i)
    reduce(hcat, (collect(I) .|> parse_height for I = i))
end

# ╔═╡ 1d7ba988-4c20-4802-b2a4-2e7d8451594b
const CI = CartesianIndex

# ╔═╡ 1b8e5030-a381-45d3-b1dc-bc67f15e4c6e
function find_endpoints(L)
    s = CI(0, 0)
    e = CI(0, 0)
    for (i, l) = enumerate(L)
        fs = findfirst('S', l)
        if !isnothing(fs)
            s = CI(fs, i)
        end
        es = findfirst('E', l)
        if !isnothing(es)
            e = CI(es, i)
        end
    end
    s, e
end

# ╔═╡ 35163d5f-ffae-48ea-b0df-6527d6457b3f
const sinput = input

# ╔═╡ 484ebf25-9ef7-4699-8ca1-7544c937f011
const S, E = find_endpoints(sinput)

# ╔═╡ 0a1781bd-edbb-4034-9409-c41dbfca4c81
const grid = parse_grid(sinput)

# ╔═╡ 11728674-54fc-448a-b576-8bd26e71fb59
neighbors = [CI(-1, 0), CI(0, -1), CI(1, 0), CI(0, 1)]

# ╔═╡ 92dc2340-b19b-4a17-99ae-b87eef630c4f
neighbors_f(C) = (p) -> Iterators.filter(
    x -> checkbounds(Bool, C, x) && C[x] <= (C[p] + 1),
    (p + n for n = neighbors)
)

# ╔═╡ ee8a2383-211a-455e-8a33-dfd48bee763a
neighbors_f2(C) = (p) -> Iterators.filter(
    x -> checkbounds(Bool, C, x) && (C[p] - 1) <= C[x],
    (p + n for n = neighbors)
)

# ╔═╡ fdc9ef16-20b9-41c5-9168-9286d37fbd52
const part2 = astar(neighbors_f2(grid), E, S, isgoal=((a, x) -> grid[x] == grid[a])).cost

# ╔═╡ 5810f222-48cf-4c80-8558-0d497abf9eff
neighbors_f(grid)(CI(1, 1)) |> collect

# ╔═╡ cf3d0f35-2c2c-4598-85ce-592268d61496
function findpath(C, s, e)
    result = astar(
        neighbors_f(C),
        s,
        e,
    )
end

# ╔═╡ 34c3e98d-2e66-4127-b147-08fb677dead0
const part1 = findpath(grid, S, E).cost

# ╔═╡ 5aaa83fa-fe54-4df9-bd9d-01d5049de593
manhattan(s, e) = sum(abs.((s - e).I))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
AStarSearch = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"

[compat]
AStarSearch = "~0.6.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "8ed4e4dc8f77e4ef510a3d2754e1998c3b7052d2"

[[deps.AStarSearch]]
deps = ["DataStructures"]
git-tree-sha1 = "b43cbd7712b6ad5efe5ef46463a1a82ac9949f09"
uuid = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
version = "0.6.2"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Dates", "LinearAlgebra", "UUIDs"]
git-tree-sha1 = "00a2cccc7f098ff3b66806862d275ca3db9e6e5a"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.5.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═6ec62334-79d8-11ed-1bf9-1db7af0d5f05
# ╠═23ee4944-c2be-4c16-bab7-0fc6c634feaf
# ╠═519e6698-90dd-4fa1-b2b3-15324a686d57
# ╠═1b8e5030-a381-45d3-b1dc-bc67f15e4c6e
# ╠═ae903a50-6220-4f8a-bfa0-7d19b7b6bdf1
# ╠═1d7ba988-4c20-4802-b2a4-2e7d8451594b
# ╠═35163d5f-ffae-48ea-b0df-6527d6457b3f
# ╠═484ebf25-9ef7-4699-8ca1-7544c937f011
# ╠═0a1781bd-edbb-4034-9409-c41dbfca4c81
# ╠═34c3e98d-2e66-4127-b147-08fb677dead0
# ╠═fdc9ef16-20b9-41c5-9168-9286d37fbd52
# ╠═11728674-54fc-448a-b576-8bd26e71fb59
# ╠═92dc2340-b19b-4a17-99ae-b87eef630c4f
# ╠═ee8a2383-211a-455e-8a33-dfd48bee763a
# ╠═5810f222-48cf-4c80-8558-0d497abf9eff
# ╠═cf3d0f35-2c2c-4598-85ce-592268d61496
# ╠═2179e8e1-d607-4010-a757-4a87d211788c
# ╠═5aaa83fa-fe54-4df9-bd9d-01d5049de593
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
