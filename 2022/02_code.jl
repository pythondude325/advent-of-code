### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ 864bba20-71fc-11ed-0562-735852004e09
const input = read("02_input.txt", String) 

# ╔═╡ 067686e1-e831-4a61-b4cc-32b8925793fc
parse_strat(l) = findfirst(==(l[1]), "ABC"), findfirst(==(l[3]), "XYZ")

# ╔═╡ 1319dfe8-906f-4b3a-82e5-5010304917dc
 const strats = parse_strat.(split(input, "\n")[1:end-1])

# ╔═╡ b9cedf36-7026-4cca-91c9-1b38310fd0c0
fight((a,b)) = mod(a-b+1,3)-1

# ╔═╡ 933b580e-c321-4b8d-9697-72dddb716982
score(strat) = -3 * fight(strat) + 3 + strat[2]

# ╔═╡ dc485a59-4aa3-4c43-b99f-1f02cd5802f8
const part1 = strats .|> score |> sum

# ╔═╡ 52f7c230-d9c7-4f02-ad05-256929392509
part2trans((a,b)) = (a,mod1(a+b-2,3))

# ╔═╡ ea7a96a9-fd4f-43aa-921a-94886bc3129d
const part2 = strats .|> part2trans .|> score |> sum

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═864bba20-71fc-11ed-0562-735852004e09
# ╠═1319dfe8-906f-4b3a-82e5-5010304917dc
# ╠═067686e1-e831-4a61-b4cc-32b8925793fc
# ╠═933b580e-c321-4b8d-9697-72dddb716982
# ╠═b9cedf36-7026-4cca-91c9-1b38310fd0c0
# ╠═dc485a59-4aa3-4c43-b99f-1f02cd5802f8
# ╠═52f7c230-d9c7-4f02-ad05-256929392509
# ╠═ea7a96a9-fd4f-43aa-921a-94886bc3129d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
