### A Pluto.jl notebook ###
# v0.19.20

using Markdown
using InteractiveUtils

# ╔═╡ 4ec79661-7be6-4a5b-89e0-a78d9b290a12
using StaticArrays

# ╔═╡ 4acbb269-9865-4aca-9e69-0bf287a60f95
using Graphs

# ╔═╡ bb965d3e-a738-4ea6-bf6e-b6f650dba92a
using AStarSearch

# ╔═╡ 451b54bc-aab8-4506-8104-b9742e63e864
using LinearAlgebra

# ╔═╡ 24ef2c64-7cfe-11ed-15bc-53d8d034e5a5
const input = readlines("16_input.txt")

# ╔═╡ 13153aa5-8c24-49fd-b16d-3cc7252e7b04
const test_input = split("\
Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II", "\n")

# ╔═╡ f94e2541-c6a2-4989-825e-cba3b6877f52
function parse_report(l) 
	m = match(r"Valve ([A-Z]{2}) has flow rate=(\d+); tunnels? leads? to valves? (([A-Z]{2}, )*[A-Z]{2})", l)
	m[1], parse(Int, m[2]), split(m[3], ", ")
end

# ╔═╡ a01ede48-b878-48c1-8411-d21922b4cb13
const reports = test_input .|> parse_report

# ╔═╡ aac25a5c-d483-49a0-b3d3-c8db936daf03
const aa = findfirst(((n,_,_),) -> n=="AA", reports)

# ╔═╡ 076b5810-f176-4cab-b91e-7638c86adeb2
function make_adjmat(rs)
	w = zeros(Int, length(rs))
	a = zeros(Int, length(rs), length(rs))
	
	for (i, (n,p,c))=enumerate(rs)
		for C=c
			y=findfirst(((N,_,_),) -> C == N, rs)
			a[i,y] = 1
		end
		w[i] = p
	end
		
	w, a
end

# ╔═╡ 7e5c9904-edce-438d-b833-9bc8422ef2e3
const pmat, emat = reports |> make_adjmat

# ╔═╡ 8ec3525f-c5f7-40c3-9ece-4274b1e8b2ea
function actions(t,p,m,emat)
	a = [(n,m) for n=findall(emat[:,p])]
	if(pmat[p] != 0 && m[p] == 0)
		M = copy(m)
		M[p] = t-1
		push!(a, (p,M))
	end
	a
end

# ╔═╡ b9d92b00-a3d6-4833-8ddc-c42ff21e2fe2
function solve4(pmat, emat, time, P)
	N = length(pmat)
	emat = Bool.(emat)

	path_lengths = floyd_warshall_shortest_paths(Graph(emat)).dists
	
	function neighbors((t,ps,ms))
		if(t <= 0)
			return []
		end
		
		Iterators.map(
			(a) -> (
				t-1,
				map(x -> x[1], a),
				reduce((a,b) -> max.(a,b), map(x -> x[2], a)),
			),
			Iterators.ProductIterator(map(p -> actions(t,p,ms,emat), ps))
		)
	end

	function h((t,ps,m),())
		unopened = findall(==(0), m)
		m=copy(m)
		m[unopened] .= max.(0, (t - 1) .- minimum(path_lengths[unopened,SVector(ps)], dims=2))
		-dot(min.(t,m),pmat)
	end

	start = (time, tuple(fill(aa, P)...), [time;zeros(Int, N-1)])
	res = astar(
		neighbors,
		start,
		(),
		cost=((_,_,m),_) -> -dot(m .> 0, pmat),
		heuristic=h,
		isgoal=((t,_,_), ()) -> t == 0,
		timeout=30,
		enable_closedset=false,
	)

	-res.cost
end

# ╔═╡ ce12a8f1-1b8d-47b7-ab06-f41dd82576ee
const part1 = solve4(pmat, emat, 30, 1)

# ╔═╡ 7692be0c-c902-40c1-87a7-ea90a001eb24
const part2 = solve4(pmat, emat, 26, 2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
AStarSearch = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[compat]
AStarSearch = "~0.6.2"
Graphs = "~1.7.4"
StaticArrays = "~1.5.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "661b02ab5db6bcf60f5865df600c5203f64ae50b"

[[deps.AStarSearch]]
deps = ["DataStructures"]
git-tree-sha1 = "b43cbd7712b6ad5efe5ef46463a1a82ac9949f09"
uuid = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
version = "0.6.2"

[[deps.ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "62e51b39331de8911e4a7ff6f5aaf38a5f4cc0ae"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Dates", "LinearAlgebra", "UUIDs"]
git-tree-sha1 = "00a2cccc7f098ff3b66806862d275ca3db9e6e5a"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.5.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Graphs]]
deps = ["ArnoldiMethod", "Compat", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "ba2d094a88b6b287bd25cfa86f301e7693ffae2f"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.7.4"

[[deps.Inflate]]
git-tree-sha1 = "5cd07aab533df5170988219191dfad0519391428"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.3"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "42324d08725e200c23d4dfb549e0d5d89dede2d2"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.10"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "StaticArraysCore", "Statistics"]
git-tree-sha1 = "ffc098086f35909741f71ce21d03dadf0d2bfa76"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.5.11"

[[deps.StaticArraysCore]]
git-tree-sha1 = "6b7ba252635a5eff6a0b0664a41ee140a1c9e72a"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.4.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═24ef2c64-7cfe-11ed-15bc-53d8d034e5a5
# ╠═13153aa5-8c24-49fd-b16d-3cc7252e7b04
# ╠═f94e2541-c6a2-4989-825e-cba3b6877f52
# ╠═a01ede48-b878-48c1-8411-d21922b4cb13
# ╠═7e5c9904-edce-438d-b833-9bc8422ef2e3
# ╠═aac25a5c-d483-49a0-b3d3-c8db936daf03
# ╠═ce12a8f1-1b8d-47b7-ab06-f41dd82576ee
# ╠═7692be0c-c902-40c1-87a7-ea90a001eb24
# ╠═b9d92b00-a3d6-4833-8ddc-c42ff21e2fe2
# ╠═8ec3525f-c5f7-40c3-9ece-4274b1e8b2ea
# ╠═076b5810-f176-4cab-b91e-7638c86adeb2
# ╠═4ec79661-7be6-4a5b-89e0-a78d9b290a12
# ╠═4acbb269-9865-4aca-9e69-0bf287a60f95
# ╠═bb965d3e-a738-4ea6-bf6e-b6f650dba92a
# ╠═451b54bc-aab8-4506-8104-b9742e63e864
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
