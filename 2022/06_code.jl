### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ b10cd5a4-7522-11ed-1769-ab2efb127d65
const input = read("06_input.txt", String)

# ╔═╡ 01e9d902-9c5f-4ba0-bd88-d2cf581e21dc
const letters = input |> collect

# ╔═╡ 80066144-0a81-40e0-a067-8f89761a447d
findunique(letters, N) =
	findfirst(
		(x -> length(unique(letters[x:x+(N-1)])) == N),
		(1:length(letters)-(N-1))
	)+(N-1)

# ╔═╡ 3e0b16c8-6e0d-4f7c-85c4-165ab1d6170a
const part1 = findunique(letters, 4)

# ╔═╡ 691bcee8-bf1d-412a-90c9-d6e922b8b3aa
const part2 = findunique(letters, 14)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═b10cd5a4-7522-11ed-1769-ab2efb127d65
# ╠═01e9d902-9c5f-4ba0-bd88-d2cf581e21dc
# ╠═80066144-0a81-40e0-a067-8f89761a447d
# ╠═3e0b16c8-6e0d-4f7c-85c4-165ab1d6170a
# ╠═691bcee8-bf1d-412a-90c9-d6e922b8b3aa
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
