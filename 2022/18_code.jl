### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ bbed2f21-f815-4e62-b5fc-53fa72c6cb69
const input = readlines("18_input.txt")

# ╔═╡ 69d6116e-7e91-11ed-3d7a-994206a7dbb3
const test_input = split("\
2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5", "\n")

# ╔═╡ 856c0d44-dca8-4c93-a5ce-1200098c6930
const CI = CartesianIndex

# ╔═╡ c7ee9fba-1dc0-4f31-88e7-c9fbb111b818
parse_points(l) = CI(Tuple(parse.(Int, split(l,","))))

# ╔═╡ 060e51a0-f600-4511-b3db-aecc15c5586d
const space = input .|> parse_points |> Set

# ╔═╡ cacb3f11-5270-4d54-9efe-545c88018686
const sides = [
	CI(1,0,0),
	CI(-1,0,0),
	CI(0,1,0),
	CI(0,-1,0),
	CI(0,0,1),
	CI(0,0,-1),
]

# ╔═╡ 3ed05e2f-df31-49e4-835f-b8fe9ee300fb
measure_sa(space) =
	sum(
		6 - count(∈(space), Ref(cube) .+ sides)
		for cube=space
	)

# ╔═╡ dd0223b7-2099-41f6-9862-b265a577e7ce
measure_sa(space)

# ╔═╡ aeacac23-acda-4f6a-9006-a6f7657d41fa
checkbounds(Bool, [1], CI(1))

# ╔═╡ 30575b98-0bf6-414f-b862-628ac3cf1a44
function void_bubbles(space)
	bounds = extrema(reduce(hcat, map(x -> collect(x.I), collect(space))), dims=2)
	a = zeros(
		Int,
		bounds[1][2] - bounds[1][1] + 3,
		bounds[2][2] - bounds[2][1] + 3,
		bounds[3][2] - bounds[3][1] + 3,
	)
	offset = CI(
		-bounds[1][1] + 1,
		-bounds[2][1] + 1,
		-bounds[3][1] + 1,
	)
	for cube=space
		a[cube + offset] = 1
	end

	queue = [CI(1, 1, 1)]
	while !isempty(queue)
		el = popfirst!(queue)
		for n=Ref(el) .+ sides
			if checkbounds(Bool, a, n) && a[n] == 0
				a[n] = 2
				push!(queue, n)
			end
		end
	end

	findall(!=(2), a) |> Set
end

# ╔═╡ e46a482e-f5b2-424f-a82d-cbe550626b90
void_bubbles(space) |> measure_sa

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═bbed2f21-f815-4e62-b5fc-53fa72c6cb69
# ╟─69d6116e-7e91-11ed-3d7a-994206a7dbb3
# ╠═c7ee9fba-1dc0-4f31-88e7-c9fbb111b818
# ╠═856c0d44-dca8-4c93-a5ce-1200098c6930
# ╠═060e51a0-f600-4511-b3db-aecc15c5586d
# ╠═cacb3f11-5270-4d54-9efe-545c88018686
# ╠═3ed05e2f-df31-49e4-835f-b8fe9ee300fb
# ╠═dd0223b7-2099-41f6-9862-b265a577e7ce
# ╠═e46a482e-f5b2-424f-a82d-cbe550626b90
# ╠═aeacac23-acda-4f6a-9006-a6f7657d41fa
# ╠═30575b98-0bf6-414f-b862-628ac3cf1a44
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
