### A Pluto.jl notebook ###
# v0.19.20

using Markdown
using InteractiveUtils

# ╔═╡ a7571420-7f59-11ed-227d-afa18d6ca212
using AStarSearch

# ╔═╡ 784b50e5-1c4f-46a9-9228-5b976de9e8f5
using LinearAlgebra

# ╔═╡ 835d8324-6e7e-4935-984f-d5c67105f225
using StaticArrays

# ╔═╡ c9f7a2de-3c8a-4eb1-84d3-26764deaf935
using Base.Threads

# ╔═╡ 6fd9a677-03b9-46d2-9351-0cb87c9de69d
const input = readlines("19_input.txt")

# ╔═╡ dda7f9b8-4e40-4c2c-bf4b-aacd170b7321
const test_input = split("\
Blueprint 1: \
  Each ore robot costs 4 ore. \
  Each clay robot costs 2 ore. \
  Each obsidian robot costs 3 ore and 14 clay. \
  Each geode robot costs 2 ore and 7 obsidian.\

Blueprint 2: \
  Each ore robot costs 2 ore. \
  Each clay robot costs 3 ore. \
  Each obsidian robot costs 3 ore and 8 clay. \
  Each geode robot costs 3 ore and 12 obsidian.\
", "\n")

# ╔═╡ ae5354d1-39ec-424f-bd59-2a4c63d64d6f
function parse_blueprint(l)
	
	p = split(l, " ")
	parse(Int8, p[7]), parse(Int8, p[13]), (parse(Int8, p[19]), parse(Int8, p[22])), (parse(Int8, p[28]), parse(Int8, p[31]))
end

# ╔═╡ 2cd6f902-b7f2-47ca-ad64-85e0c0644750
const ore_robot = SA{Int8}[1,0,0,0]

# ╔═╡ 19d5c12f-0d4e-49b5-8934-b8c4db11b503
const clay_robot = SA{Int8}[0,1,0,0]

# ╔═╡ 65320ce2-07c7-4f6e-8e80-5494fbb356ae
const obsidian_robot = SA{Int8}[0,0,1,0]

# ╔═╡ f797235a-3a08-4083-b2d1-0f3797a33ff2
const geode_robot = SA{Int8}[0,0,0,1]

# ╔═╡ 0bd1388e-5d89-431c-9d45-23ba42816201
const all_true = @SArray fill(true, 3)

# ╔═╡ 61026621-cc75-404b-b191-dad10473f0b8
function search(blueprint, time::Int32, robots::SVector{4,Int8}, res::SVector{4,Int8}, filt::SVector{3,Bool})::Int8
	if time == 0
		return res[4]
	end

	new_time::Int32 = time - 1
	new_res::SVector{4,Int8} = res + robots
	
	# Always buy a geode cracking robot if we can
	if (res[1] >= blueprint[4][1]) && (res[3] >= blueprint[4][2])
		return search(
			blueprint,
			new_time,
			robots + geode_robot,
			new_res - SA{Int8}[blueprint[4][1], 0, blueprint[4][2], 0],
			all_true,
		)
	end
	
	checks = SA{Bool}[
		res[1] >= blueprint[1],
		res[1] >= blueprint[2],
		(res[1] >= blueprint[3][1]) & (res[2] >= blueprint[3][2]),
	]

	# if you're gonna wait you can't just do a robot you could afford before
	wait_checks = checks .& filt

	
	best::Int8 = 0 # time * robots[4] + res[4]

	# no waiting if you can afford all types of robots
	if !(checks[1] && checks[2] && checks[3])
		best = max(best,
			search(blueprint, new_time, robots, new_res, (.! checks))
		)
	end
	
	if wait_checks[1]
		best = max(best, search(
			blueprint,
			new_time,
			robots + ore_robot,
			new_res - SA{Int8}[blueprint[1], 0, 0, 0],
			all_true,
		))
	end
	if wait_checks[2]
		best = max(best, search(
			blueprint,
			new_time,
			robots + clay_robot,
			new_res - SA{Int8}[blueprint[2], 0, 0, 0],
			all_true,
		))
	end
	if wait_checks[3]
		best = max(best, search(
			blueprint,
			new_time,
			robots + obsidian_robot,
			new_res - SA{Int8}[blueprint[3][1], blueprint[3][2], 0, 0],
			all_true
		))
	end
	
	return best
end

# ╔═╡ fae98eb9-7143-48d8-8d61-5fb53eece34e
function solve(blueprint; time=24)
	search(blueprint, Int32(time), SA{Int8}[1,0,0,0], SA{Int8}[0,0,0,0], all_true)
end

# ╔═╡ e54415fc-284a-4b08-9476-b37f1317dec3
const blueprints = input .|> parse_blueprint

# ╔═╡ 1c219675-bede-4de2-9b3d-dfdc676f96b2
const max_values = blueprints .|> solve

# ╔═╡ 22230455-5555-4c08-aa7f-dd6e632dcc0e
const qualities = max_values |> enumerate .|> prod

# ╔═╡ dac142dc-2474-49de-9816-e467d7063bad
const part1 = qualities |> sum

# ╔═╡ 74b8745d-99a7-4c2a-98a9-04e20a0a4c2a
const max_values2 = solve.(blueprints[1:3], time=32)

# ╔═╡ d711dd72-8b0f-411b-bf83-7e4fd79b8de7
const part2 = prod(max_values2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
AStarSearch = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[compat]
AStarSearch = "~0.6.2"
StaticArrays = "~1.5.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "7a65e14c5ee6fd5dfb551f0a0348448403c3c6e5"

[[deps.AStarSearch]]
deps = ["DataStructures"]
git-tree-sha1 = "b43cbd7712b6ad5efe5ef46463a1a82ac9949f09"
uuid = "e6cbe913-2b79-4cc5-848a-e3bbf8537828"
version = "0.6.2"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Dates", "LinearAlgebra", "UUIDs"]
git-tree-sha1 = "00a2cccc7f098ff3b66806862d275ca3db9e6e5a"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.5.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "StaticArraysCore", "Statistics"]
git-tree-sha1 = "ffc098086f35909741f71ce21d03dadf0d2bfa76"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.5.11"

[[deps.StaticArraysCore]]
git-tree-sha1 = "6b7ba252635a5eff6a0b0664a41ee140a1c9e72a"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.4.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═a7571420-7f59-11ed-227d-afa18d6ca212
# ╠═784b50e5-1c4f-46a9-9228-5b976de9e8f5
# ╠═835d8324-6e7e-4935-984f-d5c67105f225
# ╠═c9f7a2de-3c8a-4eb1-84d3-26764deaf935
# ╠═6fd9a677-03b9-46d2-9351-0cb87c9de69d
# ╟─dda7f9b8-4e40-4c2c-bf4b-aacd170b7321
# ╠═ae5354d1-39ec-424f-bd59-2a4c63d64d6f
# ╠═2cd6f902-b7f2-47ca-ad64-85e0c0644750
# ╠═19d5c12f-0d4e-49b5-8934-b8c4db11b503
# ╠═65320ce2-07c7-4f6e-8e80-5494fbb356ae
# ╠═f797235a-3a08-4083-b2d1-0f3797a33ff2
# ╠═0bd1388e-5d89-431c-9d45-23ba42816201
# ╠═61026621-cc75-404b-b191-dad10473f0b8
# ╠═fae98eb9-7143-48d8-8d61-5fb53eece34e
# ╠═e54415fc-284a-4b08-9476-b37f1317dec3
# ╠═1c219675-bede-4de2-9b3d-dfdc676f96b2
# ╠═22230455-5555-4c08-aa7f-dd6e632dcc0e
# ╠═dac142dc-2474-49de-9816-e467d7063bad
# ╠═74b8745d-99a7-4c2a-98a9-04e20a0a4c2a
# ╠═d711dd72-8b0f-411b-bf83-7e4fd79b8de7
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
