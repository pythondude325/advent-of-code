### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ 608c76ac-7121-11ed-2236-19298f829a1d
const input = readlines("01_input.txt")

# ╔═╡ eeb61a55-a254-4f4c-95a8-e97b97beb5eb
const breaks = [0; findall(==(""), input); length(input)+1]

# ╔═╡ b0412754-26b3-416f-ab16-417ba3634f9d
const elves = [parse.(Int, input[a+1:b-1]) for (a,b)=zip(breaks[1:end-1],breaks[2:end])]

# ╔═╡ 5bf61dc2-303c-456b-873a-514f724e3273
const elf_sums = sum.(elves)

# ╔═╡ 31bfab0e-1684-4da1-bbbb-b238dd49a574
sort(elf_sums)

# ╔═╡ 1ce96a37-934c-47ca-b180-6620019e3042
maximum(sum, elves)

# ╔═╡ e0853995-7a37-484d-8f4b-a68f3795d317
sort(elves, by=sum, rev=true)[1:3] .|> sum |> sum

# ╔═╡ 016587d1-fd6c-4ac8-85e5-8c259ef8d40a


# ╔═╡ 1ecf0bfd-9f99-42cf-9325-ca5c0fd320a7
mod((invmod(9, 2^32) * 5), 2^32)

# ╔═╡ efe3f9ea-bc50-4c10-afac-778db84fe167
const f = UInt32(477218589)

# ╔═╡ d90f95fe-fc62-4f22-bc80-4e5ca3f6f351
UInt32(15) * f

# ╔═╡ e94ac6de-d097-48c5-ae4c-dc2b5edcb1a3
(2^33 ÷ 9) + 1

# ╔═╡ b36cb325-d0b3-4f62-b3c0-2274929b8f7b
invmod(9,2^32)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═608c76ac-7121-11ed-2236-19298f829a1d
# ╠═eeb61a55-a254-4f4c-95a8-e97b97beb5eb
# ╠═b0412754-26b3-416f-ab16-417ba3634f9d
# ╠═5bf61dc2-303c-456b-873a-514f724e3273
# ╠═31bfab0e-1684-4da1-bbbb-b238dd49a574
# ╠═1ce96a37-934c-47ca-b180-6620019e3042
# ╠═e0853995-7a37-484d-8f4b-a68f3795d317
# ╠═016587d1-fd6c-4ac8-85e5-8c259ef8d40a
# ╠═1ecf0bfd-9f99-42cf-9325-ca5c0fd320a7
# ╠═efe3f9ea-bc50-4c10-afac-778db84fe167
# ╠═d90f95fe-fc62-4f22-bc80-4e5ca3f6f351
# ╠═e94ac6de-d097-48c5-ae4c-dc2b5edcb1a3
# ╠═b36cb325-d0b3-4f62-b3c0-2274929b8f7b
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
