### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ f96e4f62-46fd-414b-8f19-d39513563a47
const input = readlines("20_input.txt")

# ╔═╡ a7e5aafa-8026-11ed-153d-3dfc644d2890
const test_input = split("\
1
2
-3
3
-2
0
4", "\n")

# ╔═╡ 9112ffcd-0c2a-4ad1-b320-7c1835fe3037
const file = parse.(Int, input)

# ╔═╡ 96612fed-ca13-4cba-818e-c05552f78145
const test_file = parse.(Int, test_input)

# ╔═╡ 2c167499-c461-41c4-96c4-0a97664487a4
function swap_item_right!(A, i)
	i = mod1(i, length(A))
	j = mod1(i+1, length(A))
	A[i], A[j] = A[j], A[i]
end

# ╔═╡ 925e4d29-a7e5-423d-b995-9e5488e01e69
swap_item_left!(A, i) = swap_item_right!(A, i-1)

# ╔═╡ e8611240-97e9-4740-b943-83fdb15f5ad0
function shift_item!(A, i, n)
	N = length(A)
	d = mod1(i + n, N)
	d = mod1(d + fld1(i + n, N) - 1, N)
	
	if i < d
		for I=i:d-1
			swap_item_right!(A, I)
		end
	elseif d < i
		for I=i:-1:d+1
			swap_item_left!(A, I)
		end
	end
end

# ╔═╡ a99e56f8-908b-4b3f-afe1-d8349ffd7e8b
function mix(ns; rounds=1)
	N = length(ns)
	is = collect(1:N)

	for _=1:rounds
		for i=1:N
			index = findnext(==(i), is, 1)
			shift_item!(is, index, mod(ns[i], N-1))
		end
	end

	ns[is]
end

# ╔═╡ 0cda3c51-da60-49fb-8bfc-84672a5ccea7
function get_result(file; key=1, rounds=1)
	file = file .* key
	mixed = mix(file, rounds=rounds)
	n = length(mixed)
	offset = findfirst(iszero, mixed)
	mixed[mod1.(offset .+ [1000,2000,3000], n)]
end

# ╔═╡ 733982a8-eb91-411a-8f36-c3090c99652f
const part1 = get_result(file) |> sum

# ╔═╡ 5e030904-f0cb-4873-9f71-57819f074fac
const part2 = get_result(file, key=811589153, rounds=10) |> sum

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═f96e4f62-46fd-414b-8f19-d39513563a47
# ╠═a7e5aafa-8026-11ed-153d-3dfc644d2890
# ╠═9112ffcd-0c2a-4ad1-b320-7c1835fe3037
# ╠═96612fed-ca13-4cba-818e-c05552f78145
# ╠═2c167499-c461-41c4-96c4-0a97664487a4
# ╠═925e4d29-a7e5-423d-b995-9e5488e01e69
# ╠═e8611240-97e9-4740-b943-83fdb15f5ad0
# ╠═a99e56f8-908b-4b3f-afe1-d8349ffd7e8b
# ╠═0cda3c51-da60-49fb-8bfc-84672a5ccea7
# ╠═733982a8-eb91-411a-8f36-c3090c99652f
# ╠═5e030904-f0cb-4873-9f71-57819f074fac
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
