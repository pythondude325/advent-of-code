### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ d036be4e-75eb-11ed-04e7-1da3e76e12b6
const input = readlines("07_input.txt")

# ╔═╡ b271bec2-045f-4f90-9e22-77ca31c27d82
const test_input = split("\
\$ cd /
\$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
\$ cd a
\$ ls
dir e
29116 f
2557 g
62596 h.lst
\$ cd e
\$ ls
584 i
\$ cd ..
\$ cd ..
\$ cd d
\$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k\
", "\n")

# ╔═╡ 07a55835-0862-47b3-9a22-6bd05e0b6101
function find_inputs(input) 
	current_line = 1
	current_path = String[]
	files = Dict{String,Any}()

	while current_line <= length(input)
		cd_m = match(r"^\$ cd (.*)$", input[current_line])
		if !isnothing(cd_m)
			path = cd_m[1]
			absolute = startswith(path, "/")
			if absolute 
				if length(cd_m[1][2:end]) == 0
					empty!(current_path)
				else
					current_path = splitpath(cd_m[1][2:end])
				end
				# println("abs $(current_path)")
			else
				path_parts = splitpath(cd_m[1])
				for part=path_parts
					if part == "."

					elseif part == ".."
						pop!(current_path)
					else
						push!(current_path, part)
					end
				end
			end
			# println("current path: $(current_path)")

			current_line += 1
		elseif input[current_line] == "\$ ls"
			current_line += 1

			dir = files
			for part=current_path
				dir=dir[part]
			end
			
			while current_line <= length(input) && !startswith(input[current_line], "\$")
				size, name = split(input[current_line])
				if size == "dir"
					dir[name] = Dict{String,Any}()
				else
					size_i = parse(Int, size)
					dir[name] = size_i
				end
				current_line += 1
			end

		end
	end

	files
	
	
end

# ╔═╡ 42fcd155-a56c-40f3-ba37-f95ff3267a4d
function get_dir_sizes(files)
	sizes = Dict{Vector{String},Int}()


	function get_size(files::Dict{String,Any}, path)
		total = sum(get_size(s, vcat(path, [n])) for (n,s)=files)
		sizes[path] = total
		total
	end
	function get_size(size::Int, path)
		# sizes[path] = size
		size
	end

	get_size(files, [])

	sizes
end

# ╔═╡ 2a61e8b7-a2e9-4bcc-962a-b390993910c2
const dirs = input |> find_inputs |> get_dir_sizes

# ╔═╡ c7e9b70f-0d25-4f5b-96b1-fa160abe7d8f
const small_dirs = filter(((_,s),) -> s <= 100000, dirs) 

# ╔═╡ 4366c9c2-8e01-45e8-8377-f557992c6680
const part1 = sum(s for (_,s)=small_dirs)

# ╔═╡ 818a3f6f-ca40-4686-9bd5-836eb5d36431
const total_space = 70000000

# ╔═╡ aff1df4b-3cf1-422e-81cc-9383a28cd237
const needed_free = 30000000

# ╔═╡ 80a45eb2-fc1c-44a1-a452-df3161f76f98
const needed_delete = needed_free - (total_space - dirs[[]])

# ╔═╡ f62d1406-21c0-4890-b5e7-3e07852cbf08
const deletable = filter(((_,s),) -> s >= needed_delete, dirs) |> collect

# ╔═╡ b8c77f35-973e-4689-b3d2-a667da3baf97
const part2 = sort(deletable, by=(((n,s),) -> s))[1][2]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╟─d036be4e-75eb-11ed-04e7-1da3e76e12b6
# ╟─b271bec2-045f-4f90-9e22-77ca31c27d82
# ╠═07a55835-0862-47b3-9a22-6bd05e0b6101
# ╠═42fcd155-a56c-40f3-ba37-f95ff3267a4d
# ╠═2a61e8b7-a2e9-4bcc-962a-b390993910c2
# ╠═c7e9b70f-0d25-4f5b-96b1-fa160abe7d8f
# ╠═4366c9c2-8e01-45e8-8377-f557992c6680
# ╠═818a3f6f-ca40-4686-9bd5-836eb5d36431
# ╠═aff1df4b-3cf1-422e-81cc-9383a28cd237
# ╠═80a45eb2-fc1c-44a1-a452-df3161f76f98
# ╠═f62d1406-21c0-4890-b5e7-3e07852cbf08
# ╠═b8c77f35-973e-4689-b3d2-a667da3baf97
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
