### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ 799ad458-7910-11ed-3d87-dd652804750f
const input = read("11_input.txt", String)

# ╔═╡ 3e8b1b79-5281-47e0-ac12-b15e5754c38f
const test_input = "\
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
"

# ╔═╡ 84ad801b-ad04-4602-987d-c2deb31bf029
struct Monkey
	func
	mod::Int
	true_dest::Int
	false_dest::Int
end

# ╔═╡ 6cc5957c-7cf7-4d68-8038-4acc64c73487
function parse_monkey(i)
	l = split(i, "\n")
	starting_items = parse.(Int, split(split(l[2],":")[2], ", "))
	equ = split(split(l[3],":")[2][8:end], " ")
	
	operand1 = equ[1] == "old" ? :old : parse(Int, equ[1])
	operator = equ[2] == "*" ? :* : :+
	operand2 = equ[3] == "old" ? :old : parse(Int, equ[3])

	func = eval(:(old -> $operator($operand1, $operand2)))
	
	test = parse(Int, split(l[4], " ")[end])
	dt = parse(Int, split(l[5], " ")[end])+1
	df = parse(Int, split(l[6], " ")[end])+1
	starting_items, Monkey(func, test, dt, df)
end

# ╔═╡ b1a15af4-7b7f-4c79-84fb-f2925b07472f
function parse_monkeys(i)
	parse_monkey.(split(i, "\n\n"))
	
end

# ╔═╡ 458855d5-a3c4-44ad-b059-2a7ac303e30f
function run_monkey(monkey::Monkey, n::Int)
	w = monkey.func(n) ÷ 3
	if w % monkey.mod == 0
		w, monkey.true_dest
	else
		w, monkey.false_dest
	end
end

# ╔═╡ a109ff57-8fb8-4617-a16a-fda74818c1c1
function run_monkeys(monkeys::Vector{Monkey}, items::Vector{Vector{Int}}; run=run_monkey, R=20)
	N = length(monkeys)
	counts = [0 for _=1:N]
	
	# rounds = [deepcopy(items)]
	for round=1:R
		for i=1:N
			monkey = monkeys[i]
			round_items = items[i]
			items[i] = []

			for round_item=round_items
				w, dest = run(monkey, round_item)
				counts[i] += 1
				push!(items[dest], w)
			end
		end
		# push!(rounds, deepcopy(items))
	end

	sort(counts, rev=true)[1:2] |> prod
end

# ╔═╡ b04a8b7a-4249-40ac-96ad-191986a30592
const monkeys = input |> parse_monkeys

# ╔═╡ 43a4beb0-53d4-4144-81ea-cb7c05639565
const items = deepcopy(map(x -> x[1], monkeys))

# ╔═╡ fb81356a-0866-4138-80c9-059a5f043617
const monkeyarr = map(x -> x[2], monkeys)

# ╔═╡ fe10ec0d-8a39-4560-a297-fa027718f27e
run_monkeys(monkeyarr, deepcopy(items))

# ╔═╡ e3f6bc63-a249-484a-88dc-b5e43edf2093
const worry_mod = [m.mod for m=monkeyarr] |> prod

# ╔═╡ 0fb05fd5-0c3d-4466-9104-9bcea32c605c
function run_monkey2(monkey::Monkey, n::Int)
	w = monkey.func(n) % worry_mod
	if w % monkey.mod == 0
		w, monkey.true_dest
	else
		w, monkey.false_dest
	end
end

# ╔═╡ 363ca82a-2aa8-4bcd-8cd5-b1978b78f930
run_monkeys(monkeyarr, deepcopy(items), R=10_000, run=run_monkey2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═799ad458-7910-11ed-3d87-dd652804750f
# ╟─3e8b1b79-5281-47e0-ac12-b15e5754c38f
# ╠═6cc5957c-7cf7-4d68-8038-4acc64c73487
# ╠═b1a15af4-7b7f-4c79-84fb-f2925b07472f
# ╠═84ad801b-ad04-4602-987d-c2deb31bf029
# ╠═458855d5-a3c4-44ad-b059-2a7ac303e30f
# ╠═a109ff57-8fb8-4617-a16a-fda74818c1c1
# ╠═b04a8b7a-4249-40ac-96ad-191986a30592
# ╠═43a4beb0-53d4-4144-81ea-cb7c05639565
# ╠═fb81356a-0866-4138-80c9-059a5f043617
# ╠═fe10ec0d-8a39-4560-a297-fa027718f27e
# ╠═363ca82a-2aa8-4bcd-8cd5-b1978b78f930
# ╠═0fb05fd5-0c3d-4466-9104-9bcea32c605c
# ╠═e3f6bc63-a249-484a-88dc-b5e43edf2093
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
