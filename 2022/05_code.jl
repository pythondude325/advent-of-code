### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ ec5f99ba-7458-11ed-3902-d98b84c41102
const input = read("05_input.txt", String)

# ╔═╡ c993ab72-080b-4e33-a05c-6b54dc9adc78
const test_input =
"    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
"

# ╔═╡ 1ffdcc85-6a0e-4fea-b860-4fb61321a4c7
function matchi(i)
	m=match(r"move (\d+) from (\d) to (\d)", i)
	parse.(Int, (m[1], m[2], m[3]))
end

# ╔═╡ 3cfa8cf5-7e34-4903-9e72-49c43aebf042
function parse_input(l, n=3)
	lines = split(l,"\n")[1:end-1]
	s = findfirst(==(""), lines)

	stacks = [[] for _=1:n]
	for i=1:s-2
		line = lines[i]
		boxes = get.(line,2:4:(-2+4n),' ')

		for j=1:n
			if boxes[j] != ' '
				push!(stacks[j], boxes[j])
			end
		end
	end
	for i=1:n
		reverse!(stacks[i])
	end

	stacks, matchi.(lines[s+1:end])
end

# ╔═╡ ba21b4e1-9ba0-4a08-a746-56abf23abf67
function run_process!((stacks, instructions))
	for (n,f,t)=instructions
		for _=1:n
			push!(stacks[t], pop!(stacks[f]))
		end
	end
	[stack[end] for stack=stacks]
end

# ╔═╡ 5f26efb2-a75e-40bc-9c82-c6d639395da3
const part1 = parse_input(input, 9) |> run_process! |> join

# ╔═╡ 761b6e97-d1a4-46bd-a3aa-3d5d482d0192
function run_process2!((stacks, instructions))
	for (n,f,t)=instructions
		append!(stacks[t], stacks[f][end-n+1:end])
		stacks[f] = stacks[f][1:end-n]		
	end
	[stack[end] for stack=stacks]
end

# ╔═╡ 8cf2343b-a90d-4b39-8efa-99cc09a38a75
const part2 = parse_input(input, 9) |> run_process2! |> join

# ╔═╡ 51395455-0949-4e6c-b4cf-8bbb297adc74
get("abc", 10, 3)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═ec5f99ba-7458-11ed-3902-d98b84c41102
# ╠═c993ab72-080b-4e33-a05c-6b54dc9adc78
# ╠═3cfa8cf5-7e34-4903-9e72-49c43aebf042
# ╠═1ffdcc85-6a0e-4fea-b860-4fb61321a4c7
# ╠═5f26efb2-a75e-40bc-9c82-c6d639395da3
# ╠═8cf2343b-a90d-4b39-8efa-99cc09a38a75
# ╠═ba21b4e1-9ba0-4a08-a746-56abf23abf67
# ╠═761b6e97-d1a4-46bd-a3aa-3d5d482d0192
# ╠═51395455-0949-4e6c-b4cf-8bbb297adc74
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
