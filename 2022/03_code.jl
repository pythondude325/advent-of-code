### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ d0ecd1f0-72c6-11ed-3f4a-074acc5fe3d3
const input = read("03_input.txt", String)

# ╔═╡ 0879fa62-370a-4ebc-a908-bfe6ab13c6a7
priority(i) = findfirst(==(i), ['a':'z'; 'A':'Z'])

# ╔═╡ 2b3a189e-d2b4-4c2e-97dd-9857607c0976
function parse_sack(s)
	p = length(s) ÷ 2
	priority.(collect(s[1:p])), priority.(collect(s[p+1:end]))
end

# ╔═╡ 945ea8c1-d967-4b0c-8437-e4db0f1c4942
const sacks = input |> (i -> split(i, "\n")[1:end-1]) .|> parse_sack

# ╔═╡ 9c23aa33-39d2-4395-b64c-a52cbee9d43a
common((a,b)) = a ∩ b

# ╔═╡ 49171dcf-d6eb-487d-bd73-fd0c09b7de06
const part1 = sacks .|> common .|> first |> sum

# ╔═╡ 799fb229-7304-4cb8-905d-aa8328ddf0ea
badge(x) = reduce(∩, [a ∪ b for (a,b)=x]) |> first

# ╔═╡ a5dfee24-da66-4b51-98cd-02a7b4d5f011
const part2 = reshape(sacks, 3, :) |> eachcol .|> badge |> sum

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═d0ecd1f0-72c6-11ed-3f4a-074acc5fe3d3
# ╠═0879fa62-370a-4ebc-a908-bfe6ab13c6a7
# ╠═2b3a189e-d2b4-4c2e-97dd-9857607c0976
# ╠═945ea8c1-d967-4b0c-8437-e4db0f1c4942
# ╠═9c23aa33-39d2-4395-b64c-a52cbee9d43a
# ╠═49171dcf-d6eb-487d-bd73-fd0c09b7de06
# ╠═799fb229-7304-4cb8-905d-aa8328ddf0ea
# ╠═a5dfee24-da66-4b51-98cd-02a7b4d5f011
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
