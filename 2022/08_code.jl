### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ a8bf6374-76b4-11ed-2606-9d440a18eab3
const input = readlines("08_input.txt")

# ╔═╡ 625aa87f-183c-410d-a844-1ef494987f52
const test_input = split("\
30373
25512
65332
33549
35390", "\n")

# ╔═╡ db3287da-8959-4016-a5d1-cd2ee5df6769
parse_grid(l) = map(x -> parse(Int, x), reduce(hcat, [collect(i) for i=l]))

# ╔═╡ 856fcb12-84ee-4f11-a35f-4d479567b801
const grid = test_input |> parse_grid

# ╔═╡ e0c6a6ed-c06c-4795-b43e-c3eade72ad4d
rotation_map(f, A) =
	f(grid),
	(A |> rotl90 |> f |> rotr90),
	(A |> rot180 |> f |> rot180),
	(A |> rotr90 |> f |> rotl90)

# ╔═╡ bd1ca98f-b200-4e29-93c5-cf26a252e6fd
test_vis(g) =
	accumulate(
		((a,_),b) -> (max(a,b), a < b),
		g,
		dims=1,
		init=(-1,true)
	) .|> (x -> x[2])

# ╔═╡ 4507ca5e-3635-4ac3-8f10-1d07f97fa2bf
test_all_vis(g) = reduce(.|, rotation_map(test_vis, g))

# ╔═╡ 545d8261-58cd-4fc5-8cce-fe0b9e3f2ca7
const part1 = grid |> test_all_vis |> sum

# ╔═╡ d6f99959-5039-49c5-a93c-addaf836a976
score_v(v) =
	[something(findfirst(v[i+1:end] .>= v[i]), length(v[i+1:end])) for i=1:length(v)]

# ╔═╡ e412b234-7cb4-47bd-8cb7-1d58bc04d074
score_tree(g) =
	mapslices(score_v, grid, dims=1)

# ╔═╡ 38062254-cd09-4788-8674-03e73fa87048
score_all(g) =
	reduce(.*, rotation_map(score_tree, grid))

# ╔═╡ 8acb6efe-f2ef-4c1f-be30-3022c4f2a7c7
const part2 = grid |> score_all |> maximum

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═a8bf6374-76b4-11ed-2606-9d440a18eab3
# ╠═625aa87f-183c-410d-a844-1ef494987f52
# ╠═db3287da-8959-4016-a5d1-cd2ee5df6769
# ╠═856fcb12-84ee-4f11-a35f-4d479567b801
# ╠═e0c6a6ed-c06c-4795-b43e-c3eade72ad4d
# ╠═bd1ca98f-b200-4e29-93c5-cf26a252e6fd
# ╠═4507ca5e-3635-4ac3-8f10-1d07f97fa2bf
# ╠═545d8261-58cd-4fc5-8cce-fe0b9e3f2ca7
# ╠═d6f99959-5039-49c5-a93c-addaf836a976
# ╠═e412b234-7cb4-47bd-8cb7-1d58bc04d074
# ╠═38062254-cd09-4788-8674-03e73fa87048
# ╠═8acb6efe-f2ef-4c1f-be30-3022c4f2a7c7
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
