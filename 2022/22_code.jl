### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ a2b63978-81ca-11ed-0da2-5529ffd93d34
const real_input = readlines("22_input.txt")

# ╔═╡ fb831c34-904b-4d3f-9139-9a408428de92
const test_input = split(
"        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5", "\n")

# ╔═╡ 267c1acb-42ae-4205-aac1-f9c6eb814022
abstract type Instruction end

# ╔═╡ fec2fec3-cb6d-40e1-88a7-b3fd0df8e50e
struct Forward <: Instruction
	distance::Int
end

# ╔═╡ 87256a8c-00e5-4389-8dc7-4bb102eaac4c
struct TurnLeft <: Instruction end

# ╔═╡ 7abcb281-5bfe-4612-bd2e-98a949160fa1
struct TurnRight <: Instruction end

# ╔═╡ 6eb7fec9-c110-4812-bb7b-95df922ab122
function parse_instructions(l)::Vector{Instruction}
	[
		begin
			if m[1] == "L"
				TurnLeft()
			elseif m[1] == "R"
				TurnRight()
			else
				Forward(parse(Int, m[1]))
			end
		end
		for m=eachmatch(r"(L|R|[0-9]+)", l)
	]
end

# ╔═╡ c7bd4da6-33b3-4831-b489-ed5512174f54


# ╔═╡ 6da97b7e-3479-43b1-9b84-1c8f24323bea
const right_turn = Complex{Int}(0,1)

# ╔═╡ 86bda3c6-c468-40fd-a4e7-46553697b206
const left_turn = Complex{Int}(0,-1)

# ╔═╡ 915685b0-2111-44e6-9ac9-39e5c5765ba2
abstract type Tile end

# ╔═╡ b20b6021-3109-4f10-b479-a27105ed4d5e
struct Space <: Tile end

# ╔═╡ faf56c59-5fa2-44f5-b56e-73f7c311b21e
struct Wall <: Tile end

# ╔═╡ 3db10856-e07f-472c-9f6c-1fa8658917a3
function parse_grid(lines)
	start = missing
	space = Dict{Complex{Int}, Tile}()
	for (i,line)=enumerate(lines)
		for (j,c)=enumerate(collect(line))
			pos = Complex{Int}(j,i)
			if ismissing(start) && c == '.'
				start = pos
			end
			if c == '.'
				space[pos] = Space()
			elseif c == '#'
				space[pos] = Wall()
			end
		end
	end
	space, start
end

# ╔═╡ 1c6bf1a5-5e82-408a-b415-45043b218856
const Position = Complex{Int}

# ╔═╡ d879f3d9-d76e-45b0-a77d-064c7e56763e
const State = Tuple{Position,Position}

# ╔═╡ 8441ade9-aa8e-46e0-8eb5-ec2bc1529ab6
const turn_left = Position(0,-1)

# ╔═╡ 9b2a9689-4d8a-4dfe-8495-fe0e016dc2ab
const turn_right = Position(0,1)

# ╔═╡ c908f498-4cb6-41eb-989c-b502ec2deb59
function walk_wrap(
	cache::Dict{State,State},
	space::Dict{Position,Tile},
	pos::Position,
	heading::Position,
)::State
	if haskey(cache, (pos, heading))
		cache[(pos,heading)]
	else
		new_pos = pos
		while haskey(space, new_pos)
			# println("tracking back at $new_pos")
			new_pos -= heading
		end
		new_pos += heading
		# println("finished at $new_pos")
		cache[(pos,heading)] = (new_pos, heading)
		(new_pos, heading)
	end
end

# ╔═╡ fb77e755-607a-427a-ab11-8e8d813fd0e2
heading_to_number(x) =
	Dict([
		Position(1,0) => 0,
		Position(0,1) => 1,
		Position(-1,0) => 2,
		Position(0,-1) => 3
	])[x]

# ╔═╡ 78388f8f-c764-475e-9da7-4e038aa2a9b6
const input = real_input

# ╔═╡ a4a41e46-fd2c-4b6f-9874-ff07528466dd
const instructions = parse_instructions(input[end])

# ╔═╡ 8c50ae87-299a-4f05-94cc-41c6b4f5dc49
const space, start_pos = parse_grid(input[1:end-2])

# ╔═╡ 9130174b-460d-49f2-8c1a-3b9789ecbeb5
const f= 50

# ╔═╡ d1b07b71-f173-430e-a1c7-4935b896d403
function draw_debug_path(space::Dict{Position,Tile}, states::Vector{State})
	cmax = maximum(((p,_),) -> real(p), space)
	rmax = maximum(((p,_),) -> imag(p), space)

	v = fill(' ', cmax, rmax)
	for (p,tile)=space
		if isa(tile, Wall)
			v[real(p), imag(p)] = '#'
		else
			v[real(p), imag(p)] = '.'
		end
	end

	for (p, heading)=states
		c = ">v<^"[heading_to_number(heading) + 1]
		v[real(p), imag(p)] = c
	end

	println(join(String.(eachcol(v)), "\n"))
end

# ╔═╡ d313adc6-6a9a-406c-bcb9-bd26072b8862
function walk_grid(space, start, instructions; debug=false, wrap_cache=Dict{State,State}())	
	pos = start
	heading = Position(1,0)
	states = State[(pos, heading)]

	function walk_instruction(::TurnLeft)
		heading *= turn_left
	end
	function walk_instruction(::TurnRight)
		heading *= turn_right
	end
	function walk_instruction(f::Forward)
		for i=1:f.distance
			next_pos = pos + heading
			next_heading = heading
			if !haskey(space, next_pos)
				next_pos, next_heading = walk_wrap(wrap_cache, space, pos, heading)
				if debug
					println("warped from $pos to $next_pos")
				end
			end

			next_tile = space[next_pos]
			if next_tile == Wall()
				break
			else
				pos = next_pos
				heading = next_heading
				push!(states, (pos, heading))
			end
		end
	end

	for instruction=instructions
		# println("at $(pos) with heading $(heading)")
		walk_instruction(instruction)
		push!(states, (pos, heading))
	end

	if debug
		draw_debug_path(space, states)
	end

	1000imag(pos) + 4real(pos) + heading_to_number(heading)
end

# ╔═╡ 5454fc90-0e06-4340-a359-f03e02ca04e6
const part1 = walk_grid(space, start_pos, instructions)

# ╔═╡ dd2da274-2bf1-4a40-90a4-f9dd2d06dc65
pround(z) = Position(round(Int, real(z)), round(Int, imag(z)))

# ╔═╡ 1ea0b8a5-d2e2-4383-ba37-13d4c0f979a3
psign(z) = sign(real(z)) + im*sign(imag(z))

# ╔═╡ 34322f9f-7a96-4686-8afd-58b7e6ad01ee
pabs(z) = round(Int, abs(z))

# ╔═╡ c4d064ea-4253-415e-ab70-0436e60479bb
function stitch_edge!(cache, (e1a,e1b),(e2a,e2b), h)
	
	e1d = e1b - e1a
	e2d = e2b - e2a

	edge_length = pabs(e1d)

	# e1hc = pround(e1d / e2d)
	# println(e1hc)
	hc = pround(e2d / e1d)
	# println(e2hc)
	
	e1h = psign(e1d)
	e2h = psign(e2d)

	if h * hc ∉ [Position(0,1), Position(0,-1), Position(1,0), Position(-1,0)]
		error("Invalid edge with hc of $hc ($e1d / $e2d)")
	end
	
	for i=0:(edge_length-1)
		e1p = e1a + i*e1h
		e2p = e2a + i*e2h

		cache[(e1p, h)] = (e2p, h * hc)
		cache[(e2p, -h * hc)] = (e1p, -h)
	end
end

# ╔═╡ 09cc8944-e088-42d7-aa07-0082b5717a0d
function part2_test_cache()
	cache = Dict{State, State}()
	stitch_edge!(
		cache,
		Position(12,5) => Position(12,8),
		Position(16,9) => Position(13,9),
		Position(1,0)
	)
	stitch_edge!(
		cache,
		Position(9, 4) => Position(9, 1),
		Position(8, 5) => Position(5, 5),
		Position(-1, 0),
	)
	stitch_edge!(
		cache,
		Position(9, 1) => Position(12, 1),
		Position(4, 5) => Position(1, 5),
		Position(0, -1),
	)
	stitch_edge!(
		cache,
		Position(12, 4) => Position(12, 1),
		Position(16,9) => Position(16, 16),
		Position(1, 0),
	)
	stitch_edge!(
		cache,
		Position(1, 5) => Position(1, 8),
		Position(16, 16) => Position(13, 16),
		Position(-1, 0),
	)
	stitch_edge!(
		cache,
		Position(9, 12) => Position(12, 12),
		Position(4, 8) => Position(1, 8),
		Position(0, 1),
	)
	stitch_edge!(
		cache,
		Position(9, 9) => Position(9, 12),
		Position(8, 8) => Position(5, 8),
		Position(-1, 0),
	)
	cache

end

# ╔═╡ ff31a03c-c3de-4ca1-b205-d011df2ffaad
function part2_real_cache()
	cache = Dict{State, State}()
	stitch_edge!(
		cache,
		Position(2f + 1, 1f) => Position(3f, 1f),
		Position(2f, 1f + 1) => Position(2f, 2f),
		Position(0,1),
	)
	stitch_edge!(
		cache,
		Position(1f + 1, 2f) => Position(1f + 1, 1f + 1),
		Position(1f, 2f + 1) => Position(0f + 1, 2f + 1),
		Position(-1,0),
	)
	stitch_edge!(
		cache,
		Position(1f + 1, 3f) => Position(2f, 3f),
		Position(1f, 3f + 1) => Position(1f, 4f),
		Position(0,1),
	)
	stitch_edge!(
		cache,
		Position(2f,2f + 1) => Position(2f, 3f),
		Position(3f,1f) => Position(3f,0f + 1),
		Position(1,0),
	)
	stitch_edge!(
		cache,
		Position(1f,4f) => Position(0f + 1,4f),
		Position(3f,0f + 1) => Position(2f + 1,0f + 1),
		Position(0,1),
	)
	stitch_edge!(
		cache,
		Position(2f,0f + 1) => Position(1f + 1,0f + 1),
		Position(0f + 1,4f) => Position(0f + 1,3f + 1),
		Position(0,-1)
	)
	stitch_edge!(
		cache,
		Position(1f + 1,1f) => Position(1f + 1,0f + 1),
		Position(0f + 1,3f) => Position(0f + 1,2f + 1),
		Position(-1,0),
	)
	cache
end

# ╔═╡ da33d5c9-ff20-42b4-bc14-cc8368729627
const part2_cache = part2_real_cache()

# ╔═╡ b36e235e-4770-4467-99e3-a93f44ee9427
const part2 = walk_grid(space, start_pos, instructions, wrap_cache=part2_cache)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.3"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═a2b63978-81ca-11ed-0da2-5529ffd93d34
# ╠═fb831c34-904b-4d3f-9139-9a408428de92
# ╠═267c1acb-42ae-4205-aac1-f9c6eb814022
# ╠═fec2fec3-cb6d-40e1-88a7-b3fd0df8e50e
# ╠═87256a8c-00e5-4389-8dc7-4bb102eaac4c
# ╠═7abcb281-5bfe-4612-bd2e-98a949160fa1
# ╠═6eb7fec9-c110-4812-bb7b-95df922ab122
# ╠═3db10856-e07f-472c-9f6c-1fa8658917a3
# ╠═c7bd4da6-33b3-4831-b489-ed5512174f54
# ╠═a4a41e46-fd2c-4b6f-9874-ff07528466dd
# ╠═8c50ae87-299a-4f05-94cc-41c6b4f5dc49
# ╠═6da97b7e-3479-43b1-9b84-1c8f24323bea
# ╠═86bda3c6-c468-40fd-a4e7-46553697b206
# ╠═915685b0-2111-44e6-9ac9-39e5c5765ba2
# ╠═b20b6021-3109-4f10-b479-a27105ed4d5e
# ╠═faf56c59-5fa2-44f5-b56e-73f7c311b21e
# ╠═1c6bf1a5-5e82-408a-b415-45043b218856
# ╠═d879f3d9-d76e-45b0-a77d-064c7e56763e
# ╠═8441ade9-aa8e-46e0-8eb5-ec2bc1529ab6
# ╠═9b2a9689-4d8a-4dfe-8495-fe0e016dc2ab
# ╠═c908f498-4cb6-41eb-989c-b502ec2deb59
# ╠═d313adc6-6a9a-406c-bcb9-bd26072b8862
# ╠═fb77e755-607a-427a-ab11-8e8d813fd0e2
# ╠═78388f8f-c764-475e-9da7-4e038aa2a9b6
# ╠═5454fc90-0e06-4340-a359-f03e02ca04e6
# ╠═b36e235e-4770-4467-99e3-a93f44ee9427
# ╠═c4d064ea-4253-415e-ab70-0436e60479bb
# ╠═09cc8944-e088-42d7-aa07-0082b5717a0d
# ╠═9130174b-460d-49f2-8c1a-3b9789ecbeb5
# ╠═da33d5c9-ff20-42b4-bc14-cc8368729627
# ╠═ff31a03c-c3de-4ca1-b205-d011df2ffaad
# ╠═d1b07b71-f173-430e-a1c7-4935b896d403
# ╠═dd2da274-2bf1-4a40-90a4-f9dd2d06dc65
# ╠═1ea0b8a5-d2e2-4383-ba37-13d4c0f979a3
# ╠═34322f9f-7a96-4686-8afd-58b7e6ad01ee
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
