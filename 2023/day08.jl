### A Pluto.jl notebook ###
# v0.19.29

using Markdown
using InteractiveUtils

# ╔═╡ e9f5bf78-4a97-499b-a828-6dbab934568f
using LinearAlgebra

# ╔═╡ c945b84a-9908-49ec-8f41-5ffe18868c62
using SparseArrays

# ╔═╡ 02c9ddfb-ec00-4f39-962b-dfd3e5490a9a
using Primes

# ╔═╡ 43056fb8-9618-11ee-05fc-598e379a9204
const input = readlines("input08.txt")

# ╔═╡ 7412d88d-c825-4d9b-bfda-26ce805f142f
const test_input_1 = split("RL\n\
\n\
AAA = (BBB, CCC)\n\
BBB = (DDD, EEE)\n\
CCC = (ZZZ, GGG)\n\
DDD = (DDD, DDD)\n\
EEE = (EEE, EEE)\n\
GGG = (GGG, GGG)\n\
ZZZ = (ZZZ, ZZZ)", "\n")

# ╔═╡ 1e147c9d-dbf8-4746-82b7-00eb2db63b82
const test_input_2 = split("LLR\n\
\n\
AAA = (BBB, BBB)\n\
BBB = (AAA, ZZZ)\n\
ZZZ = (ZZZ, ZZZ)","\n")

# ╔═╡ 1fd55153-6130-4175-899b-1c216cadaa58
const test_input_3 = split("LR\n\
\n\
11A = (11B, XXX)\n\
11B = (XXX, 11Z)\n\
11Z = (11B, XXX)\n\
22A = (22B, XXX)\n\
22B = (22C, 22C)\n\
22C = (22Z, 22Z)\n\
22Z = (22B, 22B)\n\
XXX = (XXX, XXX)", "\n")

# ╔═╡ 31d0ef8d-193a-4efc-9788-63774e82bf7f
function parse_input(i)
	[l[s] for l=i[3:end], s=(1:3,8:10,13:15)], [(c=='R')+1 for c=i[1]]
end

# ╔═╡ 52953493-8ca2-4509-b53c-90a63fa1c8a9
const maps_str, dirs = parse_input(input)

# ╔═╡ 5a9c555d-5dc6-4a7b-abb0-dfe35b5733fa
const names = Dict(k => v for (v,k)=enumerate(unique(maps_str)))

# ╔═╡ d57cbf03-c9cc-41f6-91a3-472ac572c075
const maps = map(k -> names[k], maps_str[:, 2:3])

# ╔═╡ 2a3e7234-68a3-4341-b483-9209f8826819
const S = size(maps)[1]

# ╔═╡ 02a4342c-d1b2-4dda-9bf0-decb6a25d63d
const N = length(dirs)

# ╔═╡ c383501e-4d72-48d2-aefb-721b3a1731f6
build_mat(x) = sparse(Int8[
	x[j] == i
	for i=1:S,j=1:S
])

# ╔═╡ cc25c839-c4c5-4d84-a061-67287dfc84eb
const map_mats = (build_mat(maps[:,1]), build_mat(maps[:,2]))

# ╔═╡ ca09511a-4e22-49ad-9118-d53bb9552efa
apply_turn(state, turn) = turn * state

# ╔═╡ 8b276f22-a7f0-439e-bced-f264fe038de3
apply_turn_clamp(state, turn) = min.(0, turn * state)

# ╔═╡ 82a49edb-da7f-444b-92e1-d092e39af278
const work_arrays = accumulate(apply_turn, map_mats[x] for x=dirs)

# ╔═╡ 5f59a740-8557-45e8-98b8-f4135e55b488
function countsteps(start, finish)
	target = names[finish], names[start]

	work_mats = accumulate(apply_turn, map_mats[x] for x=dirs)
	loop_mat = work_mats[end]
	
	c = 0

	while true
		pos = findfirst(m -> m[target...] == 1, work_mats)
		if isnothing(pos)
			work_mats = apply_turn.((loop_mat,), work_mats)
			c += 1
		else
			println("c: $c p: $pos")
			return c*N + pos
		end
	end
		
end

# ╔═╡ 5cf24d1e-f0cc-4a59-9e79-eb3f62eb40bb
S

# ╔═╡ fe0bd5c9-8fe5-44e9-af7e-594b7e28934e
N

# ╔═╡ 4521dd0c-75fc-4def-aa20-e91139a151a2
countsteps("AAA", "ZZZ")

# ╔═╡ bdb78245-bbc6-473b-a27a-83003de6c9c6
[n for (s,n)=names if endswith(s, "A")]

# ╔═╡ d5d863d4-79a9-4bb7-a9ac-aaea6976d109
create_vec(is) = Int8[s in is for s=1:S]

# ╔═╡ 27db12b1-a16a-479a-bf58-f67d14fdd4bd
const starts = create_vec([n for (s,n)=names if endswith(s, "A")]) |> sparse 

# ╔═╡ 1550ffd4-4573-4ad3-b722-8e36f37facd2
function countsteps2()
	As = [n for (s,n)=names if endswith(s, "A")]	
	starts = create_vec([n for (s,n)=names if endswith(s, "A")]) |> sparse
	finishes = Set(n for (s,n)=names if endswith(s, "Z"))
	
	# work_mats = accumulate(apply_turn, map_mats[x] for x=dirs)
	loop_mat = reduce(apply_turn, map_mats[dirs])

	function checkmat(m)
		is,v = findnz(m)
		all(i in finishes for i=is)
	end

	As2 = [sparsevec([a], Int8[1], S) for a=As] 
	l = Int64[]
	for a=As2
		i = loop_mat
		for x=2:2000
			i = loop_mat * i
			if checkmat(i * a)
				append!(l, x)
				break
			end
		end
	end
	C = lcm(l...)
	println("lcm: $C ")
	return C * N

	# r = loop_mat^C * starts
	# g, h = findnz(loop_mat^C * starts)
	# println((g, h))
	# g == sort([n for (s,n)=names if endswith(s, "Z")])
end

# ╔═╡ 8beff819-4dbe-4c6d-8ceb-b0b40444f38b
countsteps2()

# ╔═╡ 4121f2ec-d0ac-421e-94ef-9babac0fc8c1
36648605837 * N

# ╔═╡ 6d4ad458-dc5a-430e-b8fc-4ab2b9266ee1
[n for (s,n)=names if endswith(s, "A")]

# ╔═╡ 41a7fb63-ba59-4daf-a799-2949cd19ca92
factor(N * 36648605837)

# ╔═╡ 7c7c709a-a701-4f4e-99b3-889603914ee0
factor(S)

# ╔═╡ 41160623-5040-47b0-9490-1a38b87966f7
714 * 714 * 1 * 269

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
SparseArrays = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[compat]
Primes = "~0.5.4"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.9.2"
manifest_format = "2.0"
project_hash = "a0ee430a0589b8c4369a032523abc0b92b455ac0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"
version = "1.1.1"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.0.5+0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"
version = "1.6.0"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "b8ffb903da9f7b8cf695a8bead8e01814aa24b30"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"
version = "0.6.3"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"
version = "7.84.0+0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"
version = "1.10.2+0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"
version = "2.28.2+0"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"
version = "2022.10.11"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"
version = "1.2.0"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.21+4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "FileWatching", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
version = "1.9.2"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "4c9f306e5d6603ae203c2000dd460d81a5251489"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.4"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "Pkg", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "5.10.1+6"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"
version = "1.10.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+0"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.8.0+0"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"
version = "1.48.0+0"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
version = "17.4.0+0"
"""

# ╔═╡ Cell order:
# ╠═e9f5bf78-4a97-499b-a828-6dbab934568f
# ╠═43056fb8-9618-11ee-05fc-598e379a9204
# ╠═7412d88d-c825-4d9b-bfda-26ce805f142f
# ╠═1e147c9d-dbf8-4746-82b7-00eb2db63b82
# ╠═1fd55153-6130-4175-899b-1c216cadaa58
# ╠═31d0ef8d-193a-4efc-9788-63774e82bf7f
# ╠═52953493-8ca2-4509-b53c-90a63fa1c8a9
# ╠═5a9c555d-5dc6-4a7b-abb0-dfe35b5733fa
# ╠═d57cbf03-c9cc-41f6-91a3-472ac572c075
# ╠═2a3e7234-68a3-4341-b483-9209f8826819
# ╠═02a4342c-d1b2-4dda-9bf0-decb6a25d63d
# ╠═c383501e-4d72-48d2-aefb-721b3a1731f6
# ╠═cc25c839-c4c5-4d84-a061-67287dfc84eb
# ╠═ca09511a-4e22-49ad-9118-d53bb9552efa
# ╠═8b276f22-a7f0-439e-bced-f264fe038de3
# ╠═82a49edb-da7f-444b-92e1-d092e39af278
# ╠═c945b84a-9908-49ec-8f41-5ffe18868c62
# ╠═5f59a740-8557-45e8-98b8-f4135e55b488
# ╠═5cf24d1e-f0cc-4a59-9e79-eb3f62eb40bb
# ╠═fe0bd5c9-8fe5-44e9-af7e-594b7e28934e
# ╠═4521dd0c-75fc-4def-aa20-e91139a151a2
# ╠═bdb78245-bbc6-473b-a27a-83003de6c9c6
# ╠═d5d863d4-79a9-4bb7-a9ac-aaea6976d109
# ╠═27db12b1-a16a-479a-bf58-f67d14fdd4bd
# ╠═1550ffd4-4573-4ad3-b722-8e36f37facd2
# ╠═8beff819-4dbe-4c6d-8ceb-b0b40444f38b
# ╠═4121f2ec-d0ac-421e-94ef-9babac0fc8c1
# ╠═6d4ad458-dc5a-430e-b8fc-4ab2b9266ee1
# ╠═41a7fb63-ba59-4daf-a799-2949cd19ca92
# ╠═7c7c709a-a701-4f4e-99b3-889603914ee0
# ╠═41160623-5040-47b0-9490-1a38b87966f7
# ╠═02c9ddfb-ec00-4f39-962b-dfd3e5490a9a
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
