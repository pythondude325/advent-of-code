### A Pluto.jl notebook ###
# v0.19.29

using Markdown
using InteractiveUtils

# ╔═╡ ef370c1a-93af-11ee-1cbb-cbd50ef7ab77
const testinput = readlines("input05_test.txt")

# ╔═╡ dc213d73-e6cb-4395-8a79-fe10515c4ae0
const input = readlines("input05.txt")

# ╔═╡ ddf3ed5a-31a9-48fe-ba32-4afc0d85acaf
function parse_input(l)
	A = Matrix{Int64}[]
	a = Matrix{Int64}(undef, 3, 0)
	for L=filter(!=(""), l[4:end])
		if L[end] == ':'
			push!(A, a)
			a = Matrix{Int64}(undef, 3, 0)
		else
			a = hcat(a, reshape(parse.(Int64, split(L)), 3, 1))
		end
	end
	push!(A, a)
	
	parse.(Int64, split(l[1][7:end])), A
end

# ╔═╡ f11e250d-c2a7-42ce-901e-468bd1ccee9a
const seeds, almanac = parse_input(testinput)

# ╔═╡ 6c029f63-4867-42a1-a2b5-7adf14f92358
const seedsI = hcat(seeds, seeds .+ 1)

# ╔═╡ dae5ff6d-f954-4bf4-8e5f-fb0c286bb6f5
convert_ranges(R) = [R[:,1] R[:,1].+R[:,2]]

# ╔═╡ f8bd59a0-2159-4c1a-8131-526d27dd7b61
const seedsII = convert_ranges(reshape(seeds, 2, :)')

# ╔═╡ b66770c2-aad1-48bb-9380-1c6c71775347
filter_ranges(R) = R[findall(R[:, 1] .< R[:, 2]), :]

# ╔═╡ 4503d5b5-31fc-4d76-94e2-d4c84e05c87c
sort_ranges(R) = sortslices(R, dims=1)

# ╔═╡ 75a7caf0-141b-4d18-8952-ada2a38bc82a
function merge_ranges(R)
	if size(R)[1] == 0
		return Matrix{Int64}(undef, 0,2)
	end
	out_ranges = Matrix{Int64}(undef, 0,2)
	current_start = R[1,1]
	current_end = R[1,2]
	for (s,e)=eachrow(@views R[2:end,:])
		if s > current_end
			out_ranges = vcat(out_ranges, [current_start current_end])
			current_start, current_end = s, e
		else
			current_end = e
		end
	end
	vcat(out_ranges, [current_start current_end])
end

# ╔═╡ 74c2fa2e-afbf-456a-9dc0-2150432e2c2d
clean_ranges(R) = R |> filter_ranges  |> sort_ranges |> merge_ranges

# ╔═╡ 49b25547-64bb-4f7c-ba29-eed65dee815c
maxmin(A, B) = @views [min.(A[:,1], B[:,1]) max.(A[:,2],B[:,2])]

# ╔═╡ 72a2b028-75c0-47f1-9ed0-c481020ae64a
function apply_map_range(map, seed_ranges)	
	ms = map[2]
	me = ms + map[3]
	mo = map[1] - ms
	
	@views is = seed_ranges[:, 1]
	@views ie = seed_ranges[:, 2]

	r1s = is
	r3s = max.(is, me)

	r1e = min.(ie, ms)
	r3e = ie

	r2s = max.(is, ms) .+ mo
	r2e = min.(ie, me) .+ mo
	
	[
		r1s r1e
		r3s r3e
	] |> clean_ranges,
	[r2s r2e] |> clean_ranges
end

# ╔═╡ bf198472-577d-48da-bc60-3db21e599f6c
function apply_maps_range(seed_ranges, maps)
	a_ranges = seed_ranges
	b_ranges = Matrix{Int64}(undef, 0, 2)

	for map=eachcol(maps)
		a_ranges, b_res = apply_map_range(map, a_ranges)
		b_ranges = vcat(b_ranges, b_res)
	end

	vcat(a_ranges, b_ranges) |> clean_ranges
end

# ╔═╡ 32b7a903-1861-4f83-b87b-f773c183e22e
apply_maps(A, seed_ranges) = foldl(apply_maps_range, A, init=seed_ranges)

# ╔═╡ 01744889-9288-4b32-8ad9-9c96842c0de9
const part1 = apply_maps(almanac, seedsI)[1]

# ╔═╡ 86201e86-9b5e-40ec-a543-ff045576569b
const part2 = apply_maps(almanac, seedsII)[1]

# ╔═╡ Cell order:
# ╠═ef370c1a-93af-11ee-1cbb-cbd50ef7ab77
# ╠═dc213d73-e6cb-4395-8a79-fe10515c4ae0
# ╠═ddf3ed5a-31a9-48fe-ba32-4afc0d85acaf
# ╠═f11e250d-c2a7-42ce-901e-468bd1ccee9a
# ╠═6c029f63-4867-42a1-a2b5-7adf14f92358
# ╠═f8bd59a0-2159-4c1a-8131-526d27dd7b61
# ╠═01744889-9288-4b32-8ad9-9c96842c0de9
# ╠═86201e86-9b5e-40ec-a543-ff045576569b
# ╠═dae5ff6d-f954-4bf4-8e5f-fb0c286bb6f5
# ╠═b66770c2-aad1-48bb-9380-1c6c71775347
# ╠═4503d5b5-31fc-4d76-94e2-d4c84e05c87c
# ╠═75a7caf0-141b-4d18-8952-ada2a38bc82a
# ╠═74c2fa2e-afbf-456a-9dc0-2150432e2c2d
# ╠═32b7a903-1861-4f83-b87b-f773c183e22e
# ╠═bf198472-577d-48da-bc60-3db21e599f6c
# ╠═49b25547-64bb-4f7c-ba29-eed65dee815c
# ╠═72a2b028-75c0-47f1-9ed0-c481020ae64a
