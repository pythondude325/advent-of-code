### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ 4222d0f7-a00e-4024-b13c-918c7c5042d6
using BenchmarkTools

# ╔═╡ 550c1b64-526b-11ec-3d43-358cb5478a79
const input = parse.(Int, readlines("01_input.txt"))

# ╔═╡ 028c9e31-7055-4ac5-9205-7a8750a8de34
part_1() = map(<, input, @view(input[2:end])) |> count

# ╔═╡ 90597b90-7c9b-484b-a65c-6ef5a1925079
part_1()

# ╔═╡ 76d229f4-cd6b-436f-8fbf-557b4b1b3480
@benchmark part_1()

# ╔═╡ 9c1cf603-7d56-410c-a80a-65df4e8b2159
part_2() = map(<, input, @view input[4:end]) |> count
# fast version:
# 

# ╔═╡ 39651aa7-f873-4a2b-8b82-df0cca8be6fe
part_2()

# ╔═╡ 2e32b38f-a0b6-4887-969b-3190b3547a88
@benchmark part_2()

# ╔═╡ f237f962-c11a-443b-9867-f47aad44fdb4
fast_part_2() = count(@inbounds input[i] < input[i+3] for i=1:length(input)-3)

# ╔═╡ b0cb98b3-b6cc-49c5-9ab5-292ae03b7a10
@benchmark fast_part_2()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"

[compat]
BenchmarkTools = "~1.2.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "61adeb0823084487000600ef8b1c00cc2474cd47"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.0"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "ae4bbcadb2906ccc085cf52ac286dc1377dceccc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.2"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"
"""

# ╔═╡ Cell order:
# ╠═550c1b64-526b-11ec-3d43-358cb5478a79
# ╠═028c9e31-7055-4ac5-9205-7a8750a8de34
# ╠═90597b90-7c9b-484b-a65c-6ef5a1925079
# ╠═76d229f4-cd6b-436f-8fbf-557b4b1b3480
# ╠═9c1cf603-7d56-410c-a80a-65df4e8b2159
# ╠═39651aa7-f873-4a2b-8b82-df0cca8be6fe
# ╠═2e32b38f-a0b6-4887-969b-3190b3547a88
# ╠═f237f962-c11a-443b-9867-f47aad44fdb4
# ╠═b0cb98b3-b6cc-49c5-9ab5-292ae03b7a10
# ╠═4222d0f7-a00e-4024-b13c-918c7c5042d6
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
