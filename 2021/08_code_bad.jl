### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ f0be8f6a-26c0-44f0-86e1-32c0e6b70cae
using Combinatorics

# ╔═╡ d62c44e7-9d25-4107-8db8-2095f50de850
using LinearAlgebra

# ╔═╡ 04282206-57e3-11ec-38d8-2b60d87b8dfb
const input = readlines("08_input_example_2.txt")

# ╔═╡ 7e352faa-8797-4393-9090-50a4737cd315
parse_signal(s) = BitVector(c in s for c="abcdefg")

# ╔═╡ fa163b90-9fe3-42e7-8f1a-506086259f5c
function parse_line(line)
	p,r = split(line, "|")
	parse_signal.(split(p)), parse_signal.(split(r))
end

# ╔═╡ 6bfd4f9b-b2a1-44e4-9c64-9727bd7b2df5
const cases = parse_line.(input)

# ╔═╡ efe73916-e379-4731-ac41-d35e23ba9692
is_my_digit(f) = sum(f) in (2,4,3,7)

# ╔═╡ 86042330-afff-490d-91a1-2f13807cb02f
part1 = sum(sum([is_my_digit(x) for x=case[2]]) for case=cases)

# ╔═╡ 59f0daa8-89ce-4790-aaf0-2294a448cf06
digits = BitMatrix([
	1 0 1 1 0 1 1 1 1 1;
	1 0 0 0 1 1 1 0 1 1;
	1 1 1 1 1 0 0 1 1 1;
	0 0 1 1 1 1 1 0 1 1;
	1 0 1 0 0 0 1 0 1 0;
	1 1 0 1 1 1 1 1 1 1;
	1 0 1 1 0 1 1 0 1 1;
])

# ╔═╡ e46950aa-18c6-4597-8b04-50ad7cabd4d9
bit_weights = mapslices(sum, digits, dims=(1))

# ╔═╡ 6bf3d0e1-8cfb-4d29-bfe7-88abf1c2fd8d
const ident = BitMatrix(Matrix(I,7,7))

# ╔═╡ 8e59dfbb-2bbe-4ffd-aa1d-9ae05e246024
permutation_to_matrix(p) = reduce(hcat, map(x -> ident[:,x], p))

# ╔═╡ 5a8f532a-fbd0-4769-a0dc-fd52811c11ea
dset = [digits[:,x] for x=1:10]

# ╔═╡ 5daa5893-175f-41f3-89cc-1e0c93836cca
function check_permutation(p,c) 
	P = permutation_to_matrix(p)
	all(result ∈ dset for result=[P * C for C=c])
end

# ╔═╡ 6d8140a9-6664-4f63-97a0-80e2a6877cac
function get_value(case)
	hints = case[1]
	ans = case[2]
	ps = filter(p -> check_permutation(p, hints), collect(permutations(1:7)))
	P = permutation_to_matrix(ps[1])
	
	evalpoly(10, reverse([findfirst([d == P*a for d=dset])-1 for a=ans]))
end

# ╔═╡ 48f518de-5731-4a5d-8967-bbf40a1a4138
part2 = sum(get_value, cases)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Combinatorics = "861a8166-3701-5b0c-9a16-15d98fcdc6aa"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[compat]
Combinatorics = "~1.0.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Combinatorics]]
git-tree-sha1 = "08c8b6831dc00bfea825826be0bc8336fc369860"
uuid = "861a8166-3701-5b0c-9a16-15d98fcdc6aa"
version = "1.0.2"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
"""

# ╔═╡ Cell order:
# ╠═04282206-57e3-11ec-38d8-2b60d87b8dfb
# ╠═7e352faa-8797-4393-9090-50a4737cd315
# ╠═fa163b90-9fe3-42e7-8f1a-506086259f5c
# ╠═6bfd4f9b-b2a1-44e4-9c64-9727bd7b2df5
# ╠═efe73916-e379-4731-ac41-d35e23ba9692
# ╠═86042330-afff-490d-91a1-2f13807cb02f
# ╟─59f0daa8-89ce-4790-aaf0-2294a448cf06
# ╠═e46950aa-18c6-4597-8b04-50ad7cabd4d9
# ╠═6bf3d0e1-8cfb-4d29-bfe7-88abf1c2fd8d
# ╠═8e59dfbb-2bbe-4ffd-aa1d-9ae05e246024
# ╠═5a8f532a-fbd0-4769-a0dc-fd52811c11ea
# ╠═5daa5893-175f-41f3-89cc-1e0c93836cca
# ╠═6d8140a9-6664-4f63-97a0-80e2a6877cac
# ╠═48f518de-5731-4a5d-8967-bbf40a1a4138
# ╠═f0be8f6a-26c0-44f0-86e1-32c0e6b70cae
# ╠═d62c44e7-9d25-4107-8db8-2095f50de850
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
