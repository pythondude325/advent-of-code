using BenchmarkTools

const input = readlines("08_input.txt")

parse_signal(s) = BitVector(c in s for c="abcdefg")

function parse_line(line)::Tuple{BitMatrix,BitMatrix}
	p,r = split(line, "|")
	reduce(hcat, parse_signal.(split(p))), reduce(hcat, parse_signal.(split(r)))
end

const cases = parse_line.(input)

const digits = BitMatrix([
	1 0 1 1 0 1 1 1 1 1;
	1 0 0 0 1 1 1 0 1 1;
	1 1 1 1 1 0 0 1 1 1;
	0 0 1 1 1 1 1 0 1 1;
	1 0 1 0 0 0 1 0 1 0;
	1 1 0 1 1 1 1 1 1 1;
	1 0 1 1 0 1 1 0 1 1;
])

idk(w, M) = M * (w .== ones(Int,1,7) * M)'

layer(w, case) = idk(w, digits) .== idk(w, case)'

solve(case) = reduce(.&, layer(n, case) for n=2:6)

decode_digit(d) = ((0:9)' * reduce(&, digits .== d, dims=1)')[1]

decode(a) = [decode_digit(a[:,i]) for i=1:4]

part2() = sum(evalpoly(10, reverse(decode(solve(h) * a))) for (h,a)=cases)

part2()

@benchmark part2()
