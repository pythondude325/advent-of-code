#lang racket/base

(require "../aoc.rkt" racket/list)

(define (day-1)
  (define input (read-input-file "01_input.txt" read))

  (define (helper n input)
    (count id (map-tails (λ (t)
                           (and (< n (length t))
                                (< (car t) (car (drop t n)))))
                         input)))
  
  (define part-1 (helper 1 input))
  (define part-2 (helper 3 input))

  (assert-equal? 1581 part-1)
  (assert-equal? 1618 part-2)
  (show-vars part-1 part-2))

(day-1)
