using Statistics: median

c_to_n(c::Char) = 5 - findfirst.(==(c), "<{[( )]}>")

parse_line(line::String) = [c_to_n(c) for c=line]

const lines = parse_line.(readlines("10_input.txt"))

function check_line(line)
	os = []
	for c=line
		if c > 0
			push!(os, c)
		else
			C = pop!(os)
			if c != -C
				return (c, os)
			end
		end
	end
	return (0, os)
end

score1((n,s)) = n == 0 ? 0 : [3,57,1197,25137][-n]

part1 = sum(score1.(check_line.(lines)))

const incomplete_lines =
	map(x -> x[2], filter(l -> l[1] == 0, map(check_line, lines)))

score2(x) = foldr((a,b) -> 5b+a, x, init=0)

part2 = Int(median(score2.(incomplete_lines)))

println("$(part1) $(part2)")
