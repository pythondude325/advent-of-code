### A Pluto.jl notebook ###
# v0.17.3

using Markdown
using InteractiveUtils

# ╔═╡ 245c842c-2786-4f55-b0e6-6425ba9a395d
using Pipe

# ╔═╡ a5032d7a-5e2c-11ec-145f-15f87741b406
const input = readlines("16_input.txt")

# ╔═╡ cb5c3e41-9704-48ba-afda-109643ef6b1b
struct Packet
	version::Int
	tag::Int
	payload
	length::Int
end

# ╔═╡ 896ae3a8-9a17-4ecb-80aa-652d375998fc


# ╔═╡ cf42ffdd-3738-4ba8-8e47-89c6b340499c


# ╔═╡ 9a89b151-c4d9-404d-b63a-3bb52312f909
collect(1:10)[(4+3):end]

# ╔═╡ bd8fb37e-170a-410c-9e6a-aa6777904a53
bv2int(bs) = evalpoly(2,reverse(bs))

# ╔═╡ 629342aa-c855-4a54-85ec-810a397dd281
begin
	import Base: parse
	function parse(::Type{Packet}, s::String)::Packet
		bv = hex2bitvec(s)
		parse(Packet, bv)
	end
	
	function parse(::Type{Packet}, bv::BitVector)::Packet
		version = bv2int(bv[1:3])
		tag = bv2int(bv[4:6])
		if tag == 4
			pos = 7
			value = 0
			break_next = true
			while break_next
				break_next = bv[pos]
				bits = bv[(pos+1):(pos+4)]
				value = (value << 4) + bv2int(bits)
				pos += 5
			end
			Packet(version, tag, value, pos-1)
		else
			length_type_id = bv[7]
			if length_type_id == false
				total_length = bv[8:(8+15-1)] |> bv2int
				
				subpackets = []
				offset = 8+15
				while offset < 8+15+total_length
					subpack = parse(Packet, bv[offset:end])
					push!(subpackets, subpack)
					offset += subpack.length
				end
				Packet(version, tag, subpackets, 6+1+15+total_length)
			else
				num_of_packets = bv[8:(8+11-1)] |> bv2int

				subpackets = []
				offset = 8+11
				for _=1:num_of_packets
					subpack = parse(Packet, bv[offset:end])
					push!(subpackets, subpack)
					offset += subpack.length
				end
				
				Packet(version, tag, subpackets, offset-1)
			end
		end
	end
end

# ╔═╡ 559ef673-421e-46a3-8f50-d20718cafc88
const input_packet = parse(Packet,input[1])

# ╔═╡ 386cb0d2-e5fd-42bd-bb55-75d8fd646769
function ctobs(c::Char)
	n = parse(Int8, c, base=16)
	[(n>>x & 1) for x=3:-1:0]
end

# ╔═╡ 14921644-7837-4283-8ca4-aec025ece413
hex2bitvec(s) = BitVector(reduce(vcat, ctobs.(collect(s))))

# ╔═╡ 467f897f-0f6b-4e1f-9d06-7218c5e26185
hex2bitvec("D2FE28")

# ╔═╡ 488d37f2-86d0-47ca-9ac3-06b8c83684f3
parse(Packet, "38006F45291200")

# ╔═╡ 340d0227-5d8e-4c6f-8c64-de1c38098453
parse(Packet, "EE00D40C823060")

# ╔═╡ 8b19a2c7-74ff-4a69-830a-84aad2747bf7
function version_sum(p::Packet)
	total = p.version
	if p.payload isa Vector
		total += sum(version_sum.(p.payload))
	end
	total
end

# ╔═╡ 9a0fd67d-da89-480b-a7fb-f233752df69e
part1 = version_sum(input_packet)

# ╔═╡ 19ba1e67-af12-42d2-9e9d-5f22e988a8dd
function interpret(p::Packet)::Int
	if p.tag == 0
		sum(interpret.(p.payload))
	elseif p.tag == 1
		prod(interpret.(p.payload))
	elseif p.tag == 2
		minimum(interpret.(p.payload))
	elseif p.tag == 3
		maximum(interpret.(p.payload))
	elseif p.tag == 4
		p.payload
	elseif p.tag == 5
		interpret(p.payload[1]) > interpret(p.payload[2])
	elseif p.tag == 6
		interpret(p.payload[1]) < interpret(p.payload[2])
	elseif p.tag == 7
		interpret(p.payload[1]) == interpret(p.payload[2])
	else
		error("shit")
	end
end

# ╔═╡ fea693f0-add1-46fa-8131-f843f206f12a
part2 = interpret(input_packet)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Pipe = "b98c9c47-44ae-5843-9183-064241ee97a0"

[compat]
Pipe = "~1.3.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.0"
manifest_format = "2.0"

[[deps.Pipe]]
git-tree-sha1 = "6842804e7867b115ca9de748a0cf6b364523c16d"
uuid = "b98c9c47-44ae-5843-9183-064241ee97a0"
version = "1.3.0"
"""

# ╔═╡ Cell order:
# ╠═a5032d7a-5e2c-11ec-145f-15f87741b406
# ╠═559ef673-421e-46a3-8f50-d20718cafc88
# ╠═386cb0d2-e5fd-42bd-bb55-75d8fd646769
# ╠═14921644-7837-4283-8ca4-aec025ece413
# ╠═467f897f-0f6b-4e1f-9d06-7218c5e26185
# ╠═cb5c3e41-9704-48ba-afda-109643ef6b1b
# ╠═896ae3a8-9a17-4ecb-80aa-652d375998fc
# ╠═629342aa-c855-4a54-85ec-810a397dd281
# ╠═cf42ffdd-3738-4ba8-8e47-89c6b340499c
# ╠═9a89b151-c4d9-404d-b63a-3bb52312f909
# ╠═bd8fb37e-170a-410c-9e6a-aa6777904a53
# ╠═488d37f2-86d0-47ca-9ac3-06b8c83684f3
# ╠═340d0227-5d8e-4c6f-8c64-de1c38098453
# ╠═8b19a2c7-74ff-4a69-830a-84aad2747bf7
# ╠═9a0fd67d-da89-480b-a7fb-f233752df69e
# ╠═19ba1e67-af12-42d2-9e9d-5f22e988a8dd
# ╠═fea693f0-add1-46fa-8131-f843f206f12a
# ╠═245c842c-2786-4f55-b0e6-6425ba9a395d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
