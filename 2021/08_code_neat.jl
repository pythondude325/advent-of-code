### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ 1a6a93a1-4b24-4ccf-8921-1076a62bd904
using BenchmarkTools

# ╔═╡ 04282206-57e3-11ec-38d8-2b60d87b8dfb
const input = readlines("08_input.txt")

# ╔═╡ 7e352faa-8797-4393-9090-50a4737cd315
parse_signal(s) = BitVector(c in s for c="abcdefg")

# ╔═╡ fa163b90-9fe3-42e7-8f1a-506086259f5c
function parse_line(line)::Tuple{BitMatrix,BitMatrix}
	p,r = split(line, "|")
	reduce(hcat, parse_signal.(split(p))), reduce(hcat, parse_signal.(split(r)))
end

# ╔═╡ 6bfd4f9b-b2a1-44e4-9c64-9727bd7b2df5
const cases = parse_line.(input)

# ╔═╡ 59f0daa8-89ce-4790-aaf0-2294a448cf06
const digits = BitMatrix([
	1 0 1 1 0 1 1 1 1 1;
	1 0 0 0 1 1 1 0 1 1;
	1 1 1 1 1 0 0 1 1 1;
	0 0 1 1 1 1 1 0 1 1;
	1 0 1 0 0 0 1 0 1 0;
	1 1 0 1 1 1 1 1 1 1;
	1 0 1 1 0 1 1 0 1 1;
])

# ╔═╡ 058b79c3-8dfa-45fe-9e23-f9106a0babe3
idk(w, M) = M * (w .== ones(Int,1,7) * M)'

# ╔═╡ 4aaf36e5-a114-46f0-8abe-abd45360f044
layer(w, case) = idk(w, digits) .== idk(w, case)'

# ╔═╡ 2d1cdb44-9888-4792-9f26-4335dc3ca44f
solve(case) = reduce(.&, layer.(3:6, Ref(case)))

# ╔═╡ 0c3ec32a-fc7d-405d-bcfe-0cc0d0834bfb
decode_digit(d) = ((0:9)' * reduce(&, digits .== d, dims=1)')[1]

# ╔═╡ e7ec7537-1cf5-4a82-b7c3-2c7f1b8fa55b
decode(a) = [decode_digit(a[:,i]) for i=1:4]

# ╔═╡ d238b086-8189-48a4-9945-81ff2d172103
part2() = sum(evalpoly(10, reverse(decode(solve(h) * a))) for (h,a)=cases)

# ╔═╡ b8dee283-b9d5-4907-a54a-c86edbe05d8d
part2()

# ╔═╡ 65cde581-c683-4c06-9308-69f9f47ba3d1
@benchmark part2()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"

[compat]
BenchmarkTools = "~1.2.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "940001114a0147b6e4d10624276d56d531dd9b49"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.2"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "ae4bbcadb2906ccc085cf52ac286dc1377dceccc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.2"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"
"""

# ╔═╡ Cell order:
# ╠═04282206-57e3-11ec-38d8-2b60d87b8dfb
# ╠═7e352faa-8797-4393-9090-50a4737cd315
# ╠═fa163b90-9fe3-42e7-8f1a-506086259f5c
# ╠═6bfd4f9b-b2a1-44e4-9c64-9727bd7b2df5
# ╟─59f0daa8-89ce-4790-aaf0-2294a448cf06
# ╠═058b79c3-8dfa-45fe-9e23-f9106a0babe3
# ╠═4aaf36e5-a114-46f0-8abe-abd45360f044
# ╠═2d1cdb44-9888-4792-9f26-4335dc3ca44f
# ╠═0c3ec32a-fc7d-405d-bcfe-0cc0d0834bfb
# ╠═e7ec7537-1cf5-4a82-b7c3-2c7f1b8fa55b
# ╠═d238b086-8189-48a4-9945-81ff2d172103
# ╠═b8dee283-b9d5-4907-a54a-c86edbe05d8d
# ╠═65cde581-c683-4c06-9308-69f9f47ba3d1
# ╠═1a6a93a1-4b24-4ccf-8921-1076a62bd904
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
