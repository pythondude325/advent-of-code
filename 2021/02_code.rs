use std::{io::{self, BufRead}, fs};

#[derive(Copy, Clone)]
enum Dir {
    Forward(u8),
    Up(u8),
    Down(u8),
}

fn parse_direction(s: &str) -> Dir {
    match s.as_bytes() {
        [b'f', .., d] => Dir::Forward(d - b'0'),
        [b'u', .., d] => Dir::Up(d - b'0'),
        [b'd', .., d] => Dir::Down(d - b'0'),
        _ => unreachable!(),
    }
}

fn main() -> io::Result<()> {
    let input_file = fs::File::open("02_input.txt")?;
    let reader = io::BufReader::new(input_file);
    let input = reader.lines().map(|r| parse_direction(&r.unwrap())).collect::<Vec<_>>();

    let part1 = {
        let total = input.iter().fold((0, 0), |(x,y),d| match *d {
            Dir::Forward(v) => (x + v as i32, y),
            Dir::Up(v) => (x, y - v as i32),
            Dir::Down(v) => (x, y + v as i32),
        });
        total.0 * total.1
    };
    dbg!(part1);

    let part2 = {
        let total = input.iter().fold((0, 0, 0), |(x,y,a),d| match *d {
            Dir::Forward(v) => (x + v as i32, y + a * v as i32, a),
            Dir::Up(v) => (x, y, a - v as i32),
            Dir::Down(v) => (x, y, a + v as i32),
        });
        total.0 * total.1
    };
    dbg!(part2);

    Ok(())
}