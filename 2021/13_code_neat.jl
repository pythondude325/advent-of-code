using LinearAlgebra
using Images

CI = CartesianIndex

const input = readlines("13_input.txt")

function parse_instruction(l)
	a,b = split(l,"=")
	a[end],parse(Int, b)
end

function fold_mat(x)
	i = Matrix(I,x-1,x-1)
	hcat(i,falses(x-1),i[:,end:-1:1])
end

function process_instruction((axis, d)::Tuple{Char,Int}, paper::Matrix)::Matrix
	fold_mat(d+1) * ((axis=='x') ? paper' : paper)
end

function folds_mat(n)
	level = trailing_ones(n)
	k = n >> level
	r = hcat(Matrix(I,k+2,k+2)[:,2:end], Matrix(I,k+2,k+2)[:,end-1:-1:1])[2:end-1,:]
	repeat(r,outer=(1,2^(level-1)))[:,1:end-1]
end

const points = [
	let (x,y)=parse.(Int,split(l,","))
		CI(y+1,x+1)
	end
	for l=Iterators.takewhile(l -> l != "", input)
]

paper_size = maximum(points)

const paper = begin
	f = zeros(Int, paper_size[1], paper_size[2])
	f[points] .= 1
	f
end

part1 = sum(process_instruction(parse_instruction(input[length(points)+2]), paper) .>= 1)

part2mat = folds_mat(size(paper,1)) * paper * folds_mat(size(paper,2))'

part2 = RGBX.(.!(part2mat .>= 1))


