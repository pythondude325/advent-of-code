### A Pluto.jl notebook ###
# v0.17.3

using Markdown
using InteractiveUtils

# ╔═╡ f70cd40e-1e5f-4e5d-b7ae-bd3b8044cd73
using PlutoUI

# ╔═╡ 845bbe61-fbfd-4d6e-8a54-130f3aa5ed4b
using BenchmarkTools

# ╔═╡ 694d4e44-6475-11ec-2590-f337943a56fe
const input = readlines("24_input.txt")

# ╔═╡ 46e1d299-5c2e-4276-b818-17d7c932744f
const input_ex = split("""
inp w
add z w
mod z 2
div w 2
add y w
mod y 2
div w 2
add x w
mod x 2
div w 2
mod w 2""", "\n")

# ╔═╡ b0ff9db4-bc2a-4d0d-92ea-9573449dbc8c
struct Immediate
	v::Int
end

# ╔═╡ f1ed8b65-a9a0-4bbe-ab1f-2bca3a109f3c
@enum Register::Int8 begin
	X
	Y
	Z
	W
end

# ╔═╡ a46ae819-de11-48dc-b153-d348438e81fc
const RValue = Union{Register, Immediate}

# ╔═╡ 48297d61-18a1-4d77-abdd-562397b178ec
Base.parse(::Type{Register}, s) =
	if s == "x"
		X
	elseif s == "y"
		Y
	elseif s == "z"
		Z
	elseif s == "w"
		W
	end

# ╔═╡ 757d0b0f-6e11-45c4-a13b-391b5a2b1be9
Base.parse(::Type{RValue}, s) =
	if s[1] in "xyzw"
		parse(Register, s)
	else
		Immediate(parse(Int, s))
	end

# ╔═╡ c9844ba7-c681-4a31-b790-a6c9862b32ed
abstract type Instruction end

# ╔═╡ fdc1e0c2-e5a9-4f8d-a211-44e4e2e76efb
struct Input <: Instruction
	r::Register
end

# ╔═╡ b01ab558-61ce-40f8-af38-c6e80a1fb15e
@enum Operator begin
	Add
	Mul
	Div
	Mod
	Eql
end

# ╔═╡ 1804af62-7094-4998-9ec4-f453c83c5f3f
Base.parse(::Type{Operator}, s) =
	if s == "add"
		Add
	elseif s == "mul"
		Mul
	elseif s == "div"
		Div
	elseif s == "mod"
		Mod
	elseif s == "eql"
		Eql
	end

# ╔═╡ 4a30f53c-8b2f-4472-a47f-64eee923a759
struct Operation <: Instruction
	op::Operator
	dst::Register
	src::RValue
end

# ╔═╡ 75271c21-c5f8-47ec-9da3-c86161d282c6
function Base.parse(::Type{Instruction}, s)
	l = split(s)
	if l[1] == "inp"
		Input(parse(Register, l[2]))
	else
		Operation(
			parse(Operator, l[1]),
			parse(Register, l[2]),
			parse(RValue, l[3]),
		)
	end
end

# ╔═╡ 4fdb556e-12cc-4b81-a71b-d6f0a6cbf0e4
code = parse.(Instruction, input_ex)

# ╔═╡ 9b94e46e-712b-442c-adaa-571a612a73d1
function compile(code::AbstractVector{Instruction})
	input_count = 0
	
	translate(imm::Immediate) = imm.v
	translate(r::Register) =
		if r == X
			:x
		elseif r == Y
			:y
		elseif r == Z
			:z
		elseif r == W
			:w
		end
	function translate(c::Input)::Expr
		input_count += 1
		ex = :($(translate(c.r)) = inp[$input_count])
	end
	function translate(c::Operation)
		src = translate(c.src)
		dst = translate(c.dst)
		if c.op == Add
			:($dst = $dst + $src)
		elseif c.op == Mul
			:($dst = $dst * $src)
		elseif c.op == Div
			:($dst = $dst ÷ $src)
		elseif c.op == Mod
			:($dst = $dst % $src)
		elseif c.op == Eql
			:($dst = Int($dst == $src))
		end
	end

	block = Expr(:block, [
		:(local x = 0);
		:(local y = 0);
		:(local z = 0);
		:(local w = 0);
		translate.(code);
		:((w,x,y,z));
	]...)

	@eval (inp) -> $block
end

# ╔═╡ b40d42ca-5130-46ed-a7e5-e98746265039
c = compile(code)

# ╔═╡ f3bd4873-97ae-49f6-905e-21931cc42538
code

# ╔═╡ 5ff0b6e6-d232-4a8b-94d3-5c32aab1461d


# ╔═╡ 7b56eaeb-0f0a-450b-91cc-972ad2803fe8
MONAD_code = parse.(Instruction, input)

# ╔═╡ 7211b806-2358-4cf8-be12-d4eb2d4aacc7


# ╔═╡ a42bf967-1c15-434d-99da-1b20e908d2c6
a = map(o -> o.src.v, MONAD_code[6:18:end])

# ╔═╡ c36ffc33-27c5-40e2-a53d-dae9d0383ab6
a .< 0

# ╔═╡ 98456ac2-5ceb-424d-8a9a-1f2a54619c44
b = map(o -> o.src.v, MONAD_code[16:18:end])

# ╔═╡ 9b0e6533-daff-4797-8ec0-14965035977d
b .+ 9

# ╔═╡ 366e64f8-8f70-4cad-8503-10c6d31df5b9
evalpoly(10, (4,8,1,1, 1,5,1,4, 7,1,9,1, 1,1) |> reverse)

# ╔═╡ a8a5fd7f-8039-4256-9b7a-19fe9974be9f
evalpoly(10, (9,9,9,9, 5,9,6,9, 9,1,9,3, 2,6) |> reverse)

# ╔═╡ 76c73f91-af98-4f09-bb0b-171853a810d6
function thing(i)
	z = 0
	for r=1:14
		d = (a[r] < 0) ? 26 : 1
		println("target: $(string(z % 26 + a[r], base=26)), new letter: $(string(i[r] + b[r], base=26)), d: $d")
		if i[r] != (z % 26 + a[r])
			z = (z ÷ d) * 26 + (i[r] + b[r])
		else
			z = (z ÷ d)
		end
		println("z: $(string(z, base=26))")
	end
	z
end

# ╔═╡ c9606ac0-6824-4350-8a3b-5207e9634a3a
with_terminal() do
	thing((9,9,9,9, 5,9,6,9, 9,1,9,3, 2,6))
end

# ╔═╡ c0980396-fb77-43f2-8c6e-68dab55d701a
with_terminal() do
	thing((4,8,1,1, 1,5,1,4, 7,1,9,1, 1,1))
end

# ╔═╡ c35c1b3c-fb13-44f4-8be7-2ebafb0ccf12
@benchmark c((12,))

# ╔═╡ aec5b2b4-51f0-4b3b-9d2f-d6dbd2683cc5
x = 5

# ╔═╡ 4f39fcfd-2008-4b81-9d20-58beb45f4dae
MONAD = compile(parse.(Instruction, input))

# ╔═╡ 33f365c8-db9c-48b0-8d73-b9e72c6bb8c3
n = digits(13579246899999) |> Tuple |> reverse

# ╔═╡ 92813de3-fd76-403b-8ef7-2f3b9dfb0ca8
with_terminal() do
	@code_warntype MONAD(n)
end

# ╔═╡ 4b54c4d0-5153-47b3-8600-2fb66b13a860
5 * 18 * n

# ╔═╡ 43a2ea4f-5c03-48cb-b244-391f513f8e86
@benchmark MONAD(n)

# ╔═╡ b9983028-a362-4284-a21a-8614bf5057e8


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
BenchmarkTools = "~1.2.2"
PlutoUI = "~0.7.27"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.0"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "940001114a0147b6e4d10624276d56d531dd9b49"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.2"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "d7fa6237da8004be601e19bd6666083056649918"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.3"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "fed057115644d04fba7f4d768faeeeff6ad11a60"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.27"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═694d4e44-6475-11ec-2590-f337943a56fe
# ╠═46e1d299-5c2e-4276-b818-17d7c932744f
# ╠═a46ae819-de11-48dc-b153-d348438e81fc
# ╠═b0ff9db4-bc2a-4d0d-92ea-9573449dbc8c
# ╠═f1ed8b65-a9a0-4bbe-ab1f-2bca3a109f3c
# ╠═48297d61-18a1-4d77-abdd-562397b178ec
# ╠═757d0b0f-6e11-45c4-a13b-391b5a2b1be9
# ╠═75271c21-c5f8-47ec-9da3-c86161d282c6
# ╠═c9844ba7-c681-4a31-b790-a6c9862b32ed
# ╠═fdc1e0c2-e5a9-4f8d-a211-44e4e2e76efb
# ╠═b01ab558-61ce-40f8-af38-c6e80a1fb15e
# ╠═1804af62-7094-4998-9ec4-f453c83c5f3f
# ╠═4a30f53c-8b2f-4472-a47f-64eee923a759
# ╠═4fdb556e-12cc-4b81-a71b-d6f0a6cbf0e4
# ╠═9b94e46e-712b-442c-adaa-571a612a73d1
# ╠═b40d42ca-5130-46ed-a7e5-e98746265039
# ╠═f3bd4873-97ae-49f6-905e-21931cc42538
# ╠═92813de3-fd76-403b-8ef7-2f3b9dfb0ca8
# ╠═5ff0b6e6-d232-4a8b-94d3-5c32aab1461d
# ╠═7b56eaeb-0f0a-450b-91cc-972ad2803fe8
# ╠═4b54c4d0-5153-47b3-8600-2fb66b13a860
# ╠═7211b806-2358-4cf8-be12-d4eb2d4aacc7
# ╠═a42bf967-1c15-434d-99da-1b20e908d2c6
# ╠═c36ffc33-27c5-40e2-a53d-dae9d0383ab6
# ╠═98456ac2-5ceb-424d-8a9a-1f2a54619c44
# ╠═9b0e6533-daff-4797-8ec0-14965035977d
# ╠═c9606ac0-6824-4350-8a3b-5207e9634a3a
# ╠═c0980396-fb77-43f2-8c6e-68dab55d701a
# ╠═366e64f8-8f70-4cad-8503-10c6d31df5b9
# ╠═a8a5fd7f-8039-4256-9b7a-19fe9974be9f
# ╠═76c73f91-af98-4f09-bb0b-171853a810d6
# ╠═c35c1b3c-fb13-44f4-8be7-2ebafb0ccf12
# ╠═aec5b2b4-51f0-4b3b-9d2f-d6dbd2683cc5
# ╠═4f39fcfd-2008-4b81-9d20-58beb45f4dae
# ╠═33f365c8-db9c-48b0-8d73-b9e72c6bb8c3
# ╠═43a2ea4f-5c03-48cb-b244-391f513f8e86
# ╠═b9983028-a362-4284-a21a-8614bf5057e8
# ╠═f70cd40e-1e5f-4e5d-b7ae-bd3b8044cd73
# ╠═845bbe61-fbfd-4d6e-8a54-130f3aa5ed4b
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
