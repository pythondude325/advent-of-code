#!/usr/bin/env -S dotnet fsi

let helper len input =
    Seq.map2 (<) input (Seq.skip len input)
    |> Seq.filter id
    |> Seq.length
let part1 = helper 1
let part2 = helper 3

let input =
    System.IO.File.ReadAllLines("01_input.txt")
    |> Array.map int

printfn $"part 1: {part1 input}"
printfn $"part 2: {part2 input}"