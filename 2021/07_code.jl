### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ 48cc254b-adb1-4f17-9187-930a9bf39775
using Statistics

# ╔═╡ 09411e48-5719-11ec-33c1-1dbbd946ff3c
const input = readlines("07_input_example.txt")

# ╔═╡ 5cbd83b7-8a9f-4736-a3a8-e689c4b1fda0
const crabs = parse.(Int,split(input[1],","))

# ╔═╡ f1982686-f526-4e63-9154-6f87019a4c67
length(crabs)

# ╔═╡ 9387f963-f69f-4ced-b3f2-73a68ae84339
sum(abs.(crabs .- round(Int,median(crabs))))

# ╔═╡ ee749f39-6db2-4ff0-8070-3fb9ed598a74
tri(n) = n*(n+1)÷2

# ╔═╡ 9e807ebe-11c1-4378-900a-2dc3e12d3539
minimum(sum(tri.(abs.(crabs .- x))) for x=1:length(crabs))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Statistics = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
"""

# ╔═╡ Cell order:
# ╠═09411e48-5719-11ec-33c1-1dbbd946ff3c
# ╠═5cbd83b7-8a9f-4736-a3a8-e689c4b1fda0
# ╠═f1982686-f526-4e63-9154-6f87019a4c67
# ╠═48cc254b-adb1-4f17-9187-930a9bf39775
# ╠═9387f963-f69f-4ced-b3f2-73a68ae84339
# ╠═ee749f39-6db2-4ff0-8070-3fb9ed598a74
# ╠═9e807ebe-11c1-4378-900a-2dc3e12d3539
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
