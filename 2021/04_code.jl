### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ f313faaa-e925-40bd-bf1b-0c37bc014814
using BenchmarkTools

# ╔═╡ 073baaac-5537-11ec-1478-1babdc830341
const input = readlines("04_input.txt")

# ╔═╡ bf3ae869-ccb7-43d8-968b-134ab1387592
const draw_numbers = parse.(Int,split(input[1],","))

# ╔═╡ 88dc1283-65a0-4559-a574-7929082948cc
function parse_board(lines)::Matrix{Int}
	parse.(Int, reduce(hcat,split.(lines)))'
end

# ╔═╡ 8088ea70-cbcd-44d6-847e-40afa79bd1f0
const boards = [parse_board(@view input[6*i-3:6*i+1]) for i=1:(length(input) ÷ 6)]

# ╔═╡ 774651ee-f72a-4244-909e-7a65a9f39b10
mash(itr) = reshape(reduce(hcat,itr), (size(first(itr))...,length(itr)))

# ╔═╡ 2062b9d6-e352-46b4-b733-bbf8a71e8c23
mash([[1 2;3 4], [4 5;6 7]])

# ╔═╡ 5927fecd-0954-4cbe-9626-05433ae2a4dc
reshape([1 3; 2 4], (4,1))

# ╔═╡ 15621bbb-a477-4f74-bb74-f943f70e5d67
function check_board(board, numbers)
	rows = [board[i,1:5] for i=1:5]
	columns = [board[1:5,i] for i=1:5]
	checks = vcat(rows,columns)
	any(c -> all(∈(numbers), c), checks)
end

# ╔═╡ c09acada-c2d2-4e72-bcae-8807fabf41c4
function score_board(board, ns)
	sum(filter(∉(ns), board)) * ns[end]
end

# ╔═╡ 9ae8acf4-12d3-4fbe-bc76-4f81b2170ffe
function day1(numbers, boards)
	for i=1:lastindex(numbers)
		ns = numbers[1:i]
		done_boards = collect(filter(b -> check_board(b, ns), boards))
		if length(done_boards) == 1
			return score_board(done_boards[1], ns)
		end
	end
end

# ╔═╡ d77de05a-5853-4af0-9033-81a990de20a3
day1(draw_numbers, boards)

# ╔═╡ 3778a658-15af-4f75-99f7-4e67bcb494c5
function day2(numbers, boards)
	for i=1:lastindex(numbers)
		ns = numbers[1:i]
		done_boards = collect(filter(b -> !check_board(b, ns), boards))
		if length(done_boards) == 1 # Only one board left now
			return day1(numbers, done_boards)
		end
	end
end

# ╔═╡ d0c75689-27fc-44cf-b64c-9e017b6d541e
day2(draw_numbers, boards)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"

[compat]
BenchmarkTools = "~1.2.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "61adeb0823084487000600ef8b1c00cc2474cd47"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.0"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "ae4bbcadb2906ccc085cf52ac286dc1377dceccc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.2"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"
"""

# ╔═╡ Cell order:
# ╠═f313faaa-e925-40bd-bf1b-0c37bc014814
# ╠═073baaac-5537-11ec-1478-1babdc830341
# ╠═bf3ae869-ccb7-43d8-968b-134ab1387592
# ╠═88dc1283-65a0-4559-a574-7929082948cc
# ╠═8088ea70-cbcd-44d6-847e-40afa79bd1f0
# ╠═774651ee-f72a-4244-909e-7a65a9f39b10
# ╠═2062b9d6-e352-46b4-b733-bbf8a71e8c23
# ╠═5927fecd-0954-4cbe-9626-05433ae2a4dc
# ╠═15621bbb-a477-4f74-bb74-f943f70e5d67
# ╠═c09acada-c2d2-4e72-bcae-8807fabf41c4
# ╠═9ae8acf4-12d3-4fbe-bc76-4f81b2170ffe
# ╠═d77de05a-5853-4af0-9033-81a990de20a3
# ╠═3778a658-15af-4f75-99f7-4e67bcb494c5
# ╠═d0c75689-27fc-44cf-b64c-9e017b6d541e
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
