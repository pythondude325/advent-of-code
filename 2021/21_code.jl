### A Pluto.jl notebook ###
# v0.17.3

using Markdown
using InteractiveUtils

# ╔═╡ 87a0f720-beac-4ac6-8ef5-dd215018165b
using Memoization

# ╔═╡ 5fe214f5-7b00-468d-a5d3-dd9a1bacc307
using PlutoUI

# ╔═╡ c631ed0b-c03f-4f50-b82a-170a570b7c95
using BenchmarkTools

# ╔═╡ 62b46c1a-57d5-4b76-849e-b8ffbcea9a59
using StaticArrays

# ╔═╡ c92cc0f2-621a-11ec-16f6-159811d64709
const input = readlines("21_input.txt")

# ╔═╡ c708150d-a4f7-4630-a997-17faa9132e0d
parse_player(i) = parse(Int,split(i)[end])

# ╔═╡ d5acf356-b369-4f99-8d03-b25735a59d30
player1 = parse_player(input[1])

# ╔═╡ 71a1e6a0-c309-4606-9142-7762d050563f
player2 = parse_player(input[2])

# ╔═╡ 3acbfe94-c6e8-403c-98ce-aa6d2c9b059b
function play_game(a, b)
	a -= 1
	b -= 1
	r = 0
	d = 1
	score_a = 0
	score_b = 0
	while score_a < 1000 && score_b < 1000
		
		a = (a+(d+d+1+d+2)) % 10
		d = (d+3) % 100
		r += 3
		score_a += a+1 
		
		if score_a >= 1000
			break
		end
		
		b = (b+(d+d+1+d+2)) % 10
		d = (d+3) % 100
		r += 3
		score_b += b+1
	end
	
	min(score_a, score_b) * r
end
		

# ╔═╡ d869d2b4-65bb-4228-ad33-a8b8a4af70a3
possible_rolls =
	reshape(1:3, :, 1, 1) .+
	reshape(1:3, 1, :, 1) .+
	reshape(1:3, 1, 1, :)

# ╔═╡ 17d46cad-b8dc-4b4a-b018-211f0d68c9e4
rolls = Dict(c => count(==(c), possible_rolls) for c=3:9)

# ╔═╡ 773bcf4b-19ab-4264-9b43-26ebd11b5321
function play2()
	state = Array{Tuple{Int,Int}}(undef, 10,10,21,21)
	for scorea=1:21,scoreb=1:21
		for b=1:10,a=1:10
			total = (0,0)
			for (r,t)=rolls
				new_a = (a+r-1) % 10 + 1
				new_score = scorea - new_a
				if new_score <= 0
					total = total .+ (t .* (1,0))
				else
					total = total .+ (t .* reverse(state[b,new_a,scoreb,new_score]))
				end
			end
			state[a,b,scorea,scoreb] = total
		end
	end
	state
end

# ╔═╡ f0d9b4b1-1e70-4868-8f60-4a47b40c5199
[
	(scoreb, scorea .- (9:-1:3))
	for scorea=1:21,scoreb=1:21
]

# ╔═╡ e82f40b7-af81-49b5-a128-5a8df56e6f19
[
	(y,x)
	for y=1:3,x=1:3
]

# ╔═╡ cd54405b-6a4a-4f2f-9aad-006edb0942c1
state = play2()

# ╔═╡ 76f7538b-afab-4670-927c-0ecdf8a1f375
state[4,8,21,21]

# ╔═╡ 8591ee85-043a-4643-b195-3d00c9b8e7ef
@memoize function play2a(a, b, scorea, scoreb)
	total = MVector(0,0)
	for (r,t)=rolls
		new_a = (a-1+r) % 10 + 1
		new_score = scorea - new_a
		if new_score <= 0
			total .+= t .* SVector(1,0)
		else
			total .+= t .* reverse(play2a(b, new_a, scoreb, new_score))
		end
	end
	total
end

# ╔═╡ 86cad4df-7be5-4e88-817d-b88e5a2a9502
play2a(4,8,21,21)

# ╔═╡ c348adc7-497a-4103-82d9-9cd38f042d05
maximum(play2a(4,8,21,21))

# ╔═╡ 1cbcccba-5a1e-4b44-bbd4-6d337a1c53fb
[444356092776315, 341960390180808]

# ╔═╡ c4cda454-5923-452b-8839-136774200ed5
739785

# ╔═╡ 803dc451-ed22-4440-888b-f2f0fdfdb877
play_game(4,8)

# ╔═╡ 292f6bb2-1bc4-4676-a462-40112406f45e
play_game(player1, player2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
Memoization = "6fafb56a-5788-4b4e-91ca-c0cea6611c73"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[compat]
BenchmarkTools = "~1.2.2"
Memoization = "~0.1.13"
PlutoUI = "~0.7.25"
StaticArrays = "~1.2.13"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.0"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "37b730f25b5662ac452f7bb2c50a0567cbb748d4"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.3"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "940001114a0147b6e4d10624276d56d531dd9b49"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.2"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Memoization]]
deps = ["MacroTools"]
git-tree-sha1 = "42d48cac95e0fee730ba2fff477adc68c52c65f4"
uuid = "6fafb56a-5788-4b4e-91ca-c0cea6611c73"
version = "0.1.13"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "d7fa6237da8004be601e19bd6666083056649918"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.3"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "93cf0910f09a9607add290a3a2585aa376b4feb6"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.25"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "3c76dde64d03699e074ac02eb2e8ba8254d428da"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.2.13"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═c92cc0f2-621a-11ec-16f6-159811d64709
# ╠═c708150d-a4f7-4630-a997-17faa9132e0d
# ╠═d5acf356-b369-4f99-8d03-b25735a59d30
# ╠═71a1e6a0-c309-4606-9142-7762d050563f
# ╠═87a0f720-beac-4ac6-8ef5-dd215018165b
# ╠═3acbfe94-c6e8-403c-98ce-aa6d2c9b059b
# ╠═d869d2b4-65bb-4228-ad33-a8b8a4af70a3
# ╠═17d46cad-b8dc-4b4a-b018-211f0d68c9e4
# ╠═773bcf4b-19ab-4264-9b43-26ebd11b5321
# ╠═f0d9b4b1-1e70-4868-8f60-4a47b40c5199
# ╠═e82f40b7-af81-49b5-a128-5a8df56e6f19
# ╠═cd54405b-6a4a-4f2f-9aad-006edb0942c1
# ╠═76f7538b-afab-4670-927c-0ecdf8a1f375
# ╠═8591ee85-043a-4643-b195-3d00c9b8e7ef
# ╠═86cad4df-7be5-4e88-817d-b88e5a2a9502
# ╠═c348adc7-497a-4103-82d9-9cd38f042d05
# ╠═1cbcccba-5a1e-4b44-bbd4-6d337a1c53fb
# ╠═c4cda454-5923-452b-8839-136774200ed5
# ╠═803dc451-ed22-4440-888b-f2f0fdfdb877
# ╠═292f6bb2-1bc4-4676-a462-40112406f45e
# ╠═5fe214f5-7b00-468d-a5d3-dd9a1bacc307
# ╠═c631ed0b-c03f-4f50-b82a-170a570b7c95
# ╠═62b46c1a-57d5-4b76-849e-b8ffbcea9a59
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
