### A Pluto.jl notebook ###
# v0.17.3

using Markdown
using InteractiveUtils

# ╔═╡ 8a8d3718-5fbe-11ec-1d7f-cd238b4969f2
const input = readlines("18_input.txt")

# ╔═╡ 1fe25bdd-2062-4999-960c-3aabfb9f14e1
abstract type SnailNumber end

# ╔═╡ fbbdef30-de77-4bfe-b348-946a239732b2
struct SnailDigit <: SnailNumber
	d::Int
end

# ╔═╡ db7f1730-48b0-4e9c-bf25-c7ef4b9edf61
snailnumber_to_string(n::SnailDigit) = string(n.d)

# ╔═╡ 0de00b3b-4d0b-4281-96d5-b58571e7395f
struct SnailPair <: SnailNumber
	a::SnailNumber
	b::SnailNumber
end

# ╔═╡ f1435bf2-f88a-48bd-90da-b5e16546cd6a
snailnumber_to_string(n::SnailPair) = "[$(snailnumber_to_string(n.a)),$(snailnumber_to_string(n.b))]"

# ╔═╡ 2d763610-75e9-49ee-8fd8-9c1a59edf2ea
abstract type ReduceAction end

# ╔═╡ 0246b734-dcd9-447f-95f0-7028b8796f27
struct ExplodeResult
	left::Union{Int,Missing}
	right::Union{Int,Missing}
end

# ╔═╡ a5c88310-b9ee-4125-9e17-dd267fba4b76
has_explode_result(r) = !ismissing(r.left) || !ismissing(r.right)

# ╔═╡ dc6ee633-d284-481f-9550-ec02d8807608
const explode_none = ExplodeResult(missing, missing)

# ╔═╡ 6619cbb6-4c45-42c5-9a54-c551b13018ba
add_leftmost(n::SnailPair, a::Int) = SnailPair(add_leftmost(n.a, a), n.b)

# ╔═╡ 48ef894a-99f6-4ffa-8cdb-fff0eef1232a
add_rightmost(n::SnailPair, a::Int) = SnailPair(n.a, add_rightmost(n.b, a))

# ╔═╡ 4395ce75-754f-4317-8e73-27fff348c5bb
function reduce_pairs(n::SnailDigit, level::Int)
	(n, explode_none, false)
end

# ╔═╡ 6118e2d0-21bd-46bf-a6af-2cd8c2ef9b7a
function reduce_split(n::SnailDigit)::Tuple{SnailNumber, Bool}
	if n.d >= 10
		(SnailPair(
			SnailDigit(floor(Int, n.d/2)),
			SnailDigit(ceil(Int, n.d/2)),
		), true)
	else
		(n, false)
	end
end

# ╔═╡ bf9dc737-2837-4e00-87fd-41d75b6e82cd
function reduce_split(n::SnailPair)::Tuple{SnailNumber, Bool}
	left, left_action_taken = reduce_split(n.a)
	if left_action_taken
		return (SnailPair(left, n.b), true)
	end

	right, right_action_taken = reduce_split(n.b)
	if right_action_taken
		return (SnailPair(n.a, right), true)
	end

	(n, false)
end

# ╔═╡ 58ab23ec-3f4e-4cc2-9868-7beb54959b12
snail_add(a::SnailNumber, b::SnailNumber) = reduce_snail_number(SnailPair(a,b))

# ╔═╡ 3a68b18e-d432-48de-8c0e-d5f79bdba3f0
begin
	import Base: +

	+(a::SnailNumber, b::SnailNumber) = snail_add(a,b)
end

# ╔═╡ 8ccb96df-b40c-432f-91bc-ca61f32c0efd
begin 
	import Base: parse

	function parse(::Type{SnailNumber}, s)::SnailNumber
		if s[1] == '['
			bracket_count = 0
			offset = 2
			while s[offset] != ',' || bracket_count != 0
				c = s[offset]
				if c == '['
					bracket_count += 1
				elseif c == ']'
					bracket_count -= 1
				end
				offset += 1
			end
			a = parse(SnailNumber, s[2:offset-1])
			b = parse(SnailNumber, s[offset+1:end-1])
			SnailPair(a, b)
		else
			SnailDigit(parse(Int, s))
		end
	end
end

# ╔═╡ f34cb8ba-a837-4af2-90f6-fa35043723d5
const snail_numbers = parse.(SnailNumber, input)

# ╔═╡ f29817fa-9532-4cad-a22e-9b8746ef2d51
add_leftmost(n::SnailDigit, a::Int) = SnailDigit(n.d + a)

# ╔═╡ 3b603f23-abad-4b0b-af48-1710a687d04b
add_rightmost(n::SnailDigit, a::Int) = SnailDigit(n.d + a)

# ╔═╡ 8a893398-121c-4d6c-a382-054bca88fa0e
function reduce_pairs(n::SnailPair, level::Int)::Tuple{SnailNumber, ExplodeResult, Bool}
	if level > 4
		# Explode
		if n.a isa SnailDigit && n.b isa SnailDigit
			return (SnailDigit(0), ExplodeResult(n.a.d, n.b.d), true)
		else
			error("shit")
		end
	end

	# propagate explosions
	left, left_explode_result, left_action_taken = reduce_pairs(n.a, level + 1)
	if left_action_taken
		if !ismissing(left_explode_result.right)
			return (
				SnailPair(left, add_leftmost(n.b, left_explode_result.right)),
				ExplodeResult(left_explode_result.left, missing),
				true,
			)
		else
			return (SnailPair(left, n.b), left_explode_result, true)
		end
	end

	right, right_explode_result, right_action_taken = reduce_pairs(n.b, level +1)
	if right_action_taken
		if !ismissing(right_explode_result.left)
			return (
				SnailPair(add_rightmost(n.a, right_explode_result.left), right),
				ExplodeResult(missing, right_explode_result.right),
				true,
			)
		else
			return (SnailPair(n.a, right), right_explode_result, true)
		end
	end
	(n, explode_none, false)
end

# ╔═╡ 6b2c3b8c-af9c-4a31-84a5-03deb7d7cb77
function reduce_snail_number(n::SnailNumber)::SnailNumber
	action_taken = true
	while action_taken
		action_taken = false

		n, r, action_taken = reduce_pairs(n, 1)
		if action_taken
			continue
		end

		n, action_taken = reduce_split(n)
		if action_taken
			continue
		end
		
	end
	n
end

# ╔═╡ 94480ccd-b999-4d0b-89e9-9d9f4eacf443
magnitude(n::SnailDigit)::Int = n.d

# ╔═╡ 96c3682d-772a-438b-a6d9-2fa84fbc240b
magnitude(n::SnailPair)::Int = 3*magnitude(n.a) + 2*magnitude(n.b)

# ╔═╡ 25f98371-e579-41be-a0df-0607fc8a6912
snailsum(x) = foldl(snail_add, x)

# ╔═╡ eab272b2-f101-4e5e-843e-efccb3a30859
part1 = snail_numbers |> sum |> magnitude

# ╔═╡ 16d5e629-cf87-442d-ac50-561c5f879464
part2 = snail_numbers .+ reshape(snail_numbers, 1, :) .|> magnitude |> maximum

# ╔═╡ cad8fabe-6474-4df2-b1ef-80d189dc4f62


# ╔═╡ Cell order:
# ╠═8a8d3718-5fbe-11ec-1d7f-cd238b4969f2
# ╠═db7f1730-48b0-4e9c-bf25-c7ef4b9edf61
# ╠═f1435bf2-f88a-48bd-90da-b5e16546cd6a
# ╠═1fe25bdd-2062-4999-960c-3aabfb9f14e1
# ╠═fbbdef30-de77-4bfe-b348-946a239732b2
# ╠═0de00b3b-4d0b-4281-96d5-b58571e7395f
# ╠═8ccb96df-b40c-432f-91bc-ca61f32c0efd
# ╠═f34cb8ba-a837-4af2-90f6-fa35043723d5
# ╠═2d763610-75e9-49ee-8fd8-9c1a59edf2ea
# ╠═0246b734-dcd9-447f-95f0-7028b8796f27
# ╠═a5c88310-b9ee-4125-9e17-dd267fba4b76
# ╠═dc6ee633-d284-481f-9550-ec02d8807608
# ╠═f29817fa-9532-4cad-a22e-9b8746ef2d51
# ╠═6619cbb6-4c45-42c5-9a54-c551b13018ba
# ╠═3b603f23-abad-4b0b-af48-1710a687d04b
# ╠═48ef894a-99f6-4ffa-8cdb-fff0eef1232a
# ╠═4395ce75-754f-4317-8e73-27fff348c5bb
# ╠═8a893398-121c-4d6c-a382-054bca88fa0e
# ╠═6118e2d0-21bd-46bf-a6af-2cd8c2ef9b7a
# ╠═bf9dc737-2837-4e00-87fd-41d75b6e82cd
# ╠═6b2c3b8c-af9c-4a31-84a5-03deb7d7cb77
# ╠═3a68b18e-d432-48de-8c0e-d5f79bdba3f0
# ╠═58ab23ec-3f4e-4cc2-9868-7beb54959b12
# ╠═94480ccd-b999-4d0b-89e9-9d9f4eacf443
# ╠═96c3682d-772a-438b-a6d9-2fa84fbc240b
# ╠═25f98371-e579-41be-a0df-0607fc8a6912
# ╠═eab272b2-f101-4e5e-843e-efccb3a30859
# ╠═16d5e629-cf87-442d-ac50-561c5f879464
# ╠═cad8fabe-6474-4df2-b1ef-80d189dc4f62
