### A Pluto.jl notebook ###
# v0.17.3

using Markdown
using InteractiveUtils

# ╔═╡ f8f30b5d-f072-4e51-9266-f21ea38abff8
using BenchmarkTools

# ╔═╡ 5a23be98-653f-11ec-3e25-cb346518c13f
const input = readlines("25_input.txt")

# ╔═╡ 7957d86e-807b-4da7-bbe5-6bed60d2a7d2
@enum Cucumber::Int8 begin
	None
	East
	South
end

# ╔═╡ a2b019ea-e79c-4371-8a77-02f6f2b0e5a5
Base.show(io::IO, ::MIME"text/plain", c::Cucumber) =
	if c == None
		print(io, ".")
	elseif c == East
		print(io, ">")
	elseif c == South
		print(io, "v")
	end

# ╔═╡ e2b1e3b8-9a82-4632-ac13-a170f15781f7
Base.parse(::Type{Cucumber}, c::Char) =
	if c == '.'
		None
	elseif c == '>'
		East
	elseif c == 'v'
		South
	end

# ╔═╡ 208209a4-da92-48cd-94f4-4d9b1ed123d8
struct Visualize
	a::AbstractMatrix{Cucumber}
end

# ╔═╡ 5bd77dc5-da31-4f98-b088-867145024152
function Base.show(io::IO, mime::MIME"text/plain", v::Visualize)
	m = v.a
	for col=eachcol(m)
		for c=col
			show(io,mime,c)
		end
		println(io)
	end
end

# ╔═╡ 998990ac-3eda-470a-9dc4-8b60b782f5f6
const seafloor = reduce(hcat, parse.(Cucumber, collect(l)) for l=input)

# ╔═╡ 53dc0d8f-5122-4eaf-8042-605f8970693a
Visualize(seafloor)

# ╔═╡ 66758a9c-86fc-4217-8b03-e7756ce82a44


# ╔═╡ 931195c7-eedd-44e1-a5c8-50623136949b
function step!(sf)
	occ_e = circshift(sf, (-1,0)) .== None
	herd_e = sf .== East
	move_e =  occ_e .& herd_e
	sf[move_e] .= None
	sf[circshift(move_e, (1,0))] .= East

	occ_s = circshift(sf, (0,-1)) .== None
	herd_s = sf .== South
	move_s = occ_s .& herd_s
	sf[move_s] .= None
	sf[circshift(move_s, (0,1))] .= South
end

# ╔═╡ 17657ad7-d54e-4b12-8ed7-5ad82cb11628
function wait(sf)
	sf = copy(sf)
	step = 0

	while true
		last_sf = copy(sf)
		step!(sf)
		step += 1
		if last_sf == sf
			return step
		end
	end
end

# ╔═╡ b0927e5f-2abd-49a2-90f3-14b15afcb3de
wait(seafloor)

# ╔═╡ 9f242563-9da3-4247-b056-e2e30d78789a
@benchmark step!($(copy(seafloor)))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"

[compat]
BenchmarkTools = "~1.2.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.0"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "940001114a0147b6e4d10624276d56d531dd9b49"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.2"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "d7fa6237da8004be601e19bd6666083056649918"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.3"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═5a23be98-653f-11ec-3e25-cb346518c13f
# ╠═a2b019ea-e79c-4371-8a77-02f6f2b0e5a5
# ╠═e2b1e3b8-9a82-4632-ac13-a170f15781f7
# ╠═7957d86e-807b-4da7-bbe5-6bed60d2a7d2
# ╠═5bd77dc5-da31-4f98-b088-867145024152
# ╠═53dc0d8f-5122-4eaf-8042-605f8970693a
# ╠═208209a4-da92-48cd-94f4-4d9b1ed123d8
# ╠═998990ac-3eda-470a-9dc4-8b60b782f5f6
# ╠═66758a9c-86fc-4217-8b03-e7756ce82a44
# ╠═b0927e5f-2abd-49a2-90f3-14b15afcb3de
# ╠═17657ad7-d54e-4b12-8ed7-5ad82cb11628
# ╠═931195c7-eedd-44e1-a5c8-50623136949b
# ╠═9f242563-9da3-4247-b056-e2e30d78789a
# ╠═f8f30b5d-f072-4e51-9266-f21ea38abff8
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
