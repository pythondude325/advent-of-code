### A Pluto.jl notebook ###
# v0.17.3

using Markdown
using InteractiveUtils

# ╔═╡ 0b5ecc0b-7f17-403b-9728-64dcec631008
using Graphs

# ╔═╡ f5e86ba6-eadf-431a-b2aa-873bae6ea5f9
using LinearAlgebra

# ╔═╡ 3b2752f1-6a57-4a57-8cd8-42d9a68d81d8
using StaticArrays

# ╔═╡ 0a0bf48e-6087-11ec-01ff-bd49caebaba8
const input = readlines("19_input_example.txt")

# ╔═╡ f768e052-11f7-403e-96b0-8664956b125f
sins = [0, 1, 0, -1]

# ╔═╡ 6cec5f35-169a-40b5-8490-be91f419faa1
coss = circshift(sins, -1)

# ╔═╡ ea114088-aa31-4db3-96e0-addf8aed6998
det([1 0 0; 0 -1 0; 0 0 -1])

# ╔═╡ 53c4c8dc-edd4-4dee-b922-4f579dde6735
rotation_yaw(a) = SA[
	cos(a) -sin(a) 0 0;
	sin(a) cos(a) 0 0;
	0 0 1 0;
	0 0 0 1;
]

# ╔═╡ 69740f3f-0ccb-48fa-b223-0edc3c9a34d3
rotation_pitch(a) = SA[
	cos(a) 0 sin(a) 0;
	0 1 0 0;
	-sin(a) 0 cos(a) 0;
	0 0 0 1;
]

# ╔═╡ 9c956dfb-4bbc-4573-859a-2fadc2e60700
rotation_roll(a) = SA[
	1 0 0 0;
	0 cos(a) -sin(a) 0;
	0 sin(a) cos(a) 0;
	0 0 0 1;
]

# ╔═╡ 1f149863-ae45-46e4-bc48-dc16bab6b2b7
a = [0, pi/2, pi, 3pi/2]

# ╔═╡ d5f0aff5-a35d-43e1-ac40-311d3ccd0d1a
rotations = unique([round.(Int, rotation_yaw(y)*rotation_pitch(p)*rotation_roll(r)) for y=a,p=a,r=a])

# ╔═╡ 561f24ae-205b-49b4-92b2-80a0ad47b758
distance(a,b) = let d = a[1:3] .- b[1:3]
	dot(d,d)
end

# ╔═╡ 98def753-a364-4579-a817-695d910d6cc6
const Coord = SVector{4,Int}

# ╔═╡ 4dd1762d-7169-4493-8b32-1038e2f45e0b
parse_coord(x) = Coord([parse.(Int,split(x,",")); 1])

# ╔═╡ b7c469de-d91e-41fe-90f5-98590404b869
function parse_scanner_data(x)
	x = collect(Iterators.takewhile(!=(""), x))
	parse_coord.(x[2:end])
end

# ╔═╡ 9d9a9cbb-9d60-48fb-a767-b78050bb148a
 const scanners = begin 
	ss = []
	local offset = 1
	while offset < length(input)
		new_scanner = parse_scanner_data(input[offset:end])
		push!(ss, new_scanner)
		offset += 2+length(new_scanner)
	end
	ss
end

# ╔═╡ 8b67aa1b-b8a8-44aa-b86d-cc5564df6002
N = length(scanners)

# ╔═╡ d0c58641-86b1-4060-8cd1-61b31b755b3c
const Transform = SMatrix{4,4,Int}

# ╔═╡ e7c6101c-149e-46cc-a80e-7d9303115a7e
const Scanner = Vector{Coord}

# ╔═╡ 4b1a322f-bd10-4e86-9e14-38bb11c1e487
translate(x::SVector{4}) = SA[
	1 0 0 x[1];
	0 1 0 x[2];
	0 0 1 x[3];
	0 0 0 1;
]

# ╔═╡ 63f95fff-d391-4303-a582-6584891ea9f4
negate(v::Coord)::Coord = SA[-v[1], -v[2], -v[3], 1]

# ╔═╡ f6de87ad-1fa0-4a60-8f44-d77077f20038
function find_transform(scanner1::Scanner, scanner2::Scanner)
	for rot=rotations
		for beacon1 = scanner1
			for beacon2=scanner2
				transform = 
					translate(negate(rot * beacon2)) *
					translate(beacon1) *
					rot
	
				matches = scanner1 .== reshape(Ref(transform) .* scanner2, 1, :)
				
				if sum(matches) >= 12
					return transform
				end
			end
		end
	end

	return missing
end

# ╔═╡ 4b123208-dd32-4d4e-8584-7f581e4f5497
manhattan_distance(t::SMatrix{4,4,Int}) = sum(abs.((t * SA[0,0,0,1])[1:3]))

# ╔═╡ 9791b49f-e8c4-45b1-8883-2c1808363eb2
find_transform(scanners[2+1], scanners[4+1])

# ╔═╡ d093f74f-6b82-4ddc-a3bd-967369f67e91


# ╔═╡ f2f4cc78-4899-42ba-abd2-211d4599a16b
fingerprint_scanner(scanner::Scanner) =
	Set(
		distance(a,b)
		for (a,b)=Iterators.filter(x -> x[1]!=x[2], (a,b) for a=scanner,b=scanner)
	)

# ╔═╡ 9dea4116-2e04-4344-89dd-39a3b02edc3f
beacons_required = 12

# ╔═╡ 542be06e-d8ee-4751-a07e-1a0c602fca34
fingerprint_match_threshold = (beacons_required^2-beacons_required)÷2

# ╔═╡ 639415a1-0874-4f4f-b0c3-b1b00bb2c7c6
const fingerprint_matches = begin
	local fingerprints = fingerprint_scanner.(scanners)
	[
		length(f1 ∩ f2) >= fingerprint_match_threshold
		for f1=fingerprints,f2=fingerprints
	]
end

# ╔═╡ 6a3f4407-0a4a-4d78-8265-dc2e0a44e5f9
transform_graph = SimpleGraph(fingerprint_matches)

# ╔═╡ a59826e1-472e-4f3e-b2b7-c79900455deb
const ts = begin
	local transforms = Matrix{Union{Missing,Transform}}(fill(missing, N,N))
	for a=1:N
		for b=1:N
			if a<b && fingerprint_matches[a,b]
				transforms[a,b] = find_transform(scanners[a],scanners[b])
			elseif a==b
				transforms[a,b] = SMatrix{4,4,Int}(I)
			else
				if !ismissing(transforms[b,a])
					transforms[a,b] = Int.(inv(transforms[b,a]))
				end
			end
		end
	end
	transforms
end

# ╔═╡ 702d37ee-d2d3-4924-b900-552710d8b0e7
get_transform(a,b)::Transform =
	prod(
		(ts[e.src,e.dst] for e=a_star(transform_graph, a,b)),
		init=Transform(I)
	)

# ╔═╡ a90fa2cc-8b46-4b71-b91a-ff1289d05266
part1 = reduce(vcat, [Ref(get_transform(1,i)) .* s for (i,s)=enumerate(scanners)]) |> unique |> length

# ╔═╡ a7f76c50-230f-4139-8535-5a9990a78f1d
part2 = maximum([manhattan_distance(get_transform(a,b)) for a=1:N,b=1:N])

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[compat]
Graphs = "~1.4.1"
StaticArrays = "~1.2.13"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.0"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "62e51b39331de8911e4a7ff6f5aaf38a5f4cc0ae"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "44c37b4636bc54afac5c574d2d02b625349d6582"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.41.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.Graphs]]
deps = ["ArnoldiMethod", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "92243c07e786ea3458532e199eb3feee0e7e08eb"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.4.1"

[[deps.Inflate]]
git-tree-sha1 = "f5fc07d4e706b84f72d54eedcc1c13d92fb0871c"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "3c76dde64d03699e074ac02eb2e8ba8254d428da"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.2.13"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═0a0bf48e-6087-11ec-01ff-bd49caebaba8
# ╠═4dd1762d-7169-4493-8b32-1038e2f45e0b
# ╠═b7c469de-d91e-41fe-90f5-98590404b869
# ╠═9d9a9cbb-9d60-48fb-a767-b78050bb148a
# ╠═8b67aa1b-b8a8-44aa-b86d-cc5564df6002
# ╠═f768e052-11f7-403e-96b0-8664956b125f
# ╠═6cec5f35-169a-40b5-8490-be91f419faa1
# ╠═ea114088-aa31-4db3-96e0-addf8aed6998
# ╠═53c4c8dc-edd4-4dee-b922-4f579dde6735
# ╠═69740f3f-0ccb-48fa-b223-0edc3c9a34d3
# ╠═9c956dfb-4bbc-4573-859a-2fadc2e60700
# ╠═1f149863-ae45-46e4-bc48-dc16bab6b2b7
# ╠═d5f0aff5-a35d-43e1-ac40-311d3ccd0d1a
# ╠═561f24ae-205b-49b4-92b2-80a0ad47b758
# ╠═98def753-a364-4579-a817-695d910d6cc6
# ╠═d0c58641-86b1-4060-8cd1-61b31b755b3c
# ╠═e7c6101c-149e-46cc-a80e-7d9303115a7e
# ╠═f6de87ad-1fa0-4a60-8f44-d77077f20038
# ╠═4b1a322f-bd10-4e86-9e14-38bb11c1e487
# ╠═63f95fff-d391-4303-a582-6584891ea9f4
# ╠═6a3f4407-0a4a-4d78-8265-dc2e0a44e5f9
# ╠═702d37ee-d2d3-4924-b900-552710d8b0e7
# ╠═a90fa2cc-8b46-4b71-b91a-ff1289d05266
# ╠═4b123208-dd32-4d4e-8584-7f581e4f5497
# ╠═a7f76c50-230f-4139-8535-5a9990a78f1d
# ╠═9791b49f-e8c4-45b1-8883-2c1808363eb2
# ╠═d093f74f-6b82-4ddc-a3bd-967369f67e91
# ╠═a59826e1-472e-4f3e-b2b7-c79900455deb
# ╠═639415a1-0874-4f4f-b0c3-b1b00bb2c7c6
# ╠═f2f4cc78-4899-42ba-abd2-211d4599a16b
# ╠═542be06e-d8ee-4751-a07e-1a0c602fca34
# ╠═9dea4116-2e04-4344-89dd-39a3b02edc3f
# ╠═0b5ecc0b-7f17-403b-9728-64dcec631008
# ╠═f5e86ba6-eadf-431a-b2aa-873bae6ea5f9
# ╠═3b2752f1-6a57-4a57-8cd8-42d9a68d81d8
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
