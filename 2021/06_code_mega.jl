using StaticArrays
using LinearAlgebra
using Octavian

const fish = begin
	const input = readlines("06_input.txt")
	parse.(Int,split(input[1], ","))
end

const counts = SVector(begin
	fish_counts = zeros(MVector{5,Int})
	for f=fish
		fish_counts[f] += 1
	end
	fish_counts
end)

function exponent(x, n)
	y = I
	while n > 1
		if isodd(n)
			y = matmul(y, x)
		end
		x = matmul(x, x)
		n ÷= 2
	end
	return x*y
end


const in_mat = [zeros(SMatrix{1,5,Int}); SMatrix{8,5}(I)]

const day_mat = SA[0 1 0 0 0 0 0 0 0;
				   0 0 1 0 0 0 0 0 0;
				   0 0 0 1 0 0 0 0 0;
				   0 0 0 0 1 0 0 0 0;
				   0 0 0 0 0 1 0 0 0;
				   0 0 0 0 0 0 1 0 0;
				   1 0 0 0 0 0 0 1 0;
				   0 0 0 0 0 0 0 0 1;
				   1 0 0 0 0 0 0 0 0]

const out_mat = ones(SMatrix{1,9,Int})

@time const mega_precompute = out_mat * big.(day_mat) ^ 1_000_000 * in_mat

@time const mega_number2 = sum(f -> mega_precompute[f], fish)

println(log10(mega_number2))
