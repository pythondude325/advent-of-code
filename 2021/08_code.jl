### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ d62c44e7-9d25-4107-8db8-2095f50de850
using LinearAlgebra

# ╔═╡ cbbcbb63-2f70-4c50-8d38-0e58faf47e2f
using StaticArrays

# ╔═╡ 1a6a93a1-4b24-4ccf-8921-1076a62bd904
using BenchmarkTools

# ╔═╡ 04282206-57e3-11ec-38d8-2b60d87b8dfb
const input = readlines("08_input.txt")

# ╔═╡ 7e352faa-8797-4393-9090-50a4737cd315
parse_signal(s) = BitVector(c in s for c="abcdefg")

# ╔═╡ fa163b90-9fe3-42e7-8f1a-506086259f5c
function parse_line(line)::Tuple{BitMatrix,BitMatrix}
	p,r = split(line, "|")
	reduce(hcat, parse_signal.(split(p))), reduce(hcat, parse_signal.(split(r)))
end

# ╔═╡ 6bfd4f9b-b2a1-44e4-9c64-9727bd7b2df5
const cases = parse_line.(input)

# ╔═╡ 9c706f98-582a-42b7-a72e-887d325b871f
casesSA = [(SMatrix{7,10,Bool}(h), SMatrix{7,4,Bool}(a)) for (h,a)=cases]

# ╔═╡ 675bf269-b7ec-46c4-bdb1-1ecbcb57c152
const digits = SMatrix{7,10,Bool}([
	1 0 1 1 0 1 1 1 1 1;
	1 0 0 0 1 1 1 0 1 1;
	1 1 1 1 1 0 0 1 1 1;
	0 0 1 1 1 1 1 0 1 1;
	1 0 1 0 0 0 1 0 1 0;
	1 1 0 1 1 1 1 1 1 1;
	1 0 1 1 0 1 1 0 1 1;
])

# ╔═╡ 058b79c3-8dfa-45fe-9e23-f9106a0babe3
idk(w, M) = M * (w .== ones(Int,1,7) * M)'

# ╔═╡ 4aaf36e5-a114-46f0-8abe-abd45360f044
function f2(w, case)
	idk(w, digits) .== idk(w, case)'
end

# ╔═╡ 2d1cdb44-9888-4792-9f26-4335dc3ca44f
function solve2(case)
	reduce(.&, f2(n, case) for n=2:6)
end

# ╔═╡ 0c3ec32a-fc7d-405d-bcfe-0cc0d0834bfb
decode_digit(d) = (reduce(&, digits .== d, dims=1) * (0:9))[1]

# ╔═╡ e7ec7537-1cf5-4a82-b7c3-2c7f1b8fa55b
decode(a) = [decode_digit(a[:,i]) for i=1:4]

# ╔═╡ 2393e003-2144-4301-98ed-436b6f21f897


# ╔═╡ 63465ce8-650f-4414-91d7-85aefc2eb5ab
function outer_equals(a::SMatrix{N,P}, b::SMatrix{P,M})::SMatrix{N,M} where {N,P,M}
	[prod(a[n,:] .== b[:,m]) for n=1:N, m=1:M]
end

# ╔═╡ 496da024-e583-4fd5-92c0-89b1c4d51e3c


# ╔═╡ eab18250-991a-4113-b1e2-710a2204f67a
function genmul(c, r, a::AbstractMatrix, b::AbstractMatrix)
	N,P1 = size(a)
	P2,M = size(b)
	@assert P1==P2 "Matrix sizes do not match"
	[reduce(r, map(c, a[n,:], b[:,m])) for n=1:N, m=1:M]
end

# ╔═╡ 1dbc66c2-8588-4256-ae73-e82adcb00bcf
function genmul(c, r, a::SMatrix{N,P}, b::SMatrix{P,M})::SMatrix{N,M} where {N,P,M}
	[reduce(r, c.(a[n,:], b[:,m])) for n=1:N, m=1:M]
end

# ╔═╡ 5322d7fa-29a0-4d2f-9edb-c2d033d63d55
outer_equals(a,b) = genmul(==, &, a, b)

# ╔═╡ baa8134b-77ef-4f65-8e88-3c04fb2d5f56
idk2(M) = reshape(M * (3:6 .== ones(Int8,1,7) * M)', (7,4,1))

# ╔═╡ 6c88b698-5f7c-4f96-af45-8377337a61f1
function solve(case)
	reshape(prod(
		permutedims(idk2(digits), (1,3,2)) .== permutedims(idk2(case), (3,1,2)),
		dims=3
	), (7,7))
end

# ╔═╡ a960b7b7-800c-4725-a62c-a28b6d427992
idk2SA(M::SMatrix{7,10})::SMatrix{7,3,Int} =
	M * (SA[3,5,6] .== ones(SMatrix{1,7,Int}) * M)'
# M * (WS ⊙ (ONES * M))'

# ╔═╡ 039f8d8e-d597-4b8f-9c0b-5882ccde4743


# ╔═╡ f6a4e46c-e6bc-457a-b494-e8df945bdb42
decode3(a::SMatrix{7,4})::SVector{4,Int} =
	outer_equals(a', digits) * (0:9)

# ╔═╡ cf848909-45fb-40c0-a3f0-be355d589101
function solve3(case::SMatrix{7,10,Bool})::SMatrix{7,7,Bool}
	outer_equals(idk2SA(digits), idk2SA(case)')
end

# ╔═╡ ec260056-c109-4a8f-9fd1-ab34dd34f8ff
function solveall(h::SMatrix{7,10},a::SMatrix{7,4})::SVector{4,Int}
	decode3(solve3(h) * a)
	#(0:9)' * (digits' ⊙ ((idk2SA(digits) ⊙ idk2SA(h)') * a))
	
	# ((a' * (
	# 	idk2SA(h)
	# 	#(h * (WS' .== (h' * ONES')))
	# 	⊙
	# 	idk2SA(digits)' #((WS .== (ONES * digits)) * digits')::SMatrix{3,7}
	# )) ⊙ digits) * (0:9)
end

# ╔═╡ 646f5d45-6fb5-4cef-bc8e-c167a75e909e
sum(evalpoly(10, reverse(solveall(h,a))) for (h,a)=casesSA)

# ╔═╡ a05331d1-2681-40be-9d59-dbff584fce6e
@benchmark sum(evalpoly(10, reverse(solveall(h,a))) for (h,a)=casesSA)

# ╔═╡ 846c8f93-97ef-4f0d-bd27-0f6feeacffcc
@benchmark evalpoly(10, solveall(casesSA[1][1], casesSA[1][2]) |> reverse)

# ╔═╡ 9ff2383d-0d8d-4777-b139-e26eb84f55af
@code_native evalpoly(10, solveall(casesSA[1][1], casesSA[1][2]) |> reverse)

# ╔═╡ 879a00a7-671a-4264-a1a6-6faef99f6368
solutions = [evalpoly(10, reverse(decode(solve(h) * a))) for (h,a)=cases]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[compat]
BenchmarkTools = "~1.2.2"
StaticArrays = "~1.2.13"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "940001114a0147b6e4d10624276d56d531dd9b49"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.2"

[[CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "ae4bbcadb2906ccc085cf52ac286dc1377dceccc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.2"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "3c76dde64d03699e074ac02eb2e8ba8254d428da"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.2.13"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═d62c44e7-9d25-4107-8db8-2095f50de850
# ╠═cbbcbb63-2f70-4c50-8d38-0e58faf47e2f
# ╠═04282206-57e3-11ec-38d8-2b60d87b8dfb
# ╠═7e352faa-8797-4393-9090-50a4737cd315
# ╠═fa163b90-9fe3-42e7-8f1a-506086259f5c
# ╠═6bfd4f9b-b2a1-44e4-9c64-9727bd7b2df5
# ╠═9c706f98-582a-42b7-a72e-887d325b871f
# ╟─675bf269-b7ec-46c4-bdb1-1ecbcb57c152
# ╠═058b79c3-8dfa-45fe-9e23-f9106a0babe3
# ╠═4aaf36e5-a114-46f0-8abe-abd45360f044
# ╠═2d1cdb44-9888-4792-9f26-4335dc3ca44f
# ╠═0c3ec32a-fc7d-405d-bcfe-0cc0d0834bfb
# ╠═e7ec7537-1cf5-4a82-b7c3-2c7f1b8fa55b
# ╠═2393e003-2144-4301-98ed-436b6f21f897
# ╠═63465ce8-650f-4414-91d7-85aefc2eb5ab
# ╠═5322d7fa-29a0-4d2f-9edb-c2d033d63d55
# ╠═496da024-e583-4fd5-92c0-89b1c4d51e3c
# ╠═eab18250-991a-4113-b1e2-710a2204f67a
# ╠═1dbc66c2-8588-4256-ae73-e82adcb00bcf
# ╠═baa8134b-77ef-4f65-8e88-3c04fb2d5f56
# ╠═6c88b698-5f7c-4f96-af45-8377337a61f1
# ╠═a960b7b7-800c-4725-a62c-a28b6d427992
# ╠═039f8d8e-d597-4b8f-9c0b-5882ccde4743
# ╠═f6a4e46c-e6bc-457a-b494-e8df945bdb42
# ╠═cf848909-45fb-40c0-a3f0-be355d589101
# ╠═ec260056-c109-4a8f-9fd1-ab34dd34f8ff
# ╠═646f5d45-6fb5-4cef-bc8e-c167a75e909e
# ╠═a05331d1-2681-40be-9d59-dbff584fce6e
# ╠═846c8f93-97ef-4f0d-bd27-0f6feeacffcc
# ╠═9ff2383d-0d8d-4777-b139-e26eb84f55af
# ╠═879a00a7-671a-4264-a1a6-6faef99f6368
# ╠═1a6a93a1-4b24-4ccf-8921-1076a62bd904
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
