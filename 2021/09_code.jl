### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ c5ca7598-e68e-4b9e-8770-5127d829d0d7
using Memoization

# ╔═╡ cf2aa832-7aae-4fe0-a42e-e466ef44c172
using OffsetArrays

# ╔═╡ f068a165-7264-479e-8d29-685029f509aa
using PlutoUI

# ╔═╡ 8cee63e8-706f-4f01-bd67-d51d44a18233
using BenchmarkTools

# ╔═╡ 9bce99de-58a9-11ec-203d-cbbbf39c1f85
const input = readlines("09_input.txt")

# ╔═╡ e98dc88e-d4d3-4fba-bb88-9bf93693b8d4
const floor = reduce(hcat, parse.(Int8, c for c=s) for s=input)

# ╔═╡ f221703c-40de-4d51-9945-4da8a85c340d
function getvalue(floor, pos)
	if 1 <= pos[1] <= size(floor)[1] && 1 <= pos[2] <= size(floor)[2]
		floor[pos]
	else
		missing
	end
end

# ╔═╡ 94f4b9db-9bba-4aad-b5a2-27bdea44db34
@benchmark [getbasin(floor, low) for low=lows]

# ╔═╡ a8818a2e-d191-449e-9c33-6282db52d3df
CI = CartesianIndex

# ╔═╡ f4cfa023-34db-4ddf-aeb3-62843f7ef39b
mask = [CI(1,0),CI(-1,0),CI(0,1),CI(0,-1)]

# ╔═╡ 13ec7cfd-d08c-4442-b711-90e087191ddf
check(floor, l) = all(floor[l] < adj for adj=skipmissing(getvalue(floor,o+l) for o=mask))

# ╔═╡ 7666eebc-d709-489d-8d2e-1971057d9228
function getbasin(floor, pos)
	s = [(getvalue(floor,pos),pos)]
	ps = Set()
	while !isempty(s)
		(LH,P) = pop!(s)
		h = getvalue(floor,P)
		if !ismissing(h) && LH <= h < 9 && P ∉ ps
			push!(ps,P)
			append!(s, [(h,P+o) for o=mask])
		end
	end
	#[p ∈ ps for p= (CI(1,1) : CI(size(floor)))]'
	length(ps)
end

# ╔═╡ 67f9b1e8-9b41-4847-8d45-ae1ad1aa1beb
lows = findall([check(floor, l) for l=CI(1,1):CI(size(floor))])

# ╔═╡ dede76d0-fc6c-42cd-a145-9b57acd8b0a0
basins = sort([getbasin(floor, low) for low=lows])

# ╔═╡ 7bb76ea3-eb3e-42f8-bc71-c08d028e10ee
prod(basins[end-2:end])

# ╔═╡ 45380102-49c9-4e0a-b3bc-f48052f6b800
sum(getindex(floor, lows) .+ 1)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
BenchmarkTools = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
Memoization = "6fafb56a-5788-4b4e-91ca-c0cea6611c73"
OffsetArrays = "6fe1bfb0-de20-5000-8ca7-80f57d26f881"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
BenchmarkTools = "~1.2.2"
Memoization = "~0.1.13"
OffsetArrays = "~1.10.8"
PlutoUI = "~0.7.21"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "abb72771fd8895a7ebd83d5632dc4b989b022b5b"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.2"

[[Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "84918055d15b3114ede17ac6a7182f68870c16f7"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.1"

[[ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "940001114a0147b6e4d10624276d56d531dd9b49"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.2.2"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[Memoization]]
deps = ["MacroTools"]
git-tree-sha1 = "42d48cac95e0fee730ba2fff477adc68c52c65f4"
uuid = "6fafb56a-5788-4b4e-91ca-c0cea6611c73"
version = "0.1.13"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[OffsetArrays]]
deps = ["Adapt"]
git-tree-sha1 = "043017e0bdeff61cfbb7afeb558ab29536bbb5ed"
uuid = "6fe1bfb0-de20-5000-8ca7-80f57d26f881"
version = "1.10.8"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "ae4bbcadb2906ccc085cf52ac286dc1377dceccc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.2"

[[Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "b68904528fd538f1cb6a3fbc44d2abdc498f9e8e"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.21"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═9bce99de-58a9-11ec-203d-cbbbf39c1f85
# ╠═e98dc88e-d4d3-4fba-bb88-9bf93693b8d4
# ╠═f221703c-40de-4d51-9945-4da8a85c340d
# ╠═f4cfa023-34db-4ddf-aeb3-62843f7ef39b
# ╠═13ec7cfd-d08c-4442-b711-90e087191ddf
# ╠═67f9b1e8-9b41-4847-8d45-ae1ad1aa1beb
# ╠═c5ca7598-e68e-4b9e-8770-5127d829d0d7
# ╠═7666eebc-d709-489d-8d2e-1971057d9228
# ╠═dede76d0-fc6c-42cd-a145-9b57acd8b0a0
# ╠═94f4b9db-9bba-4aad-b5a2-27bdea44db34
# ╠═7bb76ea3-eb3e-42f8-bc71-c08d028e10ee
# ╠═45380102-49c9-4e0a-b3bc-f48052f6b800
# ╠═a8818a2e-d191-449e-9c33-6282db52d3df
# ╠═cf2aa832-7aae-4fe0-a42e-e466ef44c172
# ╠═f068a165-7264-479e-8d29-685029f509aa
# ╠═8cee63e8-706f-4f01-bd67-d51d44a18233
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
