### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ 57cb5d51-56db-4995-b2ca-5284ce76e713
using Graphs

# ╔═╡ 0830d57a-f2e0-428c-9f65-87a5e7b495a7
using PlutoUI

# ╔═╡ 7379566e-eecf-4564-b835-b8a63ec2c36e
const input = readlines("12_input_example.txt")

# ╔═╡ af06f164-24e1-49c0-9598-045b24d7ed7d
function parse_line(l)
	a,b = split(l, "-")
	a,b
end

# ╔═╡ cd3f913b-5645-4222-a674-63d3a64441ca
const inputs = parse_line.(input)

# ╔═╡ b4af081c-7f06-405b-a587-64e8b36441d3
function get_or_intern!(v, s)
	if s ∈ v
		findfirst(==(s), v)
	else
		push!(v, s)
		length(v)
	end
end

# ╔═╡ 8dbc8bd2-21cb-4b03-ab12-e333cbf02c6f
function construct_graph(ins)
	rooms = ["start","end"]
	edges = [Edge(get_or_intern!(rooms, a), get_or_intern!(rooms, b)) for (a,b)=ins]
	SimpleGraph(edges), rooms
end

# ╔═╡ cc08d566-303e-48bb-a322-3c623186cd8a
graph, rooms = construct_graph(inputs)

# ╔═╡ 58413ed2-bae5-4d55-9c28-4860fb3b91fd
big_rooms = [uppercase(s) == s for s=rooms]

# ╔═╡ 35b24108-eec9-42dd-ab41-7d345085d5ab
room_count = length(rooms)

# ╔═╡ 6c3e41ef-f3c5-4684-ab7f-34c487cc14a8
start_i = findfirst(==("start"), rooms)

# ╔═╡ 40c0c0b5-24c5-4768-948d-e44a8501d536
end_i = findfirst(==("end"), rooms)

# ╔═╡ 45b7676c-bb56-4570-89a2-f0872c0fd3e9
adjmat = adjacency_matrix(graph)

# ╔═╡ d46e1b3a-d247-45b2-b982-5fd4a46e0b4a
big_adjmat = adjmat .* (big_rooms .| big_rooms')

# ╔═╡ 37674884-401a-477b-b393-c370a6fd96b5
# function find_paths(i::Int,s::Vector{Int})
# 	connected_rooms = findall(Bool.(adjmat[i,:]))
	
# 	# println(s)
# 	# println("c: ", filter((r -> big_rooms[r] || (r ∉ s)), connected_rooms))
# 	# println("e: ", (end_i ∈ connected_rooms))
	
# 	(end_i ∈ connected_rooms) + sum(
# 		I -> find_paths(I, vcat(s,[i])),
# 		filter((r -> (r != end_i) && (big_rooms[r] || (r ∉ s))), connected_rooms),
# 		init=0
# 	)
# end

# ╔═╡ 57ac80da-5b92-4419-bcb2-00d72b6a01ce
max_visits = begin
	m = repeat([1], room_count)
	m[start_i] = 0
	m[end_i] = 0
	m[findall(big_rooms)] .= 1000000000
	m
end

# ╔═╡ be53b351-0067-432a-b4c8-5768760fd031
function find_paths(i::Int, vc::Vector{Int}, path, doubled)
	connected_rooms = findall(Bool.(adjmat[i,:]))
	
	# println(vc)
	# println("c: ", filter(r -> (max_visits[r] > vc[r]), connected_rooms))
	# println("e: ", (end_i ∈ connected_rooms))
	# if (end_i ∈ connected_rooms)
	# 	println(join(rooms[[path...,end_i]], ","))
	# end
	
	(end_i ∈ connected_rooms) + sum(
		I -> begin
			vcI = copy(vc)
			vcI[I] += 1
			find_paths(I, vcI, [path..., I],
				doubled || ((!big_rooms[I]) && (vcI[I] > 1))
			)
		end,
		filter((r -> (max_visits[r] > vc[r]) || (
			(r != start_i) && (r != end_i) && !doubled
		)), connected_rooms),
		init=0
	)
end

# ╔═╡ bf785b74-24b1-4edc-96cf-f56277e51074
with_terminal() do
	find_paths(start_i, zeros(Int, room_count), [start_i], false)
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
Graphs = "~1.4.1"
PlutoUI = "~0.7.22"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "abb72771fd8895a7ebd83d5632dc4b989b022b5b"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.2"

[[ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "62e51b39331de8911e4a7ff6f5aaf38a5f4cc0ae"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.2.0"

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "dce3e3fea680869eaa0b774b2e8343e9ff442313"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.40.0"

[[DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[Graphs]]
deps = ["ArnoldiMethod", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "92243c07e786ea3458532e199eb3feee0e7e08eb"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.4.1"

[[Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[Inflate]]
git-tree-sha1 = "f5fc07d4e706b84f72d54eedcc1c13d92fb0871c"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.2"

[[InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "ae4bbcadb2906ccc085cf52ac286dc1377dceccc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.1.2"

[[Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "565564f615ba8c4e4f40f5d29784aa50a8f7bbaf"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.22"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "3c76dde64d03699e074ac02eb2e8ba8254d428da"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.2.13"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═57cb5d51-56db-4995-b2ca-5284ce76e713
# ╠═7379566e-eecf-4564-b835-b8a63ec2c36e
# ╠═af06f164-24e1-49c0-9598-045b24d7ed7d
# ╠═cd3f913b-5645-4222-a674-63d3a64441ca
# ╠═b4af081c-7f06-405b-a587-64e8b36441d3
# ╠═8dbc8bd2-21cb-4b03-ab12-e333cbf02c6f
# ╠═cc08d566-303e-48bb-a322-3c623186cd8a
# ╠═58413ed2-bae5-4d55-9c28-4860fb3b91fd
# ╠═35b24108-eec9-42dd-ab41-7d345085d5ab
# ╠═6c3e41ef-f3c5-4684-ab7f-34c487cc14a8
# ╠═40c0c0b5-24c5-4768-948d-e44a8501d536
# ╠═45b7676c-bb56-4570-89a2-f0872c0fd3e9
# ╠═d46e1b3a-d247-45b2-b982-5fd4a46e0b4a
# ╠═37674884-401a-477b-b393-c370a6fd96b5
# ╠═57ac80da-5b92-4419-bcb2-00d72b6a01ce
# ╠═be53b351-0067-432a-b4c8-5768760fd031
# ╠═bf785b74-24b1-4edc-96cf-f56277e51074
# ╠═0830d57a-f2e0-428c-9f65-87a5e7b495a7
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
