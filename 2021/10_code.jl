### A Pluto.jl notebook ###
# v0.17.2

using Markdown
using InteractiveUtils

# ╔═╡ 1ef68fe4-f50b-4228-8080-975d44eff686
using Statistics: median

# ╔═╡ 06044dc0-5973-11ec-131b-0f858348af0b
const input = readlines("10_input.txt")

# ╔═╡ 7be0bfd7-710f-4fb7-b868-028462fe4fd3
c_to_n(c::Char) = 5 - findfirst.(==(c), "<{[( )]}>")

# ╔═╡ 366878b6-e759-4e2b-a11b-b9ef631926f1
parse_line(line::String) = c_to_n.(collect(line))

# ╔═╡ 7c321fda-2671-447c-aca3-22cc453aba31
const lines = parse_line.(input)

# ╔═╡ 5ea09131-991c-4162-9373-7554fa551136
function check_line(line)
	os = []
	for c=line
		if c>0
			push!(os, c)
		elseif c != -pop!(os)
			return (c, os)
		end
	end
	return (0, os)
end

# ╔═╡ 49307c33-ec50-4e57-b276-b43d8a8163a1
score1((n,s)) = n == 0 ? 0 : [3,57,1197,25137][-n]

# ╔═╡ 6bd9e880-93e2-4d5c-b3e3-dd6aae5d1aa3
part1 = sum(score1.(check_line.(lines)))

# ╔═╡ f5f1cfdb-29a0-4fe8-8a5f-21422968c831
const incomplete_lines =
	map(x -> x[2], filter(l -> l[1] == 0, map(check_line, lines)))

# ╔═╡ 6b142b25-5316-4720-a057-e7b52a7e0280
score2(x) = foldr((a,b) -> 5b+a, x, init=0)

# ╔═╡ 97d27f2d-ea8e-4c4f-a220-3819ed54b888
part2 = Int(median(score2.(incomplete_lines)))

# ╔═╡ cfe262da-28d3-4a2b-b4dc-b98bdff655ee
println(part1,part2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Statistics = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
"""

# ╔═╡ Cell order:
# ╠═1ef68fe4-f50b-4228-8080-975d44eff686
# ╠═06044dc0-5973-11ec-131b-0f858348af0b
# ╠═7be0bfd7-710f-4fb7-b868-028462fe4fd3
# ╠═366878b6-e759-4e2b-a11b-b9ef631926f1
# ╠═7c321fda-2671-447c-aca3-22cc453aba31
# ╠═5ea09131-991c-4162-9373-7554fa551136
# ╠═49307c33-ec50-4e57-b276-b43d8a8163a1
# ╠═6bd9e880-93e2-4d5c-b3e3-dd6aae5d1aa3
# ╠═f5f1cfdb-29a0-4fe8-8a5f-21422968c831
# ╠═6b142b25-5316-4720-a057-e7b52a7e0280
# ╠═97d27f2d-ea8e-4c4f-a220-3819ed54b888
# ╠═cfe262da-28d3-4a2b-b4dc-b98bdff655ee
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
