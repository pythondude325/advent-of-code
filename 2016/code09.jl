### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ 9b36ace6-5487-11ed-3902-6305d6e5ac6d
const input = readlines("input09.txt")[1]

# ╔═╡ dc7b1e11-973d-49b6-86d7-6851b4f49a24
function decompress(str)
	s = Vector{UInt8}(str)
	output = UInt8[]

	i = 1
	while i <= length(s)
		if s[i] == 0x28
			ix = findnext(==(0x78), s, i)
			ic = findnext(==(0x29), s, ix)
			l = parse(Int, String(s[i+1:ix-1]))
			r = parse(Int, String(s[ix+1:ic-1]))
			println("l=$l r=$r")
			for _=1:r
				# println("adding $(@view s[ic+1:ic+l])")
				append!(output, @view s[ic+1:ic+l])
			end
			i = ic + l + 1
		else
			push!(output, s[i])
			i += 1
		end
	end

	String(output)
end

# ╔═╡ 0d70e122-b201-4887-a0a9-4b5fe33995dc
decompress.([
	"ADVENT",
	"A(1x5)BC",
	"(3x3)XYZ",
	"A(2x2)BCD(2x2)EFG",
	"(6x1)(1x3)A",
	"X(8x2)(3x3)ABCY",
])

# ╔═╡ 0f3ee41e-14d9-4b36-b14d-7aef44de1b16
const part1_file = decompress(input)

# ╔═╡ de223370-109a-4678-8128-77e10954043e
const part1 = length(part1_file)

# ╔═╡ 145d234b-b5fe-469d-96c4-bb79236b7c23
summary("hello"[2:end])

# ╔═╡ 68623368-abdd-448f-9bbd-ec549f9cf6d1
findnext('x', "hexed", 2)

# ╔═╡ 2ec38f65-531e-4315-8898-d2b7000aafb7
function part2_decompressed_length(str)
	i = 1
	len = 0
	while i <= length(str)
		if str[i] == '('
			ix = findnext('x', str, i)
			ic = findnext(')', str, ix)
			l = parse(Int, str[i+1:ix-1])
			r = parse(Int, str[ix+1:ic-1])
			len += r * part2_decompressed_length(str[ic+1:ic+l])
			i = ic + l + 1
		else
			len += 1
			i += 1
		end
	end
	len
end

# ╔═╡ 953c16f3-dc7b-4923-a6f2-be743d29efa6
part2_decompressed_length.([
	"(3x3)XYZ",
	"X(8x2)(3x3)ABCY",
	"(27x12)(20x12)(13x14)(7x10)(1x12)A",
	"(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN",
])

# ╔═╡ eb0ccbd4-14bf-4e70-8d01-899d6845231d
const part2 = part2_decompressed_length(input)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.2"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═9b36ace6-5487-11ed-3902-6305d6e5ac6d
# ╠═dc7b1e11-973d-49b6-86d7-6851b4f49a24
# ╠═0d70e122-b201-4887-a0a9-4b5fe33995dc
# ╠═0f3ee41e-14d9-4b36-b14d-7aef44de1b16
# ╠═de223370-109a-4678-8128-77e10954043e
# ╠═145d234b-b5fe-469d-96c4-bb79236b7c23
# ╠═68623368-abdd-448f-9bbd-ec549f9cf6d1
# ╠═2ec38f65-531e-4315-8898-d2b7000aafb7
# ╠═953c16f3-dc7b-4923-a6f2-be743d29efa6
# ╠═eb0ccbd4-14bf-4e70-8d01-899d6845231d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
