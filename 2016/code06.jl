### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ f33a4454-2890-11ed-23da-a18bf153db81
const input = readlines("input06.txt")

# ╔═╡ 13c01924-fc75-476c-8070-38cffdd814bb
const messages = reduce(hcat, collect.(input))

# ╔═╡ 40744f11-27a2-4d5b-9433-c459c623d5ce
const part1 = [
	maximum(((c,n),) -> (n,c), foldl(r, init=Dict{Char,Int}()) do d,c
		get!(d, c, 0)
		d[c] += 1
		d
	end) |> last for r=eachrow(messages)
] |> String

# ╔═╡ ff9a14f0-bbf1-4d6e-beff-70934eaef527
const part2 = [
	minimum(((c,n),) -> (n,c), foldl(r, init=Dict{Char,Int}()) do d,c
		get!(d, c, 0)
		d[c] += 1
		d
	end) |> last for r=eachrow(messages)
] |> String

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═f33a4454-2890-11ed-23da-a18bf153db81
# ╠═13c01924-fc75-476c-8070-38cffdd814bb
# ╠═40744f11-27a2-4d5b-9433-c459c623d5ce
# ╠═ff9a14f0-bbf1-4d6e-beff-70934eaef527
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
