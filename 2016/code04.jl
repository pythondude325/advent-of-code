### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ c18f21cc-2881-11ed-2ffe-c79c9dad09e0
const input = readlines("input04.txt")

# ╔═╡ 807136f5-4b59-4002-9565-217d0b1ee87d
const test_input = [
	"aaaaa-bbb-z-y-x-123[abxyz]",
	"a-b-c-d-e-f-g-h-987[abcde]",
	"not-a-real-room-404[oarel]",
	"totally-real-room-200[decoy]",
]

# ╔═╡ 96b449ae-6f98-43d0-a281-a1688c5bdd00
struct Room
	name::String
	sectorid::Int
	checksum::Vector{Char}
end

# ╔═╡ cdbe9027-711b-4a49-98b4-dd16b1c50318
function Base.parse(::Type{Room}, s)::Room
	m = match(r"^([a-z\-]+)-(\d+)\[([a-z]{5})\]$", s)
	Room(
		m[1],
		parse(Int, m[2]),
		collect(m[3]),
	)
end

# ╔═╡ e07263ed-678d-48f1-aed4-dcc78abed9d8
const rooms = parse.(Room, input)

# ╔═╡ 2e423cb0-8f1a-47e1-8a9e-9b8797407606
function check_room(room::Room)::Bool
	checksum = sort('a':'z', by=c -> (-count(==(c), room.name), c))
	checksum[1:5] == room.checksum
end

# ╔═╡ 1048c65b-03dd-4027-bf3b-a2d6014516af
const valid_rooms = filter(check_room, rooms)

# ╔═╡ c28c5984-33cd-4793-95e5-c46eaf78d93e
const part1 = sum(room.sectorid for room=valid_rooms)

# ╔═╡ 17d9d6e3-fd55-4de6-93b0-fce701fac827
'z'-'`'

# ╔═╡ ab42120d-f910-40bd-bcb6-77ff1b400893
function decrypt_room_name(room::Room)
	cipher = 'a':'z' .=> circshift('a':'z', -room.sectorid)
	replace(room.name, '-' => ' ', cipher...)
end

# ╔═╡ 4ff90d0e-81e8-4a3e-8917-8502bdab877e
const room_names = decrypt_room_name.(valid_rooms)

# ╔═╡ 488cdf37-f63f-4bb6-98dd-fec811f815f0
const part2 = first(filter(room -> occursin("northpole", decrypt_room_name(room)), valid_rooms)).sectorid

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═c18f21cc-2881-11ed-2ffe-c79c9dad09e0
# ╠═807136f5-4b59-4002-9565-217d0b1ee87d
# ╠═96b449ae-6f98-43d0-a281-a1688c5bdd00
# ╠═cdbe9027-711b-4a49-98b4-dd16b1c50318
# ╠═e07263ed-678d-48f1-aed4-dcc78abed9d8
# ╠═2e423cb0-8f1a-47e1-8a9e-9b8797407606
# ╠═1048c65b-03dd-4027-bf3b-a2d6014516af
# ╠═c28c5984-33cd-4793-95e5-c46eaf78d93e
# ╠═17d9d6e3-fd55-4de6-93b0-fce701fac827
# ╠═ab42120d-f910-40bd-bcb6-77ff1b400893
# ╠═4ff90d0e-81e8-4a3e-8917-8502bdab877e
# ╠═488cdf37-f63f-4bb6-98dd-fec811f815f0
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
