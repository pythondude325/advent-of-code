### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 3054a705-c358-4839-92f1-4d494f91fdc7
using MD5

# ╔═╡ a87c0059-984b-4881-86f8-12ac3056c318
using Pipe

# ╔═╡ 5e2bbe22-288d-11ed-19d7-c9993e9982a2
const input = "wtnhxymk"

# ╔═╡ ca07b6b0-8ff4-4f74-8d1e-14c64216c41f
const hexadecimal_alphabet = ['0':'9';'a':'f']

# ╔═╡ 94515801-8cbd-49da-8de0-55d98bc5b9f5
function find_password(s)
	@pipe Iterators.countfrom(0) |>
		Iterators.map(i -> md5(s*string(i)), _) |>
		Iterators.filter(d -> d[1] == 0 && d[2] == 0 && d[3] < 0x10, _) |>
		Iterators.map(d -> hexadecimal_alphabet[d[3]+1], _) |>
		Iterators.take(_, 8) |>
		collect |>
		String
end

# ╔═╡ 38f7d82c-303b-4bee-af3a-97238cc22c62
const part1 = find_password(input)

# ╔═╡ b8dc46ed-3e07-4ae9-b2f6-5a17054b9252
function find_password_part2(s)
	password = fill('_', 8)
	char_iter = @pipe Iterators.countfrom(0) |>
		Iterators.map(i -> md5(s*string(i)), _) |>
		Iterators.filter(d -> d[1] == 0 && d[2] == 0 && d[3] < 0x08, _) |>
		Iterators.map(d -> (d[3], hexadecimal_alphabet[d[4]÷0x10+1]), _)
	for (pos,char)=char_iter
		if password[pos+1] == '_'
			password[pos+1] = char
		end
		if(all(!=('_'), password))
			break
		end
	end
	String(password)
end

# ╔═╡ ec9fd01b-004f-464d-80ce-e6ef5b6b4312
const part2 = find_password_part2(input)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
MD5 = "6ac74813-4b46-53a4-afec-0b5dc9d7885c"
Pipe = "b98c9c47-44ae-5843-9183-064241ee97a0"

[compat]
MD5 = "~0.2.1"
Pipe = "~1.3.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "95c89362248694cafb9848dbe0d451dd7c49aa38"

[[deps.MD5]]
deps = ["Random", "SHA"]
git-tree-sha1 = "eeffe42284464c35a08026d23aa948421acf8923"
uuid = "6ac74813-4b46-53a4-afec-0b5dc9d7885c"
version = "0.2.1"

[[deps.Pipe]]
git-tree-sha1 = "6842804e7867b115ca9de748a0cf6b364523c16d"
uuid = "b98c9c47-44ae-5843-9183-064241ee97a0"
version = "1.3.0"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"
"""

# ╔═╡ Cell order:
# ╠═5e2bbe22-288d-11ed-19d7-c9993e9982a2
# ╠═3054a705-c358-4839-92f1-4d494f91fdc7
# ╠═a87c0059-984b-4881-86f8-12ac3056c318
# ╠═ca07b6b0-8ff4-4f74-8d1e-14c64216c41f
# ╠═94515801-8cbd-49da-8de0-55d98bc5b9f5
# ╠═38f7d82c-303b-4bee-af3a-97238cc22c62
# ╠═b8dc46ed-3e07-4ae9-b2f6-5a17054b9252
# ╠═ec9fd01b-004f-464d-80ce-e6ef5b6b4312
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
