### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 7433aaa2-2812-11ed-16f2-19163ed57a5f
const input = split(read("input01.txt", String), ", ")

# ╔═╡ 046b59be-319b-4944-a53a-3ea6330b265c
parse_directions(s)::Tuple{Complex{Int}, Int} = (s[1]=='R' ? -im : im), parse(Int, s[2:end])

# ╔═╡ 3bc9473d-68b6-4a9b-9814-f0e291e6a40e
const directions = parse_directions.(input)

# ╔═╡ 195c3381-cbcb-47bd-af51-4302f1785310
follow_path(directions) = accumulate(((p,d), (t,a)) -> (p + d * t * a, d * t), directions, init=(0+0im, 1im))

# ╔═╡ d01a0fd8-c1c8-4bd9-9b61-4798b44d0df8
const path = follow_path(directions)

# ╔═╡ d0ad0a58-ebc5-4b93-b8a9-e10159da251c
const places = first.(path)

# ╔═╡ 25c422b8-ea44-475b-a94d-f5318611bca1
const destination = places[end]

# ╔═╡ 61167b51-80e6-4025-a21e-d699db21300a
manhattan_distance(z) = abs(real(z)) + abs(imag(z))

# ╔═╡ ac4c9954-4960-479e-bf72-1c5e0418ff14
const part1 = manhattan_distance(destination)

# ╔═╡ 90c70f59-6ced-4a1e-b266-10b549fe7d5f
function first_duplicate(a)
	a[findfirst([p ∈ a[i+1:end] for (i,p)=enumerate(a)])]
end

# ╔═╡ e4a83b22-1f0b-4427-8e3b-1f9cf442d1b4
test_directions = parse_directions.(split("R8, R4, R4, R8", ", "))

# ╔═╡ a07e47a4-0256-4536-bd81-26e2d7d4d0fb
function follow_path_part2(directions)
	path = Complex{Int}[]
	p = 0+0im
	d = 1im
	for (turn,distance)=directions
		d *= turn
		append!(path, p .+ d .* (1:distance))
		p += d * distance
	end
	path
end

# ╔═╡ 1450f843-df93-4c5a-8d12-e5b8c5e74506
const destination_part2 = first_duplicate(follow_path_part2(directions))

# ╔═╡ 4eb70e77-02b5-43dd-9099-85d35670b6bf
const part2 = manhattan_distance(destination_part2)

# ╔═╡ 872d6bc2-ae7b-43fb-8488-b38afe5cd1be
follow_path_part2(test_directions)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═7433aaa2-2812-11ed-16f2-19163ed57a5f
# ╠═046b59be-319b-4944-a53a-3ea6330b265c
# ╠═3bc9473d-68b6-4a9b-9814-f0e291e6a40e
# ╠═195c3381-cbcb-47bd-af51-4302f1785310
# ╠═d01a0fd8-c1c8-4bd9-9b61-4798b44d0df8
# ╠═d0ad0a58-ebc5-4b93-b8a9-e10159da251c
# ╠═25c422b8-ea44-475b-a94d-f5318611bca1
# ╠═61167b51-80e6-4025-a21e-d699db21300a
# ╠═ac4c9954-4960-479e-bf72-1c5e0418ff14
# ╠═90c70f59-6ced-4a1e-b266-10b549fe7d5f
# ╠═1450f843-df93-4c5a-8d12-e5b8c5e74506
# ╠═4eb70e77-02b5-43dd-9099-85d35670b6bf
# ╠═e4a83b22-1f0b-4427-8e3b-1f9cf442d1b4
# ╠═a07e47a4-0256-4536-bd81-26e2d7d4d0fb
# ╠═872d6bc2-ae7b-43fb-8488-b38afe5cd1be
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
