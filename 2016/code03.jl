### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ fd4d63ec-287f-11ed-0227-0b4536bd645a
const input = readlines("input03.txt")

# ╔═╡ aabc3fdb-9784-4b70-8df3-d1adf169c697
const triangles = [parse.(Int, split(l)) for l=input]

# ╔═╡ 09371f34-ab96-41c1-869a-0d82979ccaf2
function check_triangle(t)
	t = sort(t)
	t[1] + t[2] > t[3]
end

# ╔═╡ 15b44977-3feb-4aab-8200-7ac91049f042
const part1 = count(check_triangle, triangles)

# ╔═╡ e9ff84c5-b19d-450c-bbff-1985bc166ea8
const triangle_grid = reduce(hcat, triangles)

# ╔═╡ 5650ad54-1b2f-4223-9132-2c5fee3178b2
const new_triangles = [triangle_grid[a,b:b+2] for a=1:3,b=1:3:size(triangle_grid,2)]

# ╔═╡ 8a97c40a-d00b-4022-a4f4-7e709167d85f
const part2 = count(check_triangle, new_triangles)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═fd4d63ec-287f-11ed-0227-0b4536bd645a
# ╠═aabc3fdb-9784-4b70-8df3-d1adf169c697
# ╠═09371f34-ab96-41c1-869a-0d82979ccaf2
# ╠═15b44977-3feb-4aab-8200-7ac91049f042
# ╠═e9ff84c5-b19d-450c-bbff-1985bc166ea8
# ╠═5650ad54-1b2f-4223-9132-2c5fee3178b2
# ╠═8a97c40a-d00b-4022-a4f4-7e709167d85f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
