### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ dadfd268-281b-11ed-30a9-33fc88b293cb
const input = readlines("input02.txt")

# ╔═╡ a66f1aa9-f502-491d-925c-e57ae7974cf5
const test_input = [
	"ULL",
	"RRDDD",
	"LURDL",
	"UUUUD",
]

# ╔═╡ aee8f334-4c6b-4492-99ab-5e003ba0c544
const number_pad = [
	1 2 3
	4 5 6
	7 8 9
]

# ╔═╡ 47a9d83e-3af8-49b1-811c-bb1d9c25bb3f
function parse_direction(c)
	if c == 'U'
		[-1,0]
	elseif c == 'D'
		[1,0]
	elseif c == 'L'
		[0,-1]
	elseif c == 'R'
		[0,1]
	end
end

# ╔═╡ e151553a-f782-460b-86da-4ac05065ac57
const directions = [
	[parse_direction(c) for c=s]
	for s=input
]

# ╔═╡ eb0269be-b35e-4828-939e-f7fd714728cc
function follow_directions(start, directions)
	foldl(
		(p,d) -> clamp.(p + d, [1,1], [3,3]),
		directions,
		init=start
	)
end

# ╔═╡ 819f0b53-97ba-4c24-87e8-4efb54d35eed
const number_positions = accumulate(follow_directions, directions, init=[2,2])

# ╔═╡ 03ea45e8-600d-4377-b3a4-0eb8d6743e9c
const part1 = map((v) -> number_pad[v[1],v[2]], number_positions)

# ╔═╡ a82f0f7d-a583-4636-b34c-ae5d66e23a7c
const new_keypad = [
0 0 1 0 0
0 2 3 4 0
5 6 7 8 9
0 0xA 0xB 0xC 0
0 0 0xD 0 0
]

# ╔═╡ 8079de99-b9fa-4a64-b9c2-5a4fee71891c
function follow_directions_part2(start, directions)
	foldl(
		function (p,d)
			new_pos = clamp.(p + d, [1,1], [5,5])
			if new_keypad[new_pos[1], new_pos[2]] != 0
				new_pos
			else
				p
			end
		end,
		directions,
		init=start
	)
end

# ╔═╡ b3b279b9-6019-465b-a4ac-23af7c999b8f
const new_number_positions = accumulate(follow_directions_part2, directions, init=[3,1])

# ╔═╡ 60a1a581-fc6d-4037-b2ac-180620ba1183
const part2 = map((v) -> UInt8(new_keypad[v[1], v[2]]), new_number_positions)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═dadfd268-281b-11ed-30a9-33fc88b293cb
# ╠═a66f1aa9-f502-491d-925c-e57ae7974cf5
# ╠═aee8f334-4c6b-4492-99ab-5e003ba0c544
# ╠═47a9d83e-3af8-49b1-811c-bb1d9c25bb3f
# ╠═e151553a-f782-460b-86da-4ac05065ac57
# ╠═eb0269be-b35e-4828-939e-f7fd714728cc
# ╠═819f0b53-97ba-4c24-87e8-4efb54d35eed
# ╠═03ea45e8-600d-4377-b3a4-0eb8d6743e9c
# ╠═a82f0f7d-a583-4636-b34c-ae5d66e23a7c
# ╠═8079de99-b9fa-4a64-b9c2-5a4fee71891c
# ╠═b3b279b9-6019-465b-a4ac-23af7c999b8f
# ╠═60a1a581-fc6d-4037-b2ac-180620ba1183
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
