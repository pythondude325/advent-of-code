### A Pluto.jl notebook ###
# v0.19.12

using Markdown
using InteractiveUtils

# ╔═╡ c479f46f-fb2c-4bc1-83f5-620639dc0a5e
const input = readlines("input07.txt")

# ╔═╡ 72a232f3-5231-4265-9ab5-7a818efc2914
input[1]

# ╔═╡ 2fb8e5c4-3ef9-48ed-99ef-6fad27893db6
contains_abba(s) = occursin(r"([a-z])(?!\1)([a-z])\2\1", s)

# ╔═╡ 92ce7eb5-30c4-4392-9e1e-7478b2cb03ca
contains_abba.([
	"abba",
	"aaaa",
])

# ╔═╡ 6d862027-2529-41d9-beff-1dd2866f3368
hypernets(s) = [m[1] for m=eachmatch(r"\[([a-z]*)\]", s)]

# ╔═╡ 74f082cd-d932-448e-abfa-3d5670e699ed


# ╔═╡ 172ca52c-77d5-44fe-af65-7920a4795b70
can_use_tls(s) = contains_abba(s) && !any(contains_abba, hypernets(s))

# ╔═╡ fc13c0ef-7b85-4fe4-a92a-a153fd0a20e6
can_use_tls(input[6])

# ╔═╡ a9810cb8-93c3-49b1-9ec5-a55b468fbf26
can_use_tls("abba[mnop]qrst") # true

# ╔═╡ 0ba6e57f-1cc6-4358-9448-176f47d10f33
can_use_tls("abcd[bddb]xyyx") # false

# ╔═╡ 34fc7d7d-d59b-4948-8b51-915da7582752
can_use_tls("aaaa[qwer]tyui") # false

# ╔═╡ 90c410eb-ed30-4244-b775-3f3448e337ea
can_use_tls("ioxxoj[asdfgh]zxcvbn") #true

# ╔═╡ 657afdb6-5352-4b86-adb2-cd0a20387596
const part1 = count(can_use_tls, input)

# ╔═╡ 133635a1-7985-4a40-b679-2d667cd0aa02
function parse_address(s)
	parts = split(s, r"[\[\]]")
	parts[1:2:end], parts[2:2:end]
end

# ╔═╡ f28fc2f9-934e-4b1c-b967-bf293a30c606
parse_address("abba[mnop]qrst")

# ╔═╡ e28a5649-94c3-4dd1-8913-7095e88503b9
function can_use_ssl(a)
	s,h = parse_address(a)
	abas = reduce(vcat, collect(eachmatch(r"([a-z])(?!\1)([a-z])\1", S, overlap=true)) for S=s)
	any(any(contains("$(aba[2])$(aba[1])$(aba[2])"), h) for aba=abas)
end

# ╔═╡ f08ef33b-834a-41d1-9e69-4348a6b51c1c
can_use_ssl.([
	"aba[bab]xyz",
	"xyx[xyx]xyx",
	"aaa[kek]eke",
	"zazbz[bzb]cdb",
])

# ╔═╡ e86f9bc3-4da9-4a61-be53-00d5f368beed
const part2 = count(can_use_ssl, input)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.2"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═c479f46f-fb2c-4bc1-83f5-620639dc0a5e
# ╠═72a232f3-5231-4265-9ab5-7a818efc2914
# ╠═2fb8e5c4-3ef9-48ed-99ef-6fad27893db6
# ╠═92ce7eb5-30c4-4392-9e1e-7478b2cb03ca
# ╠═6d862027-2529-41d9-beff-1dd2866f3368
# ╠═74f082cd-d932-448e-abfa-3d5670e699ed
# ╠═172ca52c-77d5-44fe-af65-7920a4795b70
# ╠═fc13c0ef-7b85-4fe4-a92a-a153fd0a20e6
# ╠═a9810cb8-93c3-49b1-9ec5-a55b468fbf26
# ╠═0ba6e57f-1cc6-4358-9448-176f47d10f33
# ╠═34fc7d7d-d59b-4948-8b51-915da7582752
# ╠═90c410eb-ed30-4244-b775-3f3448e337ea
# ╠═657afdb6-5352-4b86-adb2-cd0a20387596
# ╠═133635a1-7985-4a40-b679-2d667cd0aa02
# ╠═f28fc2f9-934e-4b1c-b967-bf293a30c606
# ╠═e28a5649-94c3-4dd1-8913-7095e88503b9
# ╠═f08ef33b-834a-41d1-9e69-4348a6b51c1c
# ╠═e86f9bc3-4da9-4a61-be53-00d5f368beed
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
