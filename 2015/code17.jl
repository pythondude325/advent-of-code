### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 6e6ef070-264d-11ed-0fe2-11cf54bc0913
const input = readlines("input17.txt")

# ╔═╡ abe0789a-33ee-4068-a78f-16aba86df8d7
const containers = sort!(parse.(Int, input), rev=true)

# ╔═╡ 25acea03-6030-4f4c-8811-0d290df5fbcb
function count_combinations(containers, eggnog; used=[])
	if length(containers) == 0
		[]
	else
		combinations = count_combinations(@view(containers[2:end]), eggnog, used=used)
		if containers[1] == eggnog
			push!(combinations, [used; containers[1]])
		elseif containers[1] < eggnog
			append!(
				combinations,
				count_combinations(
					@view(containers[2:end]),
					eggnog - containers[1],
					used=[used; containers[1]]
				)
			)
		end
		combinations
	end
end

# ╔═╡ e45513cf-ed28-417a-9504-0ee413ee46bc
const combinations = count_combinations(containers, 150)

# ╔═╡ cb70c10c-8eba-4ed9-b304-a00697a11146
const part1 = length(combinations)

# ╔═╡ 2743c50a-d5ad-4c08-a4af-2edc095f8cd9
const min_length = minimum(length.(combinations))

# ╔═╡ c854bb46-849d-4090-9a5d-560d69d78f4d
const part2 = count(c -> length(c) == min_length, combinations)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═6e6ef070-264d-11ed-0fe2-11cf54bc0913
# ╠═abe0789a-33ee-4068-a78f-16aba86df8d7
# ╠═25acea03-6030-4f4c-8811-0d290df5fbcb
# ╠═e45513cf-ed28-417a-9504-0ee413ee46bc
# ╠═cb70c10c-8eba-4ed9-b304-a00697a11146
# ╠═2743c50a-d5ad-4c08-a4af-2edc095f8cd9
# ╠═c854bb46-849d-4090-9a5d-560d69d78f4d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
