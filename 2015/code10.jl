### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 6b0fe534-261f-11ed-0ced-259a838438b5
const input = 1113222113

# ╔═╡ 19c49da3-82a8-4ea4-89cd-bfec252b5045
const input_digits = reverse(digits(input))

# ╔═╡ bfe151a2-1a3c-4943-95b9-ba76be2092d3
function look_and_say(ds)
	ds = [-1; ds; -1]
	bounds = ds[1:end-1] .!= ds[2:end]
	boundary_index = findall(bounds)

	v = Vector{Int}()
	for (c, d) = zip(diff(boundary_index), ds[boundary_index[1:end-1] .+ 1])
		append!(v, reverse(digits(c)))
		push!(v, d)
	end
	v
end

# ╔═╡ c3b0a3d2-c730-416f-b778-c31ae103aac9
function funcpow(f, p)
	foldl(∘, fill(f, p))
end

# ╔═╡ 076a28a1-71d6-4e2e-922c-db95b2f20451
const part1 = input_digits |> funcpow(look_and_say, 40) |> length

# ╔═╡ 90030649-8681-4b07-83f1-478263a902fa
const part2 = input_digits |> funcpow(look_and_say, 50) |> length

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═6b0fe534-261f-11ed-0ced-259a838438b5
# ╠═19c49da3-82a8-4ea4-89cd-bfec252b5045
# ╠═bfe151a2-1a3c-4943-95b9-ba76be2092d3
# ╠═c3b0a3d2-c730-416f-b778-c31ae103aac9
# ╠═076a28a1-71d6-4e2e-922c-db95b2f20451
# ╠═90030649-8681-4b07-83f1-478263a902fa
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
