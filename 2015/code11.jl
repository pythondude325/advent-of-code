### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ bda35764-2623-11ed-37ae-6d3329e033ad
const input = "vzbxkghb"

# ╔═╡ 474227a4-b1f7-49a7-855e-90ad5591e036
letter(n) = n + 'a'

# ╔═╡ f9a988a1-c6d2-449a-9592-8a767f1e7af1
unletter(c) = c - 'a'

# ╔═╡ fec3ea6a-6829-4d44-a294-5bc75768b4a2
decode_password(s) = unletter.(c for c=s)

# ╔═╡ 56e0c543-835f-4aa6-8aed-403d747709ef
encode_password(p) = String(letter.(p))

# ╔═╡ b58d216b-520c-48c0-8817-93d76179fba2
const input_password = decode_password(input)

# ╔═╡ b88f237b-11d1-42e2-a277-5cb7acd74464
const banned_letters = (unletter('i'), unletter('l'), unletter('o'))

# ╔═╡ ff0aea06-dd7b-43ca-a318-ca52926e3633
function test_password(p)
	all(l ∉ banned_letters for l in p) && begin
		dp = diff(p)
		pairs = findall(==(0), dp)
		length(pairs) >= 2 &&
		(pairs[end] - pairs[begin]) >= 2 && begin
			ddp = diff(dp)
			any(==(1), dp[findall(==(0), ddp)])
		end
	end
end

# ╔═╡ d8d7bc84-71ec-436b-a42e-f2d2aa6a647c
function increment_password!(p)
	i = 8
	while true
		if p[i] == 25
			p[i] = 0
			i -= 1
		elseif p[i]+1 ∈ banned_letters
			p[i] += 2
			break
		else
			p[i] += 1
			break
		end
	end
end

# ╔═╡ c71dc902-c637-4a56-92b2-475c99439ad7
function next_password(p)
	increment_password!(p)
	while !test_password(p)
		increment_password!(p)
	end
	p
end

# ╔═╡ 44cde98c-ae9a-4270-9475-18ca2d1306f7
const part1 = input |> decode_password |> next_password |> encode_password

# ╔═╡ 661a37d0-5918-483c-a31a-078cbfe4091f
const part2 = part1 |> decode_password |> next_password |> encode_password

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═bda35764-2623-11ed-37ae-6d3329e033ad
# ╠═474227a4-b1f7-49a7-855e-90ad5591e036
# ╠═f9a988a1-c6d2-449a-9592-8a767f1e7af1
# ╠═fec3ea6a-6829-4d44-a294-5bc75768b4a2
# ╠═56e0c543-835f-4aa6-8aed-403d747709ef
# ╠═b58d216b-520c-48c0-8817-93d76179fba2
# ╠═b88f237b-11d1-42e2-a277-5cb7acd74464
# ╠═ff0aea06-dd7b-43ca-a318-ca52926e3633
# ╠═d8d7bc84-71ec-436b-a42e-f2d2aa6a647c
# ╠═c71dc902-c637-4a56-92b2-475c99439ad7
# ╠═44cde98c-ae9a-4270-9475-18ca2d1306f7
# ╠═661a37d0-5918-483c-a31a-078cbfe4091f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
