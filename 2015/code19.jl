### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 67a58da8-6d7c-4287-abb1-25bab65f789b
using DataStructures

# ╔═╡ 9e4fb3f4-d76e-4b97-88f4-70bc08252595
using Graphs

# ╔═╡ 1a60531e-2654-11ed-26ed-39500c6fbef7
const input = readlines("input19.txt")
# const input = readlines("/home/gis/Downloads/new_3.txt")

# ╔═╡ 17340e5a-d450-4160-909d-825acd0a8e10
parse_chemical(s) = [Symbol(e.match) for e=eachmatch(r"(e|[A-Z][a-z]*)", s)]

# ╔═╡ 8e7c0311-fd85-428e-8a06-a48e695c29fc
function parse_rule(s)
	m = match(r"^(\w+) => (\w+)$", s)
	if isnothing(m)
		error("invalid rule format '$s'")
	end
	parse_chemical(m[1])[1] => parse_chemical(m[2])
end

# ╔═╡ c8562bae-4917-4575-9ee2-85c1d961b9d3
const rule_list = parse_rule.(input[1:end-2])

# ╔═╡ 1702b91d-2607-49f9-88c8-f4a1e73440f2
const rules = begin
	d = Dict{Symbol, Vector{Vector{Symbol}}}()
	for rule=rule_list
		push!(get!(d, rule.first, []), rule.second)
	end
	d
end

# ╔═╡ 31a6d6c1-cabb-48e2-91ea-f13ffd49f40f
const target_molecule = parse_chemical(input[end])

# ╔═╡ 34b2ca48-4e63-4cb6-a0b8-02011c128ef9
function generate_forward_subs(molecule)
	unique(
		reduce(vcat,
			[
				[
					[molecule[1:i-1]; r; molecule[i+1:end]]
					for r=get(rules, e, [])
				]
				for (i,e)=enumerate(molecule)
			]
		)
	)
end

# ╔═╡ 610d9c3c-ee04-4bdc-ad5e-a7354aa9cb12
generate_forward_subs.(generate_forward_subs([:e]))

# ╔═╡ 853a4ff8-1fa9-4280-87d1-81ab77d1b1bd
const calibration_molecules = Set(reduce(vcat, [[[target_molecule[1:i-1]; r; target_molecule[i+1:end]] for r=get(rules, e, [])] for (i,e)=enumerate(target_molecule)]))

# ╔═╡ 7de3eebf-e325-4fd3-b717-feda263ed705
const part1 = length(calibration_molecules)

# ╔═╡ 7ae6508e-3d7f-4a56-a701-140d4eb3a497
function generate_back_subs(cb, molecule)
	for rule=rule_list
		lrs = length(rule.second)
		for i=1:length(molecule)-lrs+1
			if @view(molecule[i:i+lrs-1]) == rule.second
				cb([
					molecule[begin:i-1];
					rule.first;
					molecule[i+lrs:end]
				])
			end
		end
	end
end

# ╔═╡ 705a8082-2906-4010-9f58-af9fda6dd815
function find_synthesis(target_molecule; max_iter=500_000)
	i = 0
	sucessful_syntheses = Vector{Tuple{Vector{Symbol}, Int}}()

	seen = Set{Vector{Symbol}}()
	heap = BinaryHeap(Base.By(p -> length(first(p)) + last(p)), [(target_molecule, 0)])
	while i < max_iter && !isempty(heap)
		(m, s) = pop!(heap)
		generate_back_subs(m) do back
			if back ∉ seen
				if length(back) == 1 && back[1] == :e
					println("Found one")
					push!(sucessful_syntheses, (m,s))
				end
				push!(seen, back)
				push!(heap, (back, s+1))
			end
		end
		if [:e] ∈ seen
			return sucessful_syntheses[1][2] + 1
		end
		i += 1
	end
end

# ╔═╡ 4325b46c-65a5-4824-9b50-a64027e489de
const part2 = find_synthesis(target_molecule)

# ╔═╡ c11ed88d-90ac-4262-90d2-107271631e25
function make_rulegraph()
	
	# "digraph R{" *
	# join(
	# 	"$e->$(join(("$product" for product=unique(reduce(vcat, rules))), ","));"
	# 	for (e,rules)=rules
	# ) *
	# "}"

	es = []
	intern(x) = x ∈ es ? findfirst(==(x), es) : (push!(es, x); length(es))
	
	g = DiGraph{Int}(17)
	for (e,rs)=rules
		ke = intern(e)
		for product=unique(reduce(vcat, rs))
			kp = intern(product)
			add_edge!(g, ke, kp)
		end
	end
	
	sccs = strongly_connected_components(g)
	[es[scc] for scc=sccs]
end

# ╔═╡ e81cbc7e-9006-4aad-bbb5-0813eb5bc57e
make_rulegraph()

# ╔═╡ 3a95dcd1-1bcc-4e3a-97ce-b3d4409d5458
unique(reduce(vcat, [[1,2], [2,3]]))

# ╔═╡ cbc9a171-ffaa-4075-96fc-eda034d484f8
# digraph R{Al->Th,F;Al->Th,Rn,F,Ar;B->B,Ca;B->Ti,B;B->Ti,Rn,F,Ar;Ca->Ca,Ca;Ca->P,B;Ca->P,Rn,F,Ar;Ca->Si,Rn,F,Y,F,Ar;Ca->Si,Rn,Mg,Ar;Ca->Si,Th;F->Ca,F;F->P,Mg;F->Si,Al;H->C,Rn,Al,Ar;H->C,Rn,F,Y,F,Y,F,Ar;H->C,Rn,F,Y,Mg,Ar;H->C,Rn,Mg,Y,F,Ar;H->H,Ca;H->N,Rn,F,Y,F,Ar;H->N,Rn,Mg,Ar;H->N,Th;H->O,B;H->O,Rn,F,Ar;Mg->B,F;Mg->Ti,Mg;N->C,Rn,F,Ar;N->H,Si;O->C,Rn,F,Y,F,Ar;O->C,Rn,Mg,Ar;O->H,P;O->N,Rn,F,Ar;O->O,Ti;P->Ca,P;P->P,Ti;P->Si,Rn,F,Ar;Si->Ca,Si;Th->Th,Ca;Ti->B,P;Ti->Ti,Ti;e->H,F;e->N,Al;e->O,Mg;}

# ╔═╡ c7bf6903-6e9a-43de-8d55-1fda591bbccb
length(target_molecule)

# ╔═╡ 9bf8de64-fb10-4513-a130-85eb6a2b51f5
274 / 2 / 16

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"

[compat]
DataStructures = "~0.18.13"
Graphs = "~1.7.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "acdfb93408d544b7093c9c4fc5793fb00489cc75"

[[deps.ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "62e51b39331de8911e4a7ff6f5aaf38a5f4cc0ae"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Dates", "LinearAlgebra", "UUIDs"]
git-tree-sha1 = "5856d3031cdb1f3b2b6340dfdc66b6d9a149a374"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.2.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Graphs]]
deps = ["ArnoldiMethod", "Compat", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "a6d30bdc378d340912f48abf01281aab68c0dec8"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.7.2"

[[deps.Inflate]]
git-tree-sha1 = "5cd07aab533df5170988219191dfad0519391428"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.3"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "StaticArraysCore", "Statistics"]
git-tree-sha1 = "dfec37b90740e3b9aa5dc2613892a3fc155c3b42"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.5.6"

[[deps.StaticArraysCore]]
git-tree-sha1 = "ec2bd695e905a3c755b33026954b119ea17f2d22"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.3.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═67a58da8-6d7c-4287-abb1-25bab65f789b
# ╠═9e4fb3f4-d76e-4b97-88f4-70bc08252595
# ╠═1a60531e-2654-11ed-26ed-39500c6fbef7
# ╠═17340e5a-d450-4160-909d-825acd0a8e10
# ╠═8e7c0311-fd85-428e-8a06-a48e695c29fc
# ╠═c8562bae-4917-4575-9ee2-85c1d961b9d3
# ╠═1702b91d-2607-49f9-88c8-f4a1e73440f2
# ╠═31a6d6c1-cabb-48e2-91ea-f13ffd49f40f
# ╠═34b2ca48-4e63-4cb6-a0b8-02011c128ef9
# ╠═610d9c3c-ee04-4bdc-ad5e-a7354aa9cb12
# ╠═853a4ff8-1fa9-4280-87d1-81ab77d1b1bd
# ╠═7de3eebf-e325-4fd3-b717-feda263ed705
# ╠═7ae6508e-3d7f-4a56-a701-140d4eb3a497
# ╠═705a8082-2906-4010-9f58-af9fda6dd815
# ╠═4325b46c-65a5-4824-9b50-a64027e489de
# ╠═c11ed88d-90ac-4262-90d2-107271631e25
# ╠═e81cbc7e-9006-4aad-bbb5-0813eb5bc57e
# ╠═3a95dcd1-1bcc-4e3a-97ce-b3d4409d5458
# ╠═cbc9a171-ffaa-4075-96fc-eda034d484f8
# ╠═c7bf6903-6e9a-43de-8d55-1fda591bbccb
# ╠═9bf8de64-fb10-4513-a130-85eb6a2b51f5
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
