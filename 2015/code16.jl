### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ fce32064-2649-11ed-1f3a-4f335114b0b0
const input = readlines("input16.txt")

# ╔═╡ 6a19b2d5-ec4a-4a72-9387-1ea5e08937e4


# ╔═╡ 47f82638-0b3a-4968-b7b0-509f33ea5aaf
const cols = ["children", "cats", "samoyeds", "pomeranians", "akitas", "vizslas", "goldfish", "trees", "cars", "perfumes"]

# ╔═╡ dc464cc0-b6c8-47a2-9dbb-204a3ef7535c
const test_result = [3,7,2,3,0,0,5,3,2,1]

# ╔═╡ 9268706a-4444-4919-9908-be576b5ed05a
# children: 3
# cats: 7
# samoyeds: 2
# pomeranians: 3
# akitas: 0
# vizslas: 0
# goldfish: 5
# trees: 3
# cars: 2
# perfumes: 1

# ╔═╡ c0d92970-a200-42a9-beb1-bcc2d350a90b
function parse_sues(input)
	map(input) do i
		m = match(r"^Sue \d+: (\w+): (\d+), (\w+): (\d+), (\w+): (\d+)$", i)
		v = Vector{Union{Missing, Int}}(undef, length(cols))
		fill!(v, missing)
		v[findfirst(==(m[1]), cols)] = parse(Int, m[2])
		v[findfirst(==(m[3]), cols)] = parse(Int, m[4])
		v[findfirst(==(m[5]), cols)] = parse(Int, m[6])
		v
	end
end

# ╔═╡ 434eae0b-494f-4d67-9576-fc2e693d49c9
const sues = parse_sues(input)

# ╔═╡ 3f461055-1cd4-425a-bbe0-41d99bd9bc57
function test_part1(sue)
	sue .== test_result
end

# ╔═╡ a6784535-eefc-48b9-96eb-a818e9fcdc30
const matches_part1 = map(test_part1, sues)

# ╔═╡ 250d599d-0686-4db3-bfd1-399870386f90
const part1 = findmax(count ∘ skipmissing, matches_part1)[2]

# ╔═╡ 933925ef-c23d-4ada-be87-b5e32fcf97fe
function test_part2(sue)
	[op(sue,test_result) for (op,sue,test_result)=zip([==, >, ==, <, ==, ==, <, >, ==, ==], sue, test_result)]
end

# ╔═╡ aba8926e-fec4-43f2-b005-f58998d3c387
const matches_part2 = map(test_part2, sues)

# ╔═╡ 45e90e51-7641-4507-b288-fc00491e4e60
const part2 = findmax(count ∘ skipmissing, matches_part2)[2]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═fce32064-2649-11ed-1f3a-4f335114b0b0
# ╠═6a19b2d5-ec4a-4a72-9387-1ea5e08937e4
# ╠═47f82638-0b3a-4968-b7b0-509f33ea5aaf
# ╠═dc464cc0-b6c8-47a2-9dbb-204a3ef7535c
# ╠═9268706a-4444-4919-9908-be576b5ed05a
# ╠═c0d92970-a200-42a9-beb1-bcc2d350a90b
# ╠═434eae0b-494f-4d67-9576-fc2e693d49c9
# ╠═3f461055-1cd4-425a-bbe0-41d99bd9bc57
# ╠═a6784535-eefc-48b9-96eb-a818e9fcdc30
# ╠═250d599d-0686-4db3-bfd1-399870386f90
# ╠═933925ef-c23d-4ada-be87-b5e32fcf97fe
# ╠═aba8926e-fec4-43f2-b005-f58998d3c387
# ╠═45e90e51-7641-4507-b288-fc00491e4e60
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
