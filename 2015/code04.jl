### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 38b5fe28-4aaa-4e18-a1aa-f949ab61f5ca
using MD5

# ╔═╡ 187b2a24-255a-11ed-1cfb-b3f8ea940e9e
const input = "bgvyzdsv"

# ╔═╡ bb2aac88-77d1-497d-ba5d-dcfe37c440a3
const test_input = "pqrstuv"

# ╔═╡ 750cb01e-1a95-49c8-b8ab-7cae7b72c153
md5(test_input)

# ╔═╡ 9b52e057-9466-40f4-af39-10f0d71a0981
function test_part1(s)
	digest = md5(s)
	digest[1] == 0 && digest[2] == 0 && digest[3] < 0x10
end

# ╔═╡ 4b44fac5-3462-44bd-8980-4c6340f61592
function search(key, test)
	i = 1
	while !test(key * string(i))
		i += 1
	end
	i
end

# ╔═╡ 41f5485e-d20c-4556-b9cb-207c6b4a56f3
const part1 = search(input, test_part1)

# ╔═╡ 77ee4ee4-f1f5-4f25-9466-23702bf5cb29
function test_part2(s)
	digest = md5(s)
	digest[1] == 0 && digest[2] == 0 && digest[3] == 0
end

# ╔═╡ 5bdf1514-a3b0-4c52-8626-aaa2cb0bed46
const part2 = search(input, test_part2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
MD5 = "6ac74813-4b46-53a4-afec-0b5dc9d7885c"

[compat]
MD5 = "~0.2.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "70130695b488287559a34dc41bf43c457c24ae1d"

[[deps.MD5]]
deps = ["Random", "SHA"]
git-tree-sha1 = "eeffe42284464c35a08026d23aa948421acf8923"
uuid = "6ac74813-4b46-53a4-afec-0b5dc9d7885c"
version = "0.2.1"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"
"""

# ╔═╡ Cell order:
# ╠═187b2a24-255a-11ed-1cfb-b3f8ea940e9e
# ╠═38b5fe28-4aaa-4e18-a1aa-f949ab61f5ca
# ╠═bb2aac88-77d1-497d-ba5d-dcfe37c440a3
# ╠═750cb01e-1a95-49c8-b8ab-7cae7b72c153
# ╠═9b52e057-9466-40f4-af39-10f0d71a0981
# ╠═4b44fac5-3462-44bd-8980-4c6340f61592
# ╠═41f5485e-d20c-4556-b9cb-207c6b4a56f3
# ╠═77ee4ee4-f1f5-4f25-9466-23702bf5cb29
# ╠═5bdf1514-a3b0-4c52-8626-aaa2cb0bed46
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
