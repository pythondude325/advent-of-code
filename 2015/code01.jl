### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 2c16acd6-2553-11ed-0cb3-5db5d0d96cd0
const data = readlines("input01.txt")[1]

# ╔═╡ 940aff62-caa0-46c3-8815-3d18e26a1d7c
foldl(((a, c) -> c == '(' ? a+1 : a-1), data, init=0)

# ╔═╡ 48f3412b-7614-451a-bd81-89fd839f6bdb
const levels = accumulate(((a, c) -> c == '(' ? a+1 : a-1), data, init=0)

# ╔═╡ 3307f483-dd0b-4cd2-9031-dad606d0c32b
findfirst(<(0), levels)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═2c16acd6-2553-11ed-0cb3-5db5d0d96cd0
# ╠═940aff62-caa0-46c3-8815-3d18e26a1d7c
# ╠═48f3412b-7614-451a-bd81-89fd839f6bdb
# ╠═3307f483-dd0b-4cd2-9031-dad606d0c32b
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
