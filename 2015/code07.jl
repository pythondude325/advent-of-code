### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ bce513c1-a210-48de-b8f5-3ccd41a9001d
using Graphs

# ╔═╡ 40198b28-2561-11ed-291b-fb8310c76e80
const test_input = split("123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i", "\n")

# ╔═╡ c8fc64b7-f1f6-4c59-902e-6f3df5bd182c
const input = readlines("input07.txt")

# ╔═╡ 0f04840a-3c0a-4c34-ba11-4baef0b66479
abstract type Operand end

# ╔═╡ eb58708b-6532-45b2-95b8-94ad8bf26f3d
struct Value <: Operand
	val::UInt16
end

# ╔═╡ a4de1e1e-4c30-4903-9970-c47b80406d0c
struct Register <: Operand
	reg::Symbol
end

# ╔═╡ dc6c414b-b9ba-4143-8d80-702cf49eac4c
function Base.parse(::Type{Operand}, s)::Operand
	match_value = match(r"^([0-9]+)$", s)
	if !isnothing(match_value)
		return Value(parse(UInt16, match_value[1]))
	end
	match_register = match(r"^([a-z]+)$", s)
	if !isnothing(match_register)
		return Register(Symbol(match_register[1]))
	end
	error("Invalid operand format '$s'")
end

# ╔═╡ a787f610-b2dd-4e1c-9e33-e665492a1025
abstract type Operation end

# ╔═╡ 7b99b43f-af61-4a44-920d-f1d518bad167
struct Mov <: Operation
	reg1::Operand
end

# ╔═╡ d196808f-4766-480c-8bf7-dd3197b7326c
struct Not <: Operation
	reg1::Operand
end

# ╔═╡ 9b2520a7-5432-46ed-b039-67362c21330a
struct And <: Operation
	reg1::Operand
	reg2::Operand
end

# ╔═╡ 094fbea2-6639-42d0-b01f-00b5dce75a67
struct Or <: Operation
	reg1::Operand
	reg2::Operand
end

# ╔═╡ bd61ed09-47df-47ed-9551-d4023e1ff3bb
struct LShift <: Operation
	reg1::Operand
	reg2::Operand
end

# ╔═╡ c1d469a5-f329-45be-9957-4c304eb17c0a
struct RShift <: Operation
	reg1::Operand
	reg2::Operand
end

# ╔═╡ 9bb9bc72-bf63-476f-bb30-e69d2eb78d48
function Base.parse(::Type{Operation}, s)::Operation
	match_not = match(r"^NOT (\w+)$", s)
	if !isnothing(match_not)
		return Not(
			parse(Operand, match_not[1]),
		)
	end
	match_set = match(r"^(\w+)$", s)
	if !isnothing(match_set)
		return Mov(
			parse(Operand, match_set[1])
		)
	end
	match_lshift = match(r"^(\w+) LSHIFT (\w+)$", s)
	if !isnothing(match_lshift)
		return LShift(
			parse(Operand, match_lshift[1]),
			parse(Operand, match_lshift[2]),
		)
	end
	match_rshift = match(r"^(\w+) RSHIFT (\w+)$", s)
	if !isnothing(match_rshift)
		return RShift(
			parse(Operand, match_rshift[1]),
			parse(Operand, match_rshift[2]),
		)
	end
	match_and = match(r"^(\w+) AND (\w+)$", s)
	if !isnothing(match_and)
		return And(
			parse(Operand, match_and[1]),
			parse(Operand, match_and[2]),
		)
	end
	match_or = match(r"^(\w+) OR (\w+)$", s)
	if !isnothing(match_or)
		return Or(
			parse(Operand, match_or[1]),
			parse(Operand, match_or[2]),
		)
	end

	error("Invalid instruction format '$s'")
end

# ╔═╡ 4bfb19f2-9733-44f6-8084-c76e49607e70
struct Instruction
	operation::Operation
	destination::Operand
end

# ╔═╡ 4b6f0a28-b2ef-4874-bc74-d9f3c5a1bd1c
sources(instr::Instruction) = sources(instr.operation)

# ╔═╡ 3a6fb84a-410a-44ab-bf56-97256de3ba59
sources(mov::Mov) = (mov.reg1,)

# ╔═╡ 15abd284-0ef2-4c66-b6ad-c626a40ce083
sources(not::Not) = (not.reg1,)

# ╔═╡ 5a8ed1f8-e5b7-4115-bfac-5acf2928c808
sources(and::And) = (and.reg1, and.reg2)

# ╔═╡ 95da64bf-e5a8-425d-9710-a74a0581d08c
sources(or::Or) = (or.reg1, or.reg2)

# ╔═╡ d6c1a826-831d-47b8-b249-0da761b64558
sources(rshift::RShift) = (rshift.reg1, rshift.reg2)

# ╔═╡ 15556235-89e8-48e5-abd5-2cd0c842f942
sources(lshift::LShift) = (lshift.reg1, lshift.reg2)

# ╔═╡ 292ccff0-75c3-43ef-8fdb-1c575b33cb74
function Base.parse(::Type{Instruction}, s)::Instruction
	m = match(r"^(.*) -> (\w+)$", s)
	if !isnothing(m)
		return Instruction(
			parse(Operation, m[1]),
			parse(Operand, m[2])
		)
	end
	error("Invalid instruction format '$s'")
end

# ╔═╡ 662794a2-5e39-4b96-8a53-24add419db74
const instructions = parse.(Instruction, input)

# ╔═╡ 881758b7-4109-4d28-85e3-83a7545eb20d
function sort_instructions(instructions)
	wires = Dict{Register, Int}()
	for (i,instruction)=enumerate(instructions)
		wires[instruction.destination] = i
	end
	g = DiGraph{Int}(length(instructions))
	for (i,instruction)=enumerate(instructions)
		for source=sources(instruction)
			if source isa Register
				add_edge!(g, i, wires[source])
			end
		end
	end

	instructions[reverse(topological_sort_by_dfs(g))]
end

# ╔═╡ 4ae2735e-c25f-4e23-823d-fd07d6f12c34
const reordered_instructions = sort_instructions(instructions)

# ╔═╡ 09b1b5a5-d129-4455-a0f0-b468a9703fec
function interpret(program::Vector{Instruction})
	registers = Dict{Symbol, UInt16}()
	load(val::Value)::UInt16 = val.val
	load(reg::Register)::UInt16 = registers[reg.reg]
	store(reg::Register, value::UInt16) = (registers[reg.reg] = value)
	
	execute(mov::Mov) = load(mov.reg1)
	execute(not::Not) = ~load(not.reg1)
	execute(and::And) = load(and.reg1) & load(and.reg2)
	execute(or::Or) = load(or.reg1) | load(or.reg2)
	execute(lshift::LShift) = load(lshift.reg1) << load(lshift.reg2)
	execute(rshift::RShift) = load(rshift.reg1) >> load(rshift.reg2)

	for instruction=program
		store(instruction.destination, execute(instruction.operation))
	end

	registers[:a]
end

# ╔═╡ 6e59463f-306c-4197-b09f-7904c34a2fad
const part1 = interpret(reordered_instructions)

# ╔═╡ f6863047-83df-417e-9c45-1c9e3b2dbdda
Int(part1)

# ╔═╡ de1857bd-4c94-4f82-8577-c0f5562efed2
const part2_instructions = [Instruction(Mov(Value(part1)), Register(:b)); reordered_instructions[2:end]]

# ╔═╡ aa56edc0-19cd-434d-af53-1203f04ead71
const part2 = interpret(part2_instructions)

# ╔═╡ 7b6e7915-05b7-4e11-a5de-8b3a9f497c3f
Int(part2)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Graphs = "86223c79-3864-5bf0-83f7-82e725a168b6"

[compat]
Graphs = "~1.7.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "28c280795210967a866250a6efcbe46f42575b40"

[[deps.ArnoldiMethod]]
deps = ["LinearAlgebra", "Random", "StaticArrays"]
git-tree-sha1 = "62e51b39331de8911e4a7ff6f5aaf38a5f4cc0ae"
uuid = "ec485272-7323-5ecc-a04f-4719b315124d"
version = "0.2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Dates", "LinearAlgebra", "UUIDs"]
git-tree-sha1 = "5856d3031cdb1f3b2b6340dfdc66b6d9a149a374"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.2.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Graphs]]
deps = ["ArnoldiMethod", "Compat", "DataStructures", "Distributed", "Inflate", "LinearAlgebra", "Random", "SharedArrays", "SimpleTraits", "SparseArrays", "Statistics"]
git-tree-sha1 = "a6d30bdc378d340912f48abf01281aab68c0dec8"
uuid = "86223c79-3864-5bf0-83f7-82e725a168b6"
version = "1.7.2"

[[deps.Inflate]]
git-tree-sha1 = "5cd07aab533df5170988219191dfad0519391428"
uuid = "d25df0c9-e2be-5dd7-82c8-3ad0b3e990b9"
version = "0.1.3"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.SimpleTraits]]
deps = ["InteractiveUtils", "MacroTools"]
git-tree-sha1 = "5d7e3f4e11935503d3ecaf7186eac40602e7d231"
uuid = "699a6c99-e7fa-54fc-8d76-47d257e15c1d"
version = "0.9.4"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "StaticArraysCore", "Statistics"]
git-tree-sha1 = "dfec37b90740e3b9aa5dc2613892a3fc155c3b42"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.5.6"

[[deps.StaticArraysCore]]
git-tree-sha1 = "ec2bd695e905a3c755b33026954b119ea17f2d22"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.3.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═bce513c1-a210-48de-b8f5-3ccd41a9001d
# ╠═40198b28-2561-11ed-291b-fb8310c76e80
# ╠═c8fc64b7-f1f6-4c59-902e-6f3df5bd182c
# ╠═0f04840a-3c0a-4c34-ba11-4baef0b66479
# ╠═eb58708b-6532-45b2-95b8-94ad8bf26f3d
# ╠═a4de1e1e-4c30-4903-9970-c47b80406d0c
# ╠═dc6c414b-b9ba-4143-8d80-702cf49eac4c
# ╠═9bb9bc72-bf63-476f-bb30-e69d2eb78d48
# ╠═a787f610-b2dd-4e1c-9e33-e665492a1025
# ╠═7b99b43f-af61-4a44-920d-f1d518bad167
# ╠═d196808f-4766-480c-8bf7-dd3197b7326c
# ╠═9b2520a7-5432-46ed-b039-67362c21330a
# ╠═094fbea2-6639-42d0-b01f-00b5dce75a67
# ╠═bd61ed09-47df-47ed-9551-d4023e1ff3bb
# ╠═c1d469a5-f329-45be-9957-4c304eb17c0a
# ╠═4bfb19f2-9733-44f6-8084-c76e49607e70
# ╠═4b6f0a28-b2ef-4874-bc74-d9f3c5a1bd1c
# ╠═3a6fb84a-410a-44ab-bf56-97256de3ba59
# ╠═15abd284-0ef2-4c66-b6ad-c626a40ce083
# ╠═5a8ed1f8-e5b7-4115-bfac-5acf2928c808
# ╠═95da64bf-e5a8-425d-9710-a74a0581d08c
# ╠═d6c1a826-831d-47b8-b249-0da761b64558
# ╠═15556235-89e8-48e5-abd5-2cd0c842f942
# ╠═292ccff0-75c3-43ef-8fdb-1c575b33cb74
# ╠═662794a2-5e39-4b96-8a53-24add419db74
# ╠═881758b7-4109-4d28-85e3-83a7545eb20d
# ╠═4ae2735e-c25f-4e23-823d-fd07d6f12c34
# ╠═09b1b5a5-d129-4455-a0f0-b468a9703fec
# ╠═6e59463f-306c-4197-b09f-7904c34a2fad
# ╠═f6863047-83df-417e-9c45-1c9e3b2dbdda
# ╠═de1857bd-4c94-4f82-8577-c0f5562efed2
# ╠═aa56edc0-19cd-434d-af53-1203f04ead71
# ╠═7b6e7915-05b7-4e11-a5de-8b3a9f497c3f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
