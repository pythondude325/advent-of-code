### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 8526620c-255e-11ed-1953-ef2f9cebc485
const input = readlines("input06.txt")

# ╔═╡ 021a8c42-5247-474b-bcd5-6baeaa8619f4
@enum InstructionType TurnOn TurnOff Toggle

# ╔═╡ 4f7857b6-fa1f-4426-b0f7-05db413ae61f
function parse_instruction(s)::Tuple{InstructionType, CartesianIndices}
	m = match(r"(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)", s)
	i,a,b,c,d = m
	t = if i == "turn on"
		TurnOn
	elseif i == "turn off"
		TurnOff
	elseif i == "toggle"
		Toggle
	end
	t, CartesianIndices((parse(Int, a)+1:parse(Int, c)+1, parse(Int, b)+1:parse(Int, d)+1))
end

# ╔═╡ eeb439b6-751c-4b8a-baeb-c0e5da0850d3
const instructions = parse_instruction.(input)

# ╔═╡ 4c93fa9d-bda0-41ea-994f-ac9da9373d2b
function execute_instructions_part1(instructions)
	foldl(instructions, init=falses(1000,1000)) do a, (t,c)
		if t == TurnOff
			a[c] .= false
		elseif t == TurnOn
			a[c] .= true
		elseif t == Toggle
			a[c] .⊻= true
		end
		a
	end
end

# ╔═╡ c0fb7c67-eac4-486f-b0f9-e4e4f4877074
const part1 = count(execute_instructions_part1(instructions))

# ╔═╡ 71930a94-6dca-47ae-abbb-8c50f56329c6
function execute_instructions_part2(instructions)
	foldl(instructions, init=zeros(Int, 1000,1000)) do a, (t,c)
		if t == TurnOff
			a[c] .= max.(0, a[c] .- 1)
		elseif t == TurnOn
			a[c] .+= 1
		elseif t == Toggle
			a[c] .+= 2
		end
		a
	end
end

# ╔═╡ 745bab8d-252a-4ea5-9112-9662d61541c7
const part2 = sum(execute_instructions_part2(instructions))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═8526620c-255e-11ed-1953-ef2f9cebc485
# ╠═021a8c42-5247-474b-bcd5-6baeaa8619f4
# ╠═4f7857b6-fa1f-4426-b0f7-05db413ae61f
# ╠═eeb439b6-751c-4b8a-baeb-c0e5da0850d3
# ╠═4c93fa9d-bda0-41ea-994f-ac9da9373d2b
# ╠═c0fb7c67-eac4-486f-b0f9-e4e4f4877074
# ╠═71930a94-6dca-47ae-abbb-8c50f56329c6
# ╠═745bab8d-252a-4ea5-9112-9662d61541c7
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
