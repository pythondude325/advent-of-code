### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 6fca5464-27e2-11ed-0616-231d569da6b8
const input = readlines("input24.txt")

# ╔═╡ ff14ad71-b78f-4e50-a250-57706c37ec42
const packages = reverse(parse.(Int, input))

# ╔═╡ 0c727ead-de36-4888-904e-f7298f455f8b
function find_partitions(containers::AbstractVector{Int}, eggnog::Int; used=Int[])::Vector{Vector{Int}}
	if length(containers) == 0
		Vector{Int}[]
	else
		combinations = find_partitions(@view(containers[2:end]), eggnog, used=used)
		if containers[1] == eggnog
			push!(combinations, Int[used; containers[1]])
		elseif containers[1] < eggnog
			append!(
				combinations,
				find_partitions(
					@view(containers[2:end]),
					eggnog - containers[1],
					used=[used; containers[1]]
				)
			)
		end
		combinations
	end
end

# ╔═╡ f7361209-9768-488e-86ae-e2d36eb0613d
Int[Int[1,2]; 3]

# ╔═╡ 32cc482b-f748-4e7a-8df7-7ba8a14c1da4
function count_partitions(containers::AbstractVector{Int}, eggnog::Int)::Int
	if length(containers) == 0
		0
	else
		combinations = count_partitions(@view(containers[2:end]), eggnog)
		if containers[1] == eggnog
			combinations += 1
		elseif containers[1] < eggnog
			combinations += count_partitions(
				@view(containers[2:end]),
				eggnog - containers[1],
			)
		end
		combinations
	end
end

# ╔═╡ cbdf3568-22ad-4f0d-92ff-f6d98a701d09
function any_partitions(containers::AbstractVector{Int}, eggnog::Int)::Bool
	if length(containers) == 0
		false
	else
		
		if containers[1] == eggnog
			true
		elseif containers[1] < eggnog
			any_partitions(@view(containers[2:end]), eggnog) || any_partitions(
				@view(containers[2:end]),
				eggnog - containers[1],
			)
		else
			any_partitions(@view(containers[2:end]), eggnog)
		end
	end
end

# ╔═╡ b2c05cd8-abf6-47c3-b7df-4e8e6ee1a78f
sanity_check_g1(g1) = any_partitions(filter(p -> p ∉ g1, packages), 520)

# ╔═╡ 953cd1cd-71b3-4935-a7a3-a66b77999fda
function find_group1s(packages::Vector{Int})
	group_weight = sum(packages) ÷ 3

	combinations = find_partitions(packages, group_weight)
	minlen = minimum(length, combinations)
	min_combinations = filter(c -> length(c)==minlen && sanity_check_g1(c), combinations)
	minimum(prod, min_combinations)
end

# ╔═╡ c6c0bab7-6b96-4b83-b3f7-5c52d3ec58b3
const part1 = find_group1s(packages)

# ╔═╡ 3d80e918-90f0-458c-bfe1-2510f100999d
function find_group1s_part2(packages::Vector{Int})
	group_weight = sum(packages) ÷ 4

	function check_g1_part2(g1)
			ng1 = filter(∉(g1), packages)
			r2ps = find_partitions(ng1, group_weight)
			any(r2ps) do g2
				ng2 = filter(∉(g2), ng1)
				any_partitions(ng2, group_weight)
			end
	end
	
	r1ps = find_partitions(packages, group_weight)
	
	sort!(r1ps, by=p -> (length(p), prod(p)))
	prod(first(Iterators.filter(check_g1_part2, r1ps)))
	# filter!(p -> length(p) == minlen, r1ps)
	# minimum(prod, r1ps)
end

# ╔═╡ aeda198d-2243-4c9a-b065-8c62d3c47e82
const part2 = find_group1s_part2(packages)

# ╔═╡ 16d85054-ad67-4254-995c-58755d9ae322
find_group1s([1:5; 7:11])

# ╔═╡ f80301b7-8dcf-4842-9ff8-95c11d45b580
find_group1s_part2([1:5; 7:11])

# ╔═╡ 7ed1eb0a-6720-4aa0-a4cb-f213477f0053


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═6fca5464-27e2-11ed-0616-231d569da6b8
# ╠═ff14ad71-b78f-4e50-a250-57706c37ec42
# ╠═0c727ead-de36-4888-904e-f7298f455f8b
# ╠═f7361209-9768-488e-86ae-e2d36eb0613d
# ╠═32cc482b-f748-4e7a-8df7-7ba8a14c1da4
# ╠═cbdf3568-22ad-4f0d-92ff-f6d98a701d09
# ╠═953cd1cd-71b3-4935-a7a3-a66b77999fda
# ╠═b2c05cd8-abf6-47c3-b7df-4e8e6ee1a78f
# ╠═c6c0bab7-6b96-4b83-b3f7-5c52d3ec58b3
# ╠═3d80e918-90f0-458c-bfe1-2510f100999d
# ╠═aeda198d-2243-4c9a-b065-8c62d3c47e82
# ╠═16d85054-ad67-4254-995c-58755d9ae322
# ╠═f80301b7-8dcf-4842-9ff8-95c11d45b580
# ╠═7ed1eb0a-6720-4aa0-a4cb-f213477f0053
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
