### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 4ef01326-2555-11ed-096c-83182a9912f0
const input = readlines("input03.txt")[1]

# ╔═╡ eb33bf7d-1c5f-4069-900a-32c240e5ddf6
parse_direction(c)::Complex{Int} =
	if c == '^'
		im
	elseif c == 'v'
		-im
	elseif c == '>'
		1
	elseif c == '<'
		-1
	end

# ╔═╡ fe188f69-74ff-40c4-887d-70ab9b0b81b8
const directions = parse_direction.(collect(input))

# ╔═╡ da8ec608-d55b-4950-aca1-910751d4b1d2
const part1_houses = Set([0+0im; cumsum(directions)])

# ╔═╡ 6dff5960-a120-407f-aeb7-a159ef44cbe4
const part1_answer = length(part1_houses)

# ╔═╡ daa88732-259e-4bdf-8f7f-26784955284d
const part2_houses = Set([0+0im; cumsum(directions[1:2:end]); cumsum(directions[2:2:end])])

# ╔═╡ ed1d91d1-7cbc-4764-b7b9-66d9a4a89d5a
const part2_answer = length(part2_houses)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═4ef01326-2555-11ed-096c-83182a9912f0
# ╠═eb33bf7d-1c5f-4069-900a-32c240e5ddf6
# ╠═fe188f69-74ff-40c4-887d-70ab9b0b81b8
# ╠═da8ec608-d55b-4950-aca1-910751d4b1d2
# ╠═6dff5960-a120-407f-aeb7-a159ef44cbe4
# ╠═daa88732-259e-4bdf-8f7f-26784955284d
# ╠═ed1d91d1-7cbc-4764-b7b9-66d9a4a89d5a
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
