### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ eefdd75e-2553-11ed-3136-01f5f4ca386c
const input = readlines("input02.txt")

# ╔═╡ e260c613-3665-4ddc-81c1-b575ce1752e3
parse_box(s)::NTuple{3,Int} = (parse.(Int, split(s, "x"))...,)

# ╔═╡ ae5dc4f4-c0bc-4364-baa7-b7cea5488176
const boxes = parse_box.(input)

# ╔═╡ 681cea2d-0262-4602-bcfc-0ee76c121bf6
surface_area((l,h,w)) = 2 * (l*h + h*w + w*l) + min(l*h, h*w, w*l)

# ╔═╡ 46c9bed9-bd70-4aa2-8325-a2413bc15f59
const total_area = sum(surface_area.(boxes))

# ╔═╡ 2f261e83-c665-4a86-b95e-28a90e64505c
ribbon_length((l,h,w)) = l*h*w + 2*min(l+h, h+w, w+l)

# ╔═╡ a06bb37d-9e20-4902-95f9-ddc969a96621
const total_length = sum(ribbon_length.(boxes))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═eefdd75e-2553-11ed-3136-01f5f4ca386c
# ╠═e260c613-3665-4ddc-81c1-b575ce1752e3
# ╠═ae5dc4f4-c0bc-4364-baa7-b7cea5488176
# ╠═681cea2d-0262-4602-bcfc-0ee76c121bf6
# ╠═46c9bed9-bd70-4aa2-8325-a2413bc15f59
# ╠═2f261e83-c665-4a86-b95e-28a90e64505c
# ╠═a06bb37d-9e20-4902-95f9-ddc969a96621
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
