### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 8fc78d02-0cf5-48dd-bba2-23febcd47a18
using JSON

# ╔═╡ 6492546e-2627-11ed-1ead-9b740fc8b534
const input = readlines("input12.txt")[1]

# ╔═╡ c97c99ec-46ec-438b-933f-247c12ed83f8
const data = JSON.parse(input)

# ╔═╡ e164e438-e869-4a78-9daa-10661d410eb8
summary(data[1][2][1])

# ╔═╡ 065baf07-14eb-47a3-abe3-a422ca73c9ae
total(s::String) = 0

# ╔═╡ cb91282a-a142-4ad1-9561-70b0a8b14744
total(i::Int64) = i

# ╔═╡ 6f0f8ce8-077a-4e10-9b96-373f12fac645
total(d::Dict{String, Any}) = sum(total.(values(d)))

# ╔═╡ f1f30ed2-e313-4d1b-bc5f-a9aacd7fe8a1
total(v::Vector{Any}) = sum(total.(v))

# ╔═╡ 4f96e891-cbb0-4671-a268-fc6e718c1018
const part1 = total(data)

# ╔═╡ 5a7da533-38f4-4f27-a82c-bc24cfb2d773
total_part2(s::String) = 0

# ╔═╡ e94920cb-bdc4-4173-b3de-8806561abf1b
total_part2(i::Int64) = i

# ╔═╡ 3bc3efa7-97f7-4d6c-98d3-133c1f49a2fe
function total_part2(d::Dict{String, Any})
	vs = values(d)
	"red" ∈ vs ? 0 : sum(total_part2.(vs))
end

# ╔═╡ ffce566d-c2e7-4769-82f7-a6df2bf49974
total_part2(v::Vector{Any}) = sum(total_part2.(v))

# ╔═╡ c2c66b1c-1d00-4fc0-ad3b-032417ebeacb
const part2 = total_part2(data)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
JSON = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"

[compat]
JSON = "~0.21.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "394674f2e1dbc0990c888c3ca4df111f6d9b2796"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "0044b23da09b5608b4ecacb4e5e6c6332f833a7e"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.2"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"
"""

# ╔═╡ Cell order:
# ╠═8fc78d02-0cf5-48dd-bba2-23febcd47a18
# ╠═6492546e-2627-11ed-1ead-9b740fc8b534
# ╠═c97c99ec-46ec-438b-933f-247c12ed83f8
# ╠═e164e438-e869-4a78-9daa-10661d410eb8
# ╠═065baf07-14eb-47a3-abe3-a422ca73c9ae
# ╠═cb91282a-a142-4ad1-9561-70b0a8b14744
# ╠═6f0f8ce8-077a-4e10-9b96-373f12fac645
# ╠═f1f30ed2-e313-4d1b-bc5f-a9aacd7fe8a1
# ╠═4f96e891-cbb0-4671-a268-fc6e718c1018
# ╠═5a7da533-38f4-4f27-a82c-bc24cfb2d773
# ╠═e94920cb-bdc4-4173-b3de-8806561abf1b
# ╠═3bc3efa7-97f7-4d6c-98d3-133c1f49a2fe
# ╠═ffce566d-c2e7-4769-82f7-a6df2bf49974
# ╠═c2c66b1c-1d00-4fc0-ad3b-032417ebeacb
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
