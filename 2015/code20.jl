### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 9168997b-144d-4b3c-b27c-d3e204a076d0
using Primes

# ╔═╡ fa693f88-27bd-11ed-1c69-c98e3b6ad3b4
const input = 34000000

# ╔═╡ e6dd2cfd-dcd9-49a4-ba15-35051f028d4e
[pfact for pfact=factor(12)]

# ╔═╡ 2727a940-5d6c-4889-aaba-29b4aa60c30c
function σ1(n)
	#=
	Gives us the SUM of the divisors.
	https://mathworld.wolfram.com/DivisorFunction.html
	See equation 14 in the above link.
	=#
	prod((p^(k + 1) - 1)÷(p - 1) for (p,k)=factor(n), init=1)
end

# ╔═╡ 85381970-2a91-41f6-b67b-61f14550d99c
function search()
	i = 2
	while σ1(i)*10 < input
		i += 1
	end
	i
end

# ╔═╡ 32462b83-98fb-4665-a000-3857cb43e2fc
const part1 = search()

# ╔═╡ 41f0390d-99a3-4ad9-b2fa-3e5fb1250cd0
q3(n) = n + sum(n .÷ Iterators.filter(k -> n .% k .== 0, 2:50))

# ╔═╡ c617d1f3-7029-4f42-9780-00efabacf976
function search_part2()
	i = 2
	while q3(i)*11 < input
		i += 1
	end
	i
end

# ╔═╡ ccdabaf6-91d2-4e59-86b6-3d4dbe3bbcf1
const part2 = search_part2()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Primes = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"

[compat]
Primes = "~0.5.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "0c4d3af99c9f69506536d54356b41ae580afc070"

[[deps.IntegerMathUtils]]
git-tree-sha1 = "f366daebdfb079fd1fe4e3d560f99a0c892e15bc"
uuid = "18e54dd8-cb9d-406c-a71d-865a43cbb235"
version = "0.1.0"

[[deps.Primes]]
deps = ["IntegerMathUtils"]
git-tree-sha1 = "311a2aa90a64076ea0fac2ad7492e914e6feeb81"
uuid = "27ebfcd6-29c5-5fa9-bf4b-fb8fc14df3ae"
version = "0.5.3"
"""

# ╔═╡ Cell order:
# ╠═9168997b-144d-4b3c-b27c-d3e204a076d0
# ╠═fa693f88-27bd-11ed-1c69-c98e3b6ad3b4
# ╠═e6dd2cfd-dcd9-49a4-ba15-35051f028d4e
# ╠═2727a940-5d6c-4889-aaba-29b4aa60c30c
# ╠═85381970-2a91-41f6-b67b-61f14550d99c
# ╠═32462b83-98fb-4665-a000-3857cb43e2fc
# ╠═41f0390d-99a3-4ad9-b2fa-3e5fb1250cd0
# ╠═c617d1f3-7029-4f42-9780-00efabacf976
# ╠═ccdabaf6-91d2-4e59-86b6-3d4dbe3bbcf1
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
