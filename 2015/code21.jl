### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ f4ba9ac8-27c7-11ed-28f1-03829199c882
const input = readlines("input21.txt")

# ╔═╡ 29a02557-51fb-4f54-b98b-1f06f0cfd310
struct Fighter
	hp::Int
	damage::Int
	armor::Int
end

# ╔═╡ bbddb591-a420-4d11-bf2c-8c58e83109a0
const boss = Fighter(
	parse(Int, input[1][12:end]),
	parse(Int, input[2][8:end]),
	parse(Int, input[3][7:end]),
)

# ╔═╡ 2fdeb9fd-6d09-439c-8b95-ce18e3a036c7
attack(attacker::Fighter, defender::Fighter)::Int = max(1, attacker.damage - defender.armor)

# ╔═╡ 06ff07fa-369a-4415-8191-d9f58df0fe28
test_battle(battle) = [battle(Fighter(e,a,b), Fighter(f,c,d)) for a=0:5,b=0:5,c=0:5,d=0:5,e=1:10,f=1:10]

# ╔═╡ 4ea9c5ea-6421-4dea-b6c0-d3db755ac286
function try_battle(player, boss)
	player_hp = player.hp
	boss_hp = boss.hp
	while true
		boss_hp -= attack(player, boss)
		if boss_hp <= 0
			return true
		end
		player_hp -= attack(boss, player)
		if player_hp <= 0
			return false
		end
	end
end

# ╔═╡ 435b379b-f3c0-42e2-8a81-422f38ba6ce1
@assert try_battle(Fighter(8,5,5), Fighter(12,7,2)) == true

# ╔═╡ d9193dfa-e937-435e-9ed2-8dc122f6b86f
boss.hp

# ╔═╡ 8879d61d-5493-4af1-815e-91af44d90876
function try_battle2(a, b)
	ceil(100 / max(1, a.damage - 2)) - ceil(100 / max(1, 8 - a.armor)) <= 0
	# ceil(100 / max(1, a.damage - 2)) <= ceil(a.hp / max(1, 8 - a.armor))
	# ceil(b.hp / max(1, a.damage - b.armor)) <= ceil(a.hp / max(1, b.damage - a.armor))
end

# ╔═╡ d757780b-b9e0-48a1-90f3-3a1e17690af3
test_battle(try_battle) == test_battle(try_battle2)

# ╔═╡ 1a3c3515-1af5-4308-b0a6-6b6c77d482dd
# Weapons:    Cost  Damage  Armor
# Dagger        8     4       0
# Shortsword   10     5       0
# Warhammer    25     6       0
# Longsword    40     7       0
# Greataxe     74     8       0
const weapons_shop = [
	8 10 25 40 74
	4 5 6 7 8
	0 0 0 0 0
]

# ╔═╡ 2d61b789-a22c-487d-9f23-05194218f2eb
# Armor:      Cost  Damage  Armor
# Leather      13     0       1
# Chainmail    31     0       2
# Splintmail   53     0       3
# Bandedmail   75     0       4
# Platemail   102     0       5
const armor_shop = [
	13 31 53 75 102
	0 0 0 0 0
	1 2 3 4 5
]

# ╔═╡ aa00b4ee-9177-4387-9517-fe93ca752c54
# Rings:      Cost  Damage  Armor
# Damage +1    25     1       0
# Damage +2    50     2       0
# Damage +3   100     3       0
# Defense +1   20     0       1
# Defense +2   40     0       2
# Defense +3   80     0       3
const ring_shop = [
	25 50 100 20 40 80
	1 2 3 0 0 0
	0 0 0 1 2 3
]

# ╔═╡ b52c816b-261e-4fc1-8760-3a1e28faeb5f
const shops = [weapons_shop armor_shop ring_shop]

# ╔═╡ 7b58081d-3b30-4876-9f18-c0e8ebc3cf9e
# constraints:
# [
# 	1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0
# 	0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0
# 	0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1
# ] * build <= [1,1,2]
function all_builds()
	unique([
		BitVector([
			[x == w for x=1:5];
			[y == a for y=1:5];
			[z == r1 || z == r2 for z=1:6];
		]) for w=1:5,a=0:5,r1=0:6,r2=0:6
	])
end

# ╔═╡ 49bfe4c9-053e-4545-a82d-798806317f04
[[1,0,0] <= ([
	1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0
	0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0
	0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1
] * build) <= [1,1,2] for build=all_builds()]

# ╔═╡ dbff9e97-42ec-4028-8827-bf0c2d25a41e
function test_build(build)
	stats = shops * build
	try_battle2(Fighter(100, stats[2], stats[3]), boss)
end

# ╔═╡ 65cf9bd4-d784-41c8-bd59-2201d70580a1
const part1 = minimum(build -> [1 0 0] * shops * build, filter(test_build, all_builds()))

# ╔═╡ c40c7cc7-efd5-4a7c-8b1d-fd6fea5c24e0
const part2 = maximum(build -> [1 0 0] * shops * build, filter(!test_build, all_builds()))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═f4ba9ac8-27c7-11ed-28f1-03829199c882
# ╠═29a02557-51fb-4f54-b98b-1f06f0cfd310
# ╠═bbddb591-a420-4d11-bf2c-8c58e83109a0
# ╠═2fdeb9fd-6d09-439c-8b95-ce18e3a036c7
# ╠═06ff07fa-369a-4415-8191-d9f58df0fe28
# ╠═4ea9c5ea-6421-4dea-b6c0-d3db755ac286
# ╠═435b379b-f3c0-42e2-8a81-422f38ba6ce1
# ╠═d9193dfa-e937-435e-9ed2-8dc122f6b86f
# ╠═8879d61d-5493-4af1-815e-91af44d90876
# ╠═d757780b-b9e0-48a1-90f3-3a1e17690af3
# ╠═1a3c3515-1af5-4308-b0a6-6b6c77d482dd
# ╠═2d61b789-a22c-487d-9f23-05194218f2eb
# ╠═aa00b4ee-9177-4387-9517-fe93ca752c54
# ╠═b52c816b-261e-4fc1-8760-3a1e28faeb5f
# ╠═7b58081d-3b30-4876-9f18-c0e8ebc3cf9e
# ╠═49bfe4c9-053e-4545-a82d-798806317f04
# ╠═dbff9e97-42ec-4028-8827-bf0c2d25a41e
# ╠═65cf9bd4-d784-41c8-bd59-2201d70580a1
# ╠═c40c7cc7-efd5-4a7c-8b1d-fd6fea5c24e0
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
