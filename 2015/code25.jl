### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ f0524794-41ad-4ea9-b9e1-ba0fbc8534d8
const input = readlines("input25.txt")[1]

# ╔═╡ 5488b48c-d8ee-4892-a366-44ec4eb73302
t(n) = n*(n+1)÷2

# ╔═╡ cd9d17e2-d2a8-4635-9fb3-2e072fa79f51
# (c+r-2)*(c+r-1)     ÷2 + c
#     	c 		r 		-2
# c 	c^2 	cr 		-2c
# r 	cr 		r^2 	-2r
# -1 	-c 		-r 		2
# (c^2 + r^2 + 2cr -3c -3r + 2 + 2c) ÷ 2

# ╔═╡ 464a4ee3-5b57-4604-ba2f-94005eede374
get_code_index2(r,c) = (r^2 + c^2 + 2*c*r -3r - c + 2)÷2

# ╔═╡ ee1d7944-27f2-11ed-1e02-c9489b074d59
get_code_index(r,c) = t(c + r - 2) + c

# ╔═╡ 07aee17f-e8b6-43d5-aa00-2b2ba5e4c3b7
get_code_index(5,3)

# ╔═╡ 18442ca7-1ee5-478e-b048-b96332488d3e
const code_index = get_code_index(2981, 3075)

# ╔═╡ d9d13efc-a26e-4cce-b29a-385a084f96d9
function get_nth_code(n)
	m = 33554393
	20151125 * powermod(252533, n-1, m) % m
end

# ╔═╡ bd9dc095-e8db-4533-a7f2-0026769a73ba
const part1 = get_nth_code(get_code_index(2981, 3075))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═f0524794-41ad-4ea9-b9e1-ba0fbc8534d8
# ╠═5488b48c-d8ee-4892-a366-44ec4eb73302
# ╠═cd9d17e2-d2a8-4635-9fb3-2e072fa79f51
# ╠═464a4ee3-5b57-4604-ba2f-94005eede374
# ╠═07aee17f-e8b6-43d5-aa00-2b2ba5e4c3b7
# ╠═ee1d7944-27f2-11ed-1e02-c9489b074d59
# ╠═18442ca7-1ee5-478e-b048-b96332488d3e
# ╠═d9d13efc-a26e-4cce-b29a-385a084f96d9
# ╠═bd9dc095-e8db-4533-a7f2-0026769a73ba
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
