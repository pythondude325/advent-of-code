### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ fa4b2392-27de-11ed-3876-fbeda7479e12
const input = readlines("input23.txt")

# ╔═╡ 13bc7cc5-fcb1-43c4-bede-771b29f3037d
abstract type Instruction end

# ╔═╡ 6edd3e7b-d99c-4aec-a067-3421ef7322e9
struct Half <: Instruction reg::Symbol end

# ╔═╡ a4a8e1a4-9ea6-4911-9673-3d58afe01cd9
struct Triple <: Instruction reg::Symbol end

# ╔═╡ 1c4da8d1-9153-493b-b4d0-baef6aadf6ea
struct Increment <: Instruction reg::Symbol end

# ╔═╡ 197ff3d2-07c4-49de-aa59-6be4d6274fef
struct Jump <: Instruction offset::Int end

# ╔═╡ b54e422f-bfbc-440e-ac1a-4ff44e3c0cad
struct JumpIfEven <: Instruction
	reg::Symbol
	offset::Int
end

# ╔═╡ 4d9e238e-b53a-4a0b-90d7-54ffa42f205a
struct JumpIfOne <: Instruction
	reg::Symbol
	offset::Int
end

# ╔═╡ 75d25edf-9b85-4167-9830-e2c10437800e
function Base.parse(::Type{Instruction}, s)::Instruction
	i = s[1:3]
	if i == "hlf"
		Half(Symbol(s[5:end]))
	elseif i == "tpl"
		Triple(Symbol(s[5:end]))
	elseif i == "inc"
		Increment(Symbol(s[5:end]))
	elseif i == "jmp"
		Jump(parse(Int, s[5:end]))
	elseif i == "jie"
		JumpIfEven(Symbol(s[5:5]), parse(Int, s[8:end]))
	elseif i == "jio"
		JumpIfOne(Symbol(s[5:5]), parse(Int, s[8:end]))
	else
		error("invalid instruction format '$s'")
	end
end

# ╔═╡ 94b3bb1e-b82d-4da8-82e2-96b7347993fa
const program = parse.(Instruction, input)

# ╔═╡ 60fb935a-2dbe-46d6-b3d4-6e8833a8c16c
function interpret(program::Vector{Instruction}; init=(0,0))
	a, b = init
	ip = 1
	
	execute(half::Half) = half.reg == :a ? (a ÷= 2) : (b ÷= 2)
	execute(triple::Triple) = triple.reg == :a ? (a *= 3) : (b *= 3)
	execute(increment::Increment) = increment.reg == :a ? (a += 1) : (b += 1)
	execute(jump::Jump) = (ip += jump.offset - 1)
	function execute(jie::JumpIfEven)
		if iseven(jie.reg == :a ? a : b)
			ip += jie.offset - 1
		end
	end
	function execute(jio::JumpIfOne)
		if 1 == (jio.reg == :a ? a : b)
			ip += jio.offset - 1
		end
	end
	
	while ip <= length(program)
		execute(program[ip])
		ip += 1
	end

	a,b
end

# ╔═╡ 5f8cd324-7b0c-4192-8b39-5244b34ed73d
const part1 = interpret(program)

# ╔═╡ b0e315ce-9856-4021-badd-e57b87b47bf9
const part2 = interpret(program; init=(1,0))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═fa4b2392-27de-11ed-3876-fbeda7479e12
# ╠═13bc7cc5-fcb1-43c4-bede-771b29f3037d
# ╠═6edd3e7b-d99c-4aec-a067-3421ef7322e9
# ╠═a4a8e1a4-9ea6-4911-9673-3d58afe01cd9
# ╠═1c4da8d1-9153-493b-b4d0-baef6aadf6ea
# ╠═197ff3d2-07c4-49de-aa59-6be4d6274fef
# ╠═b54e422f-bfbc-440e-ac1a-4ff44e3c0cad
# ╠═4d9e238e-b53a-4a0b-90d7-54ffa42f205a
# ╠═75d25edf-9b85-4167-9830-e2c10437800e
# ╠═94b3bb1e-b82d-4da8-82e2-96b7347993fa
# ╠═60fb935a-2dbe-46d6-b3d4-6e8833a8c16c
# ╠═5f8cd324-7b0c-4192-8b39-5244b34ed73d
# ╠═b0e315ce-9856-4021-badd-e57b87b47bf9
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
