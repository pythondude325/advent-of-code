### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ fdbbd2a8-2651-11ed-061b-8118f1538650
const input = readlines("input18.txt")

# ╔═╡ fc53c3e7-a4d9-415b-8ac3-dd8b85bd97d0
const start = reduce(hcat, ['#' == c for c=s] for s=input)

# ╔═╡ 42e11423-66cd-482d-a484-dc59c248e647
function gol(m)
	get(x,y) = checkbounds(Bool, m, x, y) ? m[x,y] : false
	[
		begin
			ns = sum(get(x+dx,y+dy) for dx=-1:1,dy=-1:1) 
			ns == 3 || (ns == 4 && get(x,y))
		end
		for x=1:size(m,1),y=1:size(m,2)
	]

end

# ╔═╡ 3341c5e4-534c-4ff0-97c9-d9ec79234609
funcpow(f,p) = foldl(∘, fill(f, p))

# ╔═╡ 22338420-2928-46d0-ac1f-a29fbf444444
const part1 = start |> funcpow(gol, 100) |> count

# ╔═╡ b9ac0c99-930a-47be-8156-803377cf69ce
function set_corners(m)
	m[1,1] = true
	m[1,100] = true
	m[100,1] = true
	m[100,100] = true
	m
end

# ╔═╡ 29e097c3-ab2f-4b66-aeef-c3184726bca2
const part2 = start |> funcpow(set_corners ∘ gol, 100) |> count

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═fdbbd2a8-2651-11ed-061b-8118f1538650
# ╠═fc53c3e7-a4d9-415b-8ac3-dd8b85bd97d0
# ╠═42e11423-66cd-482d-a484-dc59c248e647
# ╠═3341c5e4-534c-4ff0-97c9-d9ec79234609
# ╠═22338420-2928-46d0-ac1f-a29fbf444444
# ╠═b9ac0c99-930a-47be-8156-803377cf69ce
# ╠═29e097c3-ab2f-4b66-aeef-c3184726bca2
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
