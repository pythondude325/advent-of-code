### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 7ad2b3f8-255b-11ed-2044-3f253d02ada7
const input = readlines("input05.txt")

# ╔═╡ 9a7f3cf4-a450-479e-9d3e-89047f258a7d
'a' ∈ "aeiou"

# ╔═╡ ea57228f-fcf3-4d66-8c2d-2e59d5488cf3
function test_double(s)
	s = collect(s)
	any(s[1:end-1] .== s[2:end])
end

# ╔═╡ 394555b8-485d-486c-864c-a74d4c39ee96
function test_part1(s)
	count(∈("aeiou"), s) >= 3 &&
	occursin(r"[aeiou].*[aeoiu].*[aeoiu]", s) &&
	occursin(r"([a-z])\1", s) &&
	!contains(s, "ab") &&
	!contains(s, "cd") &&
	!contains(s, "pq") &&
	!contains(s, "xy")
end

# ╔═╡ f96cf726-95ae-43e8-96fa-4e33d7986c41
const part1 = count(test_part1, input)

# ╔═╡ cae6b9f3-32f0-4ca0-b84f-12cce2ebe9e6
function test_part2(s)
	occursin(r"([a-z][a-z]).*\1", s) && occursin(r"([a-z])[a-z]\1", s)
end

# ╔═╡ cdce2100-62b6-4869-9c12-faaffba56185
const part2 = count(test_part2, input)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═7ad2b3f8-255b-11ed-2044-3f253d02ada7
# ╠═9a7f3cf4-a450-479e-9d3e-89047f258a7d
# ╠═ea57228f-fcf3-4d66-8c2d-2e59d5488cf3
# ╠═394555b8-485d-486c-864c-a74d4c39ee96
# ╠═f96cf726-95ae-43e8-96fa-4e33d7986c41
# ╠═cae6b9f3-32f0-4ca0-b84f-12cce2ebe9e6
# ╠═cdce2100-62b6-4869-9c12-faaffba56185
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
