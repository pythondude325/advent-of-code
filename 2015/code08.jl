### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 0c64c674-260f-11ed-2703-ff041e9c1003
const input = readlines("input08.txt")

# ╔═╡ 669bb2ad-96e6-49d0-a1b4-326519e1f360
const test_input = split(strip("""
	""
	"abc"
	"aaa\\"aaa"
	"\\x27" """), "\n")

# ╔═╡ 004607e1-61b9-4d8d-beb5-a2692729fd55
function parse_string(s)
	s = s[2:end-1]
	v = Vector{Char}()
	i = 1
	while i <= length(s)
		if s[i] == '\\'
			if s[i+1] == 'x'
				push!(v, Char(parse(UInt8, s[i+2:i+3], base=16)))
				i += 4
			elseif s[i+1] == '"'
				push!(v, '"')
				i += 2
			elseif s[i+1] == '\\'
				push!(v, '\\')
				i += 2
			end
		else
			push!(v, s[i])
			i += 1
		end
	end
	v
end

# ╔═╡ f8f966a1-05e1-40d7-b525-68c8923112c6
function measure(input)
	sum(length.(input)) - sum(length.(parse_string.(input)))
end

# ╔═╡ 4de0e366-4c5a-4e18-a6ab-edc47c859a55
const part1 = measure(input)

# ╔═╡ 81490e32-732c-460d-ad45-c453dfc57987
function encode_string(s)
	l = 2
	for c=s
		if c == '\\'
			l += 2
		elseif c == '\"'
			l += 2
		else
			l += 1
		end
	end
	l                                                                 
end

# ╔═╡ b30df79e-15d7-4e35-bc54-d51ac308600d
function measure_part2(input)
	sum(encode_string.(input)) - sum(length.(input))
end

# ╔═╡ 91c8ac20-c15f-4471-a11f-5d7651523ab8
const part2 = measure_part2(input)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═0c64c674-260f-11ed-2703-ff041e9c1003
# ╠═669bb2ad-96e6-49d0-a1b4-326519e1f360
# ╠═004607e1-61b9-4d8d-beb5-a2692729fd55
# ╠═f8f966a1-05e1-40d7-b525-68c8923112c6
# ╠═4de0e366-4c5a-4e18-a6ab-edc47c859a55
# ╠═81490e32-732c-460d-ad45-c453dfc57987
# ╠═b30df79e-15d7-4e35-bc54-d51ac308600d
# ╠═91c8ac20-c15f-4471-a11f-5d7651523ab8
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
