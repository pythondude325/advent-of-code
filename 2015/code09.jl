### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ e66940d0-1b7b-40a9-87f4-2ee2fc8c2976
using Permutations

# ╔═╡ 31ba1adf-3991-4fab-a366-e0ce9bdc8e90
const input = readlines("input09.txt")

# ╔═╡ 6f7abb28-1779-4e19-8ed9-1a8778d42dbc
function build_graph(input)
	cities = String[]
	function intern(x::AbstractString)::Int
		if x ∉ cities 
			push!(cities, x)
			length(cities)
		else
			findfirst(==(x), cities)
		end
	end
	
	paths = NTuple{3,Int}[]
	for path=input
		m = match(r"^(\w+) to (\w+) = (\d+)$", path)
		a = intern(m[1])
		b = intern(m[2])
		d = parse(Int, m[3])
		push!(paths, (a,b,d))
	end

	n = length(cities)
	adjmat = zeros(Int, n, n)
	for (a,b,d)=paths
		adjmat[a,b] = d
		adjmat[b,a] = d
	end
	n, adjmat
end

# ╔═╡ 0d2fd8d4-1f30-4b95-bed7-499c3dc0a18b
n, g = build_graph(input)

# ╔═╡ 5bd4bf81-c4f7-4665-8b67-34c466d4b58d
function path_length(k)
	P = collect(Permutation(n,k))
	sum(g[a,b] for (a,b)=zip(P[1:end-1], P[2:end]))
end

# ╔═╡ 56dc29c7-669d-413b-96a5-7ddd1b116b50
const part1 = minimum(path_length, 1:factorial(n))

# ╔═╡ af009bb1-41a9-4209-8e96-49be4046f531
const part2 = maximum(path_length, 1:factorial(n))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Permutations = "2ae35dd2-176d-5d53-8349-f30d82d94d4f"

[compat]
Permutations = "~0.4.14"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "af1309ffb38d482184b67bb42e10852ae1012561"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Combinatorics]]
git-tree-sha1 = "08c8b6831dc00bfea825826be0bc8336fc369860"
uuid = "861a8166-3701-5b0c-9a16-15d98fcdc6aa"
version = "1.0.2"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.Permutations]]
deps = ["Combinatorics", "LinearAlgebra", "Random"]
git-tree-sha1 = "f3e7100a00388b602c7d88a7e3cf12539370f28d"
uuid = "2ae35dd2-176d-5d53-8349-f30d82d94d4f"
version = "0.4.14"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═e66940d0-1b7b-40a9-87f4-2ee2fc8c2976
# ╠═31ba1adf-3991-4fab-a366-e0ce9bdc8e90
# ╠═6f7abb28-1779-4e19-8ed9-1a8778d42dbc
# ╠═0d2fd8d4-1f30-4b95-bed7-499c3dc0a18b
# ╠═5bd4bf81-c4f7-4665-8b67-34c466d4b58d
# ╠═56dc29c7-669d-413b-96a5-7ddd1b116b50
# ╠═af009bb1-41a9-4209-8e96-49be4046f531
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
