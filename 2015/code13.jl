### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 4f3ca3b0-cd5a-4b3c-81a8-4720e6434545
using Permutations

# ╔═╡ e522b6ea-2628-11ed-3f36-8fd4e9ac6797
const input = readlines("input13.txt")

# ╔═╡ 3690eee1-3272-4a95-a4bf-926396837528
const test_input = split(strip("""
Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.
"""), "\n")

# ╔═╡ caa8d7b0-8111-46b0-93b1-3ef97dd46d68
function build_graph(input)
	cities = String[]
	function intern(x::AbstractString)::Int
		if x ∉ cities 
			push!(cities, x)
			length(cities)
		else
			findfirst(==(x), cities)
		end
	end
	
	paths = NTuple{3,Int}[]
	for path=input
		m = match(r"^(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+).$", path)
		a = intern(m[1])
		b = intern(m[4])
		d = ((m[2] == "gain") ? 1 : -1) * parse(Int, m[3])
		push!(paths, (a,b,d))
	end

	n = length(cities)
	adjmat = zeros(Int, n, n)
	for (a,b,d)=paths
		adjmat[a,b] = d
	end
	n, adjmat
end

# ╔═╡ de8255c1-9b0f-4871-92c6-e5d303c08a26
function path_length(n,g)
	function (k)
		P = collect(Permutation(n,k))
		sum(g[a,b] + g[b,a] for (a,b)=zip(P, circshift(P, 1)))
	end
end

# ╔═╡ a4e74cb9-ec54-4124-8539-e3a871f6ca8b
const n, g = build_graph(input)

# ╔═╡ 580fed23-5200-4f62-8beb-763a6075131e
const part1 = maximum(path_length(n,g), 1:factorial(n))

# ╔═╡ 1a08c0d0-bcfe-43c0-ace7-3c510a1cb751
const p2_n, p2_g = 9, [g zeros(Int, 8); zeros(Int, 1,9)]

# ╔═╡ 2879f463-97eb-4092-b8e1-a9f7ca308ba2
const part2 = maximum(path_length(p2_n, p2_g), 1:factorial(p2_n))

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Permutations = "2ae35dd2-176d-5d53-8349-f30d82d94d4f"

[compat]
Permutations = "~0.4.14"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "2a7cd7a16c595c77714fd9ac660d2e1cc715b8dd"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Combinatorics]]
git-tree-sha1 = "08c8b6831dc00bfea825826be0bc8336fc369860"
uuid = "861a8166-3701-5b0c-9a16-15d98fcdc6aa"
version = "1.0.2"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.5.2+0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.20+0"

[[deps.Permutations]]
deps = ["Combinatorics", "LinearAlgebra", "Random"]
git-tree-sha1 = "f3e7100a00388b602c7d88a7e3cf12539370f28d"
uuid = "2ae35dd2-176d-5d53-8349-f30d82d94d4f"
version = "0.4.14"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.1.1+0"
"""

# ╔═╡ Cell order:
# ╠═4f3ca3b0-cd5a-4b3c-81a8-4720e6434545
# ╠═e522b6ea-2628-11ed-3f36-8fd4e9ac6797
# ╟─3690eee1-3272-4a95-a4bf-926396837528
# ╠═caa8d7b0-8111-46b0-93b1-3ef97dd46d68
# ╠═de8255c1-9b0f-4871-92c6-e5d303c08a26
# ╠═a4e74cb9-ec54-4124-8539-e3a871f6ca8b
# ╠═580fed23-5200-4f62-8beb-763a6075131e
# ╠═1a08c0d0-bcfe-43c0-ace7-3c510a1cb751
# ╠═2879f463-97eb-4092-b8e1-a9f7ca308ba2
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
