### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ ec0b87b4-27d5-11ed-3d72-5de932b6d74b
const input = readlines("input22.txt")

# ╔═╡ 20c1d8a6-4e66-4e89-8cdc-af69ee81d003
const boss_hp = parse(Int, input[1][12:end])

# ╔═╡ 72d4f7a4-0271-4ce1-b121-24a3580709fa
const boss_damage = parse(Int, input[2][8:end])

# ╔═╡ 68f19e6a-7043-4fa5-88d6-508f91410e5c
# Spell list:
# - Magic Missile costs 53 mana. It instantly does 4 damage.
# - Drain costs 73 mana. It instantly does 2 damage and heals you for 2 hit points.
# - Shield costs 113 mana. It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
# - Poison costs 173 mana. It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
# - Recharge costs 229 mana. It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.


# ╔═╡ 56b11856-b25f-46c5-8661-3063c9a04d2f
struct State
	boss_hp::Int
	player_hp::Int
	mana::Int
	shield_timer::Int
	poison_timer::Int
	recharge_timer::Int
end

# ╔═╡ 3992a8a2-f9aa-48f6-8ace-149f083a0fd8
do_effects(state) =
	7 * (state.shield_timer > 0), State(
		state.boss_hp - 3 * (state.poison_timer > 0),
		state.player_hp,
		state.mana + 101 * (state.recharge_timer > 0),
		max(0, state.shield_timer - 1),
		max(0, state.poison_timer - 1),
		max(0, state.recharge_timer - 1)
	)

# ╔═╡ 99aad1d0-4810-4f90-b936-1023d384fbd2
function play_boss(state, part2)
	armor, state = do_effects(state)
	if state.boss_hp <= 0
		return 0
	end
	play_player(State(
		state.boss_hp,
		state.player_hp - max(1, boss_damage - armor),
		state.mana,
		state.shield_timer,
		state.poison_timer,
		state.recharge_timer,
	), part2)
end

# ╔═╡ bc8aa9e1-e252-497c-93b0-abb630dc8d6f
function play_player(state, part2)
	if part2
		state = State(
			state.boss_hp,
			state.player_hp - 1,
			state.mana,
			state.shield_timer,
			state.poison_timer,
			state.recharge_timer,
		)
	end
	
	if state.player_hp <= 0
		return nothing
	end
	
	_, state = do_effects(state)

	if state.boss_hp <= 0
		return 0
	end

	possibilities = Int[]
	
	# try casting MM
	if state.mana >= 53
		p = play_boss(
			State(
				state.boss_hp - 4,
				state.player_hp,
				state.mana - 53,
				state.shield_timer,
				state.poison_timer,
				state.recharge_timer,
			),
			part2,
		)
		if !isnothing(p)
			push!(possibilities, p + 53)
		end
	end

	# try casting Drain
	if state.mana >= 73
		p = play_boss(
			State(
				state.boss_hp - 2,
				state.player_hp + 2,
				state.mana - 73,
				state.shield_timer,
				state.poison_timer,
				state.recharge_timer,
			),
			part2
		)
		if !isnothing(p)
			push!(possibilities, p + 73)
		end
	end

	# try casting Shield
	if state.mana >= 113
		p = play_boss(
			State(
				state.boss_hp,
				state.player_hp,
				state.mana - 113,
				6,
				state.poison_timer,
				state.recharge_timer,
			),
			part2
		)
		if !isnothing(p)
			push!(possibilities, p + 113)
		end
	end

	# try casting Poison
	if state.mana >= 173
		p = play_boss(
			State(
				state.boss_hp,
				state.player_hp,
				state.mana - 173,
				state.shield_timer,
				6,
				state.recharge_timer,
			),
			part2
		)
		if !isnothing(p)
			push!(possibilities, p + 173)
		end
	end

	# try casting Recharge
	if state.mana >= 229
		p = play_boss(
			State(
				state.boss_hp,
				state.player_hp,
				state.mana - 229,
				state.shield_timer,
				state.poison_timer,
				5,
			),
			part2
		)
		if !isnothing(p)
			push!(possibilities, p + 229)
		end
	end

	if length(possibilities) > 0
		minimum(possibilities)
	else
		nothing
	end
end

# ╔═╡ 9a60d8b5-c504-4653-8f98-9e7baad68c74
const part1 = play_player(State(
	boss_hp,
	50,
	500,
	0,
	0,
	0,
), false)

# ╔═╡ c778c0fc-54e7-4304-a9a5-3cbecfab7602
const part2 = play_player(State(
	boss_hp,
	50,
	500,
	0,
	0,
	0,
), true)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═ec0b87b4-27d5-11ed-3d72-5de932b6d74b
# ╠═20c1d8a6-4e66-4e89-8cdc-af69ee81d003
# ╠═72d4f7a4-0271-4ce1-b121-24a3580709fa
# ╠═68f19e6a-7043-4fa5-88d6-508f91410e5c
# ╠═56b11856-b25f-46c5-8661-3063c9a04d2f
# ╠═3992a8a2-f9aa-48f6-8ace-149f083a0fd8
# ╠═bc8aa9e1-e252-497c-93b0-abb630dc8d6f
# ╠═99aad1d0-4810-4f90-b936-1023d384fbd2
# ╠═9a60d8b5-c504-4653-8f98-9e7baad68c74
# ╠═c778c0fc-54e7-4304-a9a5-3cbecfab7602
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
