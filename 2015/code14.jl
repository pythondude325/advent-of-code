### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ b1ed96c6-262a-11ed-2af6-97de06a332c7
const input = readlines("input14.txt")

# ╔═╡ a900d9d8-fce5-4d4a-ade4-e0a8fccd5a0e
struct Reindeer
	name::String
	flyspeed::Int
	flytime::Int
	resttime::Int
end

# ╔═╡ c989e8ca-5c4e-4add-ba63-6def4c92b083
function Base.parse(::Type{Reindeer}, s)::Reindeer
	# Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds
	m = match(r"^(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.$", s)
	Reindeer(
		m[1],
		parse(Int, m[2]),
		parse(Int, m[3]),
		parse(Int, m[4]),
	)
end

# ╔═╡ 03919cb2-202e-43cc-b73b-f44850184a9d
const reindeer = parse.(Reindeer, input)

# ╔═╡ 2bbefce1-001d-4741-82ee-47ce0865331f
const test_comet = parse(Reindeer, "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")

# ╔═╡ 9e4a0a44-01ca-4069-a5b0-d0b6a6d9420c
const test_dancer = parse(Reindeer, "Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")

# ╔═╡ 48088c50-b18c-4b4d-a5dc-6f2cacc4b82d
function race(r::Reindeer, time::Int)
	d = 0
	while true
		if time >= r.flytime
			time -= r.flytime
			d += r.flytime * r.flyspeed
		else 
			d += time * r.flyspeed
		end
		if time >= r.resttime
			time -= r.resttime
		else
			break
		end
	end
	d
end

# ╔═╡ 72e42fa9-60d3-4490-81d9-695d2b29fbd4
const part1 = maximum(r -> race(r, 2503), reindeer)

# ╔═╡ c919baf4-3bb9-4bce-acb5-50918ab65576
function race_part2(rs::Vector{Reindeer}, time_limit::Int)
	points = [0 for r=rs]
	states = [(true, r.flytime, 0) for r=rs]

	advance(r, (f, t, d)) =
		(
			f ⊻ (t <= 1),
			if t <= 1
				f ? r.resttime : r.flytime
			else
				t - 1
			end,
			d + f * r.flyspeed
		)
	for t=1:time_limit
		states .= advance.(rs, states)
		distances = [d for (f,t,d)=states]
		points[findall(==(maximum(distances)), distances)] .+= 1
	end
	maximum(points)
end

# ╔═╡ 7a1dafba-2381-479e-9b4e-262c28564563
const part2 = race_part2(reindeer, 2503)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.0"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═b1ed96c6-262a-11ed-2af6-97de06a332c7
# ╠═a900d9d8-fce5-4d4a-ade4-e0a8fccd5a0e
# ╠═c989e8ca-5c4e-4add-ba63-6def4c92b083
# ╠═03919cb2-202e-43cc-b73b-f44850184a9d
# ╠═2bbefce1-001d-4741-82ee-47ce0865331f
# ╠═9e4a0a44-01ca-4069-a5b0-d0b6a6d9420c
# ╠═48088c50-b18c-4b4d-a5dc-6f2cacc4b82d
# ╠═72e42fa9-60d3-4490-81d9-695d2b29fbd4
# ╠═c919baf4-3bb9-4bce-acb5-50918ab65576
# ╠═7a1dafba-2381-479e-9b4e-262c28564563
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
